package ru.nobirds.rx.gl.font

import org.junit.Test
import ru.nobirds.rx.gl.font.fnt.FontMetaParser
import ru.nobirds.rx.test.LwjglGdxTest


class FontMetaTest : LwjglGdxTest {

    @Test
    fun test1() {
        val meta = FontMetaParser
                .parse(ClassLoader.getSystemResourceAsStream("test.fnt").reader(), "test") {
                    ClassLoader.getSystemResourceAsStream(it).readBytes()
                }

        println(meta)
    }

}