package ru.nobirds.rx.gl.texture

import java.lang.StrictMath.abs
import java.lang.StrictMath.round

fun uv(u: Float, v: Float):UV = UV(u, v)

data class UV(val u: Float, val v: Float)

data class UVRegion(val from: UV, val to: UV) {

    companion object {
        fun from(u1: Float, v1: Float, u2: Float, v2: Float):UVRegion = UVRegion(UV(u1, v1), UV(u2, v2))

        fun from(originalWidth: Int, originalHeight: Int, x: Int, y: Int, width: Int, height: Int):UVRegion {
            val invertedWidth = 1f / originalWidth
            val invertedHeight = 1f / originalHeight

            val from = UV(x * invertedWidth, y * invertedHeight)
            val to = UV((x + width) * invertedWidth, (y + height) * invertedHeight)

            return UVRegion(from, to)
        }
    }

}

interface TextureRegion {

    val texture: Texture

    val region: UVRegion

    val x: Int
    val y: Int
    val width: Int
    val height: Int

    companion object {

        fun from(texture: Texture): TextureRegion = TextureRegionImpl(texture)

        fun from(region: TextureRegion, x: Int, y: Int, width: Int, height: Int): TextureRegion {
            return from(region.texture, region.x + x, region.y + y, width, height)
        }

        fun from(texture: Texture, x: Int, y: Int, width: Int, height: Int): TextureRegion {
            val uvRegion = UVRegion.from(texture.width, texture.height, x, y, width, height)

            return TextureRegionImpl(texture, uvRegion, x, y, width, height)
        }

        fun from(texture: Texture, u1: Float, v1: Float, u2: Float, v2: Float): TextureRegion {
            return TextureRegionImpl(texture, UVRegion.from(u1, v1, u2, v2))
        }

    }

}

fun Texture.region(): TextureRegion = TextureRegion.from(this)

fun Texture.region(x: Int, y: Int, width: Int, height: Int): TextureRegion {
    return TextureRegion.from(this, x, y, width, height)
}

fun TextureRegion.region(x: Int, y: Int, width: Int, height: Int): TextureRegion {
    return TextureRegion.from(this, x, y, width, height)
}

private data class TextureRegionImpl(
        override val texture: Texture,
        override val region: UVRegion = UVRegion.from(0f, 0f, 1f, 1f),
        override val x: Int = round(region.from.u * texture.width),
        override val y: Int = round(region.from.v * texture.height),
        override val width: Int = round(abs(region.to.u - region.from.u) * texture.width),
        override val height: Int = round(abs(region.to.v - region.from.v) * texture.height)) : TextureRegion

