package ru.nobirds.rx.gl.texture

import com.badlogic.gdx.graphics.GL20

class Texture2d(val textureSource: TextureSource,
                magFilter: Texture.Filter = Texture.Filter.Nearest,
                minFilter: Texture.Filter = Texture.Filter.Nearest,
                uWrap: Texture.Wrap = Texture.Wrap.ClampToEdge,
                vWrap: Texture.Wrap = Texture.Wrap.ClampToEdge
) : AbstractTexture(GL20.GL_TEXTURE_2D, magFilter,  minFilter, uWrap, vWrap) {

    init {
        textureSource.loadTo(this)
    }

    override val width: Int
        get() = textureSource.width

    override val height: Int
        get() = textureSource.height

    override fun dispose() {
        super.dispose()
        textureSource.dispose()
    }

}