package ru.nobirds.rx.gl.mesh

import ru.nobirds.rx.gl.BufferDrawer
import ru.nobirds.rx.gl.DrawPrimitive
import ru.nobirds.rx.gl.Shader
import ru.nobirds.rx.gl.bind
import ru.nobirds.rx.gl.buffer.framebuffer.FrameBuffer

class Mesh(val shader: Shader, val drawer: BufferDrawer,
           values: Map<String, Any> = emptyMap()) {

    private var dirty = false

    private val values = hashMapOf<String, Any>().apply {
        values.toMap(this)
    }

    operator fun set(name: String, value: Any) {
        values[name] = value
        dirty = true
    }

    fun remove(name: String) {
        values.remove(name)
        dirty = true
    }

    operator fun get(name: String): Any? = values[name]

    private inline fun draw(block: BufferDrawer.() -> Unit) {
        shader.bind {
            if (dirty) {
                dirty = false
                set(values)
            }
            drawer.block()
        }
    }

    fun draw() {
        draw { draw() }
    }

    fun drawTo(frameBuffer: FrameBuffer) {
        draw { drawTo(frameBuffer) }
    }

}

fun mesh(shader: Shader, primitive: DrawPrimitive, builder: MeshBuilder.()->Unit): Mesh =
        MeshBuilder(shader, primitive).apply { builder() }.build()

