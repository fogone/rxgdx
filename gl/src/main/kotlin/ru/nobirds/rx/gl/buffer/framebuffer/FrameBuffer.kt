package ru.nobirds.rx.gl.buffer.framebuffer

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Pixmap
import ru.nobirds.rx.gl.Bindable
import ru.nobirds.rx.gl.Disposable
import ru.nobirds.rx.gl.bind
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.utils.Gl

class FrameBuffer(val attachments: Set<FrameBufferAttachment>) : Bindable, Disposable {

    private val handle = createFrameBuffer()

    init {
        attach()
    }

    private fun attach() {
        bind {
            for (attachment in attachments) {
                attachment.bind {
                    attachment.attach()
                }
            }
            checkIsAttached()
        }
    }

    private fun checkIsAttached() {
        val result = Gl.glCheckFramebufferStatus(GL20.GL_FRAMEBUFFER)

        if(result != GL20.GL_FRAMEBUFFER_COMPLETE) {
            dispose()
            dispatchError(result)
        }
    }

    private fun dispatchError(result: Int):Nothing {
        val message = when (result) {
            GL20.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT -> "incomplete attachment"
            GL20.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS -> "incomplete dimensions"
            GL20.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT -> "missing attachment"
            GL20.GL_FRAMEBUFFER_UNSUPPORTED -> "unsupported combination of formats"
            else -> "unknown error $result"
        }

        throw IllegalStateException("frame buffer couldn't be constructed: $message")
    }

    private fun createFrameBuffer():Int {
        return Gl.glGenFramebuffer()
    }

    override fun bind() {
        Gl.glBindFramebuffer(GL20.GL_FRAMEBUFFER, handle)
    }

    override fun unbind() {
        Gl.glBindFramebuffer(GL20.GL_FRAMEBUFFER, 0)
    }

    override fun dispose() {
        for (attachment in attachments) {
            attachment.dispose()
        }
        Gl.glDeleteFramebuffer(handle)
    }

}

fun frameBuffer(width: Int, height: Int, builder: FrameBufferBuilder.()->Unit):FrameBuffer {
    val frameBufferBuilder = FrameBufferBuilder(width, height)
    frameBufferBuilder.builder()
    return frameBufferBuilder.build()
}

class FrameBufferBuilder(val width: Int, val height: Int) {

    private var textureIndex = 0

    private val attachments = hashSetOf<FrameBufferAttachment>()

    fun withTexture(format: Pixmap.Format = Pixmap.Format.RGBA8888): Texture {
        val texture = Texture.empty(width, height, format)

        add(RenderTexture(texture, textureIndex++))
        return texture
    }

    fun withDepth(internalFormat:Int = GL20.GL_DEPTH_COMPONENT) {
        add(RenderBuffer(width, height, AttachmentType.depth, internalFormat))
    }

    fun withStencil(internalFormat: Int = GL20.GL_STENCIL_INDEX) {
        add(RenderBuffer(width, height, AttachmentType.depth, internalFormat))
    }

    private fun add(attachment: FrameBufferAttachment) {
        attachments.add(attachment)
    }

    fun build(): FrameBuffer = FrameBuffer(attachments)

}
