package ru.nobirds.rx.gl.texture

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.gl.Glable
import ru.nobirds.rx.gl.bind
import ru.nobirds.rx.gl.utils.Gl
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

abstract class AbstractTexture(override val target: Int,
                               magFilter: Texture.Filter = Texture.Filter.Nearest,
                               minFilter: Texture.Filter = Texture.Filter.Nearest,
                               uWrap: Texture.Wrap = Texture.Wrap.ClampToEdge,
                               vWrap: Texture.Wrap = Texture.Wrap.ClampToEdge) : Texture {

    override val gl: Int = Gl.glGenTexture()

    override var magFilter: Texture.Filter by parameter(GL20.GL_TEXTURE_MAG_FILTER, magFilter)
    override var minFilter: Texture.Filter by parameter(GL20.GL_TEXTURE_MIN_FILTER, minFilter)
    override var uWrap: Texture.Wrap by parameter(GL20.GL_TEXTURE_WRAP_S, uWrap)
    override var vWrap: Texture.Wrap by parameter(GL20.GL_TEXTURE_WRAP_T, vWrap)

    override fun bind() {
        Gl.glBindTexture(target, gl)
    }

    override fun unbind() {
        Gl.glBindTexture(target, 0)
    }

    override fun dispose() {
        Gl.glDeleteTexture(gl)
    }

    private fun <E : Glable> parameter(parameter: Int, initial: E): ReadWriteProperty<Texture, E> {
        setParameter(parameter, initial.gl)

        return object : ReadWriteProperty<Texture, E> {

            private var value: E = initial

            override fun getValue(thisRef: Texture, property: KProperty<*>): E = value

            override fun setValue(thisRef: Texture, property: KProperty<*>, value: E) {
                setParameter(parameter, value.gl)
                this.value = value
            }

        }
    }

    private fun setParameter(parameter: Int, value: Int) {
        bind {
            Gl.glTexParameteri(target, parameter, value)
        }
    }

}