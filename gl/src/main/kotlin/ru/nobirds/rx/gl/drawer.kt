package ru.nobirds.rx.gl

import ru.nobirds.rx.gl.buffer.ElementsArrayBuffer
import ru.nobirds.rx.gl.buffer.StructuredArrayBuffer
import ru.nobirds.rx.gl.buffer.framebuffer.FrameBuffer
import ru.nobirds.rx.gl.utils.Gl

interface BufferDrawer {

    val primitive: DrawPrimitive

    fun draw()

    fun drawTo(buffer: FrameBuffer)

}

abstract class AbstractArrayBufferDrawer(
        val data: StructuredArrayBuffer,
        override val primitive: DrawPrimitive
) : BufferDrawer {

    override fun draw() {
        data.bind {
            drawImpl()
        }
    }

    override fun drawTo(buffer: FrameBuffer) {
        try {
            buffer.bind()
            draw()
        } finally {
            buffer.unbind()
        }
    }

    abstract fun drawImpl()

}

class ArrayBufferDrawer(data: StructuredArrayBuffer, primitive: DrawPrimitive) :
        AbstractArrayBufferDrawer(data, primitive) {

    override fun drawImpl() {
        Gl.glDrawArrays(primitive.gl, 0, data.count)
    }
}

class ElementsArrayBufferDrawer(data: StructuredArrayBuffer, val index: ElementsArrayBuffer<*>, primitive: DrawPrimitive) :
        AbstractArrayBufferDrawer(data, primitive) {

    override fun drawImpl() {
        index.bind {
            Gl.glDrawElements(primitive.gl, index.size, index.glType, 0)
        }
    }

}