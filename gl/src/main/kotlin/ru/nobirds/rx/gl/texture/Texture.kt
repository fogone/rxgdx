package ru.nobirds.rx.gl.texture

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Pixmap
import ru.nobirds.rx.gl.Bindable
import ru.nobirds.rx.gl.Disposable
import ru.nobirds.rx.gl.Glable
import ru.nobirds.rx.utils.files.ImmutableResource

interface Texture : Bindable, Disposable, Glable {

    companion object {

        fun empty(width: Int, height: Int, format: Pixmap.Format): Texture = Texture2d(TextureSource.empty(format, height, width))
        fun from(resource: ImmutableResource): Texture = Texture2d(TextureSource.from(resource))
        fun from(pixmap: Pixmap): Texture = Texture2d(TextureSource.from(pixmap))
        fun from(source: TextureSource): Texture = Texture2d(source)

    }

    enum class Filter(override val gl: Int) : Glable {

        Nearest(GL20.GL_NEAREST),
        Linear(GL20.GL_LINEAR),

        MipMap(GL20.GL_LINEAR_MIPMAP_LINEAR),
        MipMapNearestNearest(GL20.GL_NEAREST_MIPMAP_NEAREST),
        MipMapLinearNearest(GL20.GL_LINEAR_MIPMAP_NEAREST),
        MipMapNearestLinear(GL20.GL_NEAREST_MIPMAP_LINEAR),
        MipMapLinearLinear(GL20.GL_LINEAR_MIPMAP_LINEAR);

        val isMipMap: Boolean
            get() = gl != GL20.GL_NEAREST && gl != GL20.GL_LINEAR
    }

    enum class Wrap(override val gl: Int) : Glable {
        MirroredRepeat(GL20.GL_MIRRORED_REPEAT),
        ClampToEdge(GL20.GL_CLAMP_TO_EDGE),
        Repeat(GL20.GL_REPEAT)
    }


    val target: Int

    val width: Int

    val height: Int

    var magFilter: Filter
    var minFilter: Filter
    var uWrap: Wrap
    var vWrap: Wrap

}

