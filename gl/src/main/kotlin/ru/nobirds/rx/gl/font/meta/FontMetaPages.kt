package ru.nobirds.rx.gl.font.meta

import com.badlogic.gdx.graphics.Pixmap

interface FontMetaPages {

    val ids: Iterable<Int>

    fun get(id: Int): Pixmap

    fun all(): Iterable<Pair<Int, Pixmap>> = ids.map { it to get(it) }

}