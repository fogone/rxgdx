package ru.nobirds.rx.gl.font.meta

data class FontMetaPadding(val top: Int = 0, val left: Int = 0, val bottom: Int = 0, val right: Int = 0)
data class FontMetaSpacing(val horizontal: Int = 0, val vertical: Int = 0)
data class FontMetaMetrics(
        val face: String,
        val size: Int,
        val bold: Boolean,
        val italic: Boolean,
        val outline: Int,
        val stretchHeight: Int,
        val lineHeight: Int,
        val base: Int,
        val scaleWidth: Int,
        val scaleHeight: Int,
        val ascent: Int,
        val descent: Int,
        val capitalLetterHeight: Int,
        val padding: FontMetaPadding,
        val spacing: FontMetaSpacing
)