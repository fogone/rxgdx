package ru.nobirds.rx.gl.font.meta

interface FontMetaKerning {

    fun get(first: Char, second: Char): Int

}

class PredefinedFontMetaKerning(val kerning: Map<Char, Map<Char, Int>>, val default: Int = 0) : FontMetaKerning {

    override fun get(first: Char, second: Char): Int = kerning.get(first)?.get(second) ?: default

}