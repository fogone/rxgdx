package ru.nobirds.rx.gl.utils

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.utils.BufferUtils

interface TextureSlotsManager {

    fun binded(texture: Int): Boolean

    fun bind(texture: Int): Int

    fun bind(texture: Int, slot: Int): Int

    fun unbind(texture: Int)

    fun hasAvailableSlots(count: Int = 1): Boolean

    fun clean()

}

class SimpleTextureSlotsManager : TextureSlotsManager {

    private val MAX_GL_ES_UNITS = 32

    private val cache = hashMapOf<Int, Int>()
    private val slots = hashSetOf<Int>()

    private val capacity = 0 .. Math.min(glMaxCapacity(), MAX_GL_ES_UNITS) - 1

    override fun binded(texture: Int): Boolean = cache.containsKey(texture)

    override fun bind(texture: Int): Int {
        cache[texture]?.let { slot ->
            return slot
        }

        return bindImpl(texture, findFreeSlot())
    }

    private fun findFreeSlot(): Int = capacity.first { it !in slots } // todo: optimize

    override fun bind(texture: Int, slot: Int): Int {
        val prevSlot = cache[texture]

        if (prevSlot == slot) {
            return slot
        }

        if (prevSlot != null) {
            unbindBySlot(prevSlot)
        }

        return bindImpl(texture, slot)
    }

    private fun bindImpl(texture: Int, slot: Int): Int {
        slots.add(slot)
        cache[texture] = slot

        glBind(texture, slot)

        return slot
    }

    private fun glBind(texture: Int, slot: Int) {
        Gl.glActiveTexture(GL20.GL_TEXTURE0 + slot)
        Gl.glBindTexture(GL20.GL_TEXTURE_2D, texture)
    }

    override fun unbind(texture: Int) {
        val slot = cache.remove(texture)

        if (slot != null) {
            unbindBySlot(slot)
        }
    }

    private fun unbindBySlot(slot: Int) {
        slots.remove(slot)
        unbindBySlotImpl(slot)
    }

    private fun unbindBySlotImpl(slot: Int) {
        glBind(0, slot)
    }

    private fun glMaxCapacity(): Int {
        val buffer = BufferUtils.newIntBuffer(16)
        Gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_IMAGE_UNITS, buffer)
        return buffer.get(0)
    }

    override fun hasAvailableSlots(count: Int): Boolean = slots.size + count in capacity

    override fun clean() {
        cache.clear()
        slots.clear()
    }

}
