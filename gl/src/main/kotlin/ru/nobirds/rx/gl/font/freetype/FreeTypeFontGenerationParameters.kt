package ru.nobirds.rx.gl.font.freetype

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.freetype.FreeType
import ru.nobirds.rx.gl.texture.Texture

data class FreeTypeFontGenerationParameters(

        /** The size in pixels  */
        val size: Int = 16,

        /** If true, font smoothing is disabled.  */
        val mono: Boolean = false,

        /** Strength of hinting when smoothing is enabled  */
        val hinting: FontHinting = FontHinting.None,

        /** Foreground color (required for non-black borders)  */
        val color: Color = Color.WHITE,

        /** Glyph gamma. Values > 1 reduce antialiasing.  */
        val gamma: Float = 1.8f,

        /** Number of times to render the glyph. Useful with a shadow or border, so it doesn't show through the glyph.  */
        val renderCount: Int = 2,

        /** Border width in pixels, 0 to disable  */
        val borderWidth: Float = 0f,

        /** Border color; only used if borderWidth > 0  */
        val borderColor: Color = Color.BLACK,

        /** true for straight (mitered), false for rounded borders  */
        val borderStraight: Boolean = false,

        /** Values < 1 increase the border size.  */
        val borderGamma: Float = 1.8f,

        /** Offset of text shadow on X axis in pixels, 0 to disable  */
        val shadowOffsetX: Int = 0,

        /** Offset of text shadow on Y axis in pixels, 0 to disable  */
        val shadowOffsetY: Int = 0,

        /** Shadow color; only used if shadowOffset > 0  */
        val shadowColor: Color = Color(0f, 0f, 0f, 0.75f),

        /** Pixels to add to glyph spacing. Can be negative.  */
        val spaceX: Int = 0,
        val spaceY: Int = 0,

        /** The characters the font should contain  */
        val characters: String = FreeTypeSupport.DEFAULT_CHARS,

        /** Whether the font should include kerning  */
        val kerning: Boolean = true,

        /** Whether to flip the font vertically  */
        val flip: Boolean = false,

        /** Whether to generate mip maps for the resulting texture  */
        val genMipMaps: Boolean = false,

        /** Minification filter  */
        val minFilter: Texture.Filter = Texture.Filter.Nearest,

        /** Magnification filter  */
        val magFilter: Texture.Filter = Texture.Filter.Nearest,

        /** When true, glyphs are rendered on the fly to the font's glyph page textures as they are needed. The
         * FreeTypeFontGenerator must not be disposed until the font is no longer needed. The FreeTypeBitmapFontData must be
         * disposed (separately from the generator) when the font is no longer needed. The FreeTypeFontParameter should not be
         * modified after creating a font.*/
        val incremental: Boolean = false
)

/** Font smoothing algorithm.  */
enum class FontHinting(val flags: Int) {

    /** Disable hinting. Generated glyphs will look blurry.  */
    None(FreeType.FT_LOAD_DEFAULT or FreeType.FT_LOAD_NO_HINTING),

    /** Light hinting with fuzzy edges, but close to the original shape  */
    Slight(FreeType.FT_LOAD_DEFAULT or FreeType.FT_LOAD_FORCE_AUTOHINT or FreeType.FT_LOAD_TARGET_LIGHT),

    /** Default hinting  */
    Medium(FreeType.FT_LOAD_DEFAULT or FreeType.FT_LOAD_FORCE_AUTOHINT or FreeType.FT_LOAD_TARGET_NORMAL),

    /** Strong hinting with crisp edges at the expense of shape fidelity  */
    Full(FreeType.FT_LOAD_DEFAULT or FreeType.FT_LOAD_FORCE_AUTOHINT or FreeType.FT_LOAD_TARGET_MONO)
}