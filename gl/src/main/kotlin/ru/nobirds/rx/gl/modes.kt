package ru.nobirds.rx.gl

import com.badlogic.gdx.graphics.GL20


enum class DrawMode(val gl: Int) {
    static(GL20.GL_STATIC_DRAW), dynamic(GL20.GL_DYNAMIC_DRAW)
}

enum class DrawPrimitive(val gl:Int) {

    points(GL20.GL_POINTS),
    lineStrip(GL20.GL_LINE_STRIP),
    lineLoop(GL20.GL_LINE_LOOP),
    lines(GL20.GL_LINES),
    triangleStrip(GL20.GL_TRIANGLE_STRIP),
    triangleFan(GL20.GL_TRIANGLE_FAN),
    triangles(GL20.GL_TRIANGLES)

}
