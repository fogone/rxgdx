package ru.nobirds.rx.gl.types

import ru.nobirds.rx.gl.AttributeType
import ru.nobirds.rx.gl.utils.Gl
import ru.nobirds.rx.gl.utils.GlType

class AttributeTypeImpl(override val type: Int, override val size: Int, val components:Int,
                        val glType: GlType, val options: AttributeTypeOptions) : AttributeType {

    private val inputType = options.glType ?: glType

    override val bytesSize: Int
        get() = (components * inputType.size) * size

    override fun structure(location: Int, stride:Int, offset: Int) {
        Gl.glVertexAttribPointer(location, components, inputType.glHandle, options.normalized, stride, offset)
    }

}

fun attributeType(type: Int, size: Int, components:Int, glType: GlType, options: AttributeTypeOptions): AttributeType =
        AttributeTypeImpl(type, size, components, glType, options)