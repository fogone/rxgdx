package ru.nobirds.rx.gl.font

import ru.nobirds.rx.gl.font.meta.FontMetaChar
import ru.nobirds.rx.gl.font.meta.FontMetaInfo
import ru.nobirds.rx.gl.font.meta.FontMetaMetrics
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.texture.TextureRegion
import ru.nobirds.rx.gl.texture.region

interface Font {

    val metrics: FontMetaMetrics

    fun available(char: Char): Boolean

    fun glyph(char: Char): FontGlyph

    fun kerning(first: Char, second: Char): Int

    companion object {

        fun from(meta: FontMetaInfo): Font {

            val pages = meta.pages.all().map { (id, pixmap) -> id to Texture.from(pixmap) }.toMap()

            val glyphs = meta.chars.availableNow.map { char ->

                val texture = pages[char.pageId] ?:
                        throw IllegalStateException("Page with id ${char.pageId} not found")

                val glyphRegion = texture.region(char.x, char.y, char.width, char.height)

                FontGlyphImpl(char, glyphRegion)
            }

            return FontImpl(meta, glyphs)
        }

    }

}

interface FontGlyph {

    val region: TextureRegion

    val char: FontMetaChar

}


internal class FontImpl(val meta: FontMetaInfo, glyphs: Iterable<FontGlyph>) : Font {

    private val hash = glyphs.associateBy { it.char.char }

    override val metrics: FontMetaMetrics
        get() = meta.metrics

    override fun kerning(first: Char, second: Char): Int = meta.kerning.get(first, second)

    override fun available(char: Char): Boolean = char in hash

    override fun glyph(char: Char): FontGlyph = hash[char] ?:
            throw IllegalArgumentException("No glyph for char '$char' (${char.toInt()}) in font ${meta.name}")

}

internal data class FontGlyphImpl(
        override val char: FontMetaChar,
        override val region: TextureRegion) : FontGlyph
