package ru.nobirds.rx.gl.buffer

import ru.nobirds.rx.gl.Bindable
import ru.nobirds.rx.gl.Disposable
import java.nio.Buffer

interface DataBuffer<in B: Buffer> : Disposable, Bindable {

    val size:Int

    val sizeInBytes:Int
        get() = size * bufferElementSize

    val bufferElementSize:Int

    fun set(buffer: B)

}