package ru.nobirds.rx.gl.types

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.gl.AttributeType
import ru.nobirds.rx.gl.utils.GlTypes

interface AttributeTypeRegistry {

    fun get(type:Int, size:Int, options: AttributeTypeOptions): AttributeType

}

class SimpleAttributeTypeRegistry() : AttributeTypeRegistry {

    private val index = mapOf(
            /* floats */
            GL20.GL_FLOAT      to { s: Int, a: AttributeTypeOptions -> attributeType(GL20.GL_FLOAT,      s, 1, GlTypes.FLOAT, a) },
            GL20.GL_FLOAT_VEC2 to { s: Int, a: AttributeTypeOptions -> attributeType(GL20.GL_FLOAT_VEC2, s, 2, GlTypes.FLOAT, a) },
            GL20.GL_FLOAT_VEC3 to { s: Int, a: AttributeTypeOptions -> attributeType(GL20.GL_FLOAT_VEC3, s, 3, GlTypes.FLOAT, a) },
            GL20.GL_FLOAT_VEC4 to { s: Int, a: AttributeTypeOptions -> attributeType(GL20.GL_FLOAT_VEC4, s, 4, GlTypes.FLOAT, a) },

            /* ints */
            GL20.GL_INT      to { s: Int, a: AttributeTypeOptions -> attributeType(GL20.GL_INT,      s, 1, GlTypes.INT, a) },
            GL20.GL_INT_VEC2 to { s: Int, a: AttributeTypeOptions -> attributeType(GL20.GL_INT_VEC2, s, 2, GlTypes.INT, a) },
            GL20.GL_INT_VEC3 to { s: Int, a: AttributeTypeOptions -> attributeType(GL20.GL_INT_VEC3, s, 3, GlTypes.INT, a) },
            GL20.GL_INT_VEC4 to { s: Int, a: AttributeTypeOptions -> attributeType(GL20.GL_INT_VEC4, s, 4, GlTypes.INT, a) }
    )

    override fun get(type:Int, size:Int, options: AttributeTypeOptions): AttributeType {
        val typed = index[type] ?: throw IllegalArgumentException("Unsupported type: $type")
        return typed(size, options)
    }

}
