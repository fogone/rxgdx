package ru.nobirds.rx.gl.font.freetype

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.g2d.freetype.FreeType
import com.badlogic.gdx.utils.Disposable
import ru.nobirds.rx.gl.font.meta.FontMetaChar
import ru.nobirds.rx.gl.font.meta.FontMetaInfo
import ru.nobirds.rx.gl.font.meta.FontMetaMetrics
import ru.nobirds.rx.gl.font.meta.FontMetaPacker
import ru.nobirds.rx.gl.font.meta.FontMetaPadding
import ru.nobirds.rx.gl.font.meta.FontMetaPages
import ru.nobirds.rx.gl.font.meta.FontMetaSpacing
import ru.nobirds.rx.gl.font.meta.PredefinedFontMetaChars
import ru.nobirds.rx.gl.font.meta.PredefinedFontMetaKerning
import ru.nobirds.rx.gl.utils.isFlagSet
import kotlin.experimental.and

private class FontMetaPagesManager : FontMetaPages {

    private var index = 1
    private val pages = hashMapOf<Int, Pixmap>()

    fun createPage(pixmap: Pixmap): Int {
        val page = index++
        pages.put(page, pixmap)
        return page
    }

    override val ids: Iterable<Int>
        get() = pages.keys

    override fun get(id: Int): Pixmap = pages[id] ?: throw IllegalArgumentException()

}

class FreeTypeFontGenerator(val name: String, val face: FreeType.Face) : Disposable {

    private val isBitmapFont = face.isBitmapFont()

    fun generate(parameters: FreeTypeFontGenerationParameters, packer: FontMetaPacker): FontMetaInfo {

        if (parameters.incremental) {
            TODO("Not yet implemented: return FontMetaInfo(metrics, LazyFontMetaPages(), ...)")
        }

        val pageManager = FontMetaPagesManager()

        val size = face.size
        val metrics = size.metrics

        face.setPixelSizes(0, parameters.size)

        val fontMetaMetrics = FontMetaMetrics(
                name,
                metrics.height.toSignableInt(),
                face.styleFlags.isFlagSet(FreeType.FT_STYLE_FLAG_BOLD),
                face.styleFlags.isFlagSet(FreeType.FT_STYLE_FLAG_ITALIC),
                1, // todo
                0, // todo
                metrics.ascender.toSignableInt(),
                0, // todo
                metrics.xScale.toSignableInt(),
                metrics.yscale.toSignableInt(),
                metrics.ascender.toSignableInt(),
                metrics.descender.toSignableInt(),
                metrics.height.toSignableInt(),
                FontMetaPadding(),
                FontMetaSpacing()
        )

        val generatedChars = generateChars(parameters, pageManager, fontMetaMetrics)

        val chars = PredefinedFontMetaChars(generatedChars)

        val kerning = fetchKerning(generatedChars)
        
        val metaInfo = FontMetaInfo(
                name,
                fontMetaMetrics,
                pageManager,
                chars,
                PredefinedFontMetaKerning(kerning)
        )

        return metaInfo // todo: packer.pack(metaInfo) !!!
    }

    private fun fetchKerning(chars: Iterable<FontMetaChar>): Map<Char, Map<Char, Int>> = chars
            .associateBy(FontMetaChar::char) { val left = it.char
                chars.associateBy(FontMetaChar::char) { val right = it.char
                    face.getKerning(left.toInt(), right.toInt(), 0).toSignableInt()
                }
            }
            .map { it.key to it.value.filter { it.value != 0 } }
            .filter { it.second.isNotEmpty() }
            .toMap()

    private fun generateChars(parameters: FreeTypeFontGenerationParameters,
                              pageManager: FontMetaPagesManager,
                              fontMetaMetrics: FontMetaMetrics): List<FontMetaChar> {

        return parameters
                .characters
                .map(Char::toInt)
                .filter { face.getCharIndex(it) != 0 }
                .map { generateChar(it, parameters, pageManager, fontMetaMetrics) }
    }

    private fun generateChar(char: Int,
                             parameters: FreeTypeFontGenerationParameters,
                             pageManager: FontMetaPagesManager,
                             fontMetaMetrics: FontMetaMetrics): FontMetaChar {

        val (slot, glyph, pixmap) = face.createGlyphAndPixmap(char, parameters, isBitmapFont) {
            if (parameters.bordersEnabled) {
                withBorder(FreeTypeSupport.strocker)
            }
            if (parameters.shadowsEnabled) {
                withShadows()
            }
        }

        val pageId = pageManager.createPage(pixmap)

        val metaChar = FontMetaChar(
                char.toChar(),
                0, 0,
                pixmap.width, pixmap.height,
                glyph.left, fontMetaMetrics.lineHeight - glyph.top,
                slot.metrics.horiAdvance.toSignableInt(),
                pageId
        )

        return metaChar
    }

    override fun dispose() {
        face.dispose()
    }

}

fun FreeType.Face.isBitmapFont(): Boolean {
    val fixedSizeAndHorizontal = with(faceFlags) {
        isFlagSet(FreeType.FT_FACE_FLAG_FIXED_SIZES) &&
                isFlagSet(FreeType.FT_FACE_FLAG_HORIZONTAL)
    }

    return fixedSizeAndHorizontal
            && loadChar(32, FreeType.FT_LOAD_DEFAULT or FreeType.FT_LOAD_FORCE_AUTOHINT)
            && glyph.format == 1651078259
}

data class SlotGlyphPixmap(val slot: FreeType.GlyphSlot, val glyph: FreeType.Glyph, val pixmap: Pixmap)

private fun FreeType.Face.createGlyphAndPixmap(char: Int,
                                               parameters: FreeTypeFontGenerationParameters,
                                               isBitmapFont: Boolean,
                                               builder: FreeTypeGlyphSlotBuilder.()->Unit): SlotGlyphPixmap {

    return FreeTypeGlyphSlotBuilder(char, this, parameters, isBitmapFont).apply(builder).build()
}

private class FreeTypeGlyphSlotBuilder(val char: Int,
                                       val face: FreeType.Face,
                                       val parameters: FreeTypeFontGenerationParameters,
                                       val isBitmapFont: Boolean) {

    private val slot = createSlot(char)

    private fun createSlot(char: Int): FreeType.GlyphSlot {
        require(face.getCharIndex(char) != 0) { "No ${char.toChar()} in this font." }
        require(face.loadChar(char, parameters.hinting.flags))
            { "Couldn't load char ${char.toChar()} for font" }

        val slot = face.glyph

        if (!isBitmapFont) {
            require(slot.renderGlyph(FreeType.FT_RENDER_MODE_NORMAL))
                { "Couldn't render glyph for char ${char.toChar()}" }
        }

        return slot
    }

    private var resultGlyph: FreeType.Glyph = slot.glyph
        set(value) {
            field.dispose()
            field = value
        }

    private var result: Pixmap = createMain(resultGlyph)
        set(value) {
            field.dispose()
            field = value
        }

    private fun createMain(glyph: FreeType.Glyph): Pixmap {
        return glyph.render(parameters.renderMode, parameters.color, parameters.gamma)
    }

    fun withBorder(stroker: FreeType.Stroker) {
        val borderGlyph = slot.glyph

        borderGlyph.strokeBorder(stroker, false)
        val offsetX = resultGlyph.left - borderGlyph.left
        val offsetY = -(resultGlyph.top - borderGlyph.top)

        val borderPixmap = borderGlyph.render(parameters.renderMode, parameters.borderColor, parameters.borderGamma)

        repeat(parameters.renderCount) {
            borderPixmap.drawPixmap(result, offsetX, offsetY)
        }

        resultGlyph = borderGlyph
        result = borderPixmap
    }

    fun withShadows() {
        val width = result.width
        val height = result.height

        val shadowOffsetX = Math.max(parameters.shadowOffsetX, 0)
        val shadowOffsetY = Math.max(parameters.shadowOffsetY, 0)

        val shadowWidth = width + Math.abs(parameters.shadowOffsetX)
        val shadowHeight = height + Math.abs(parameters.shadowOffsetY)

        val shadowPixmap = Pixmap(shadowWidth, shadowHeight, result.format)

        val shadowColor = parameters.shadowColor

        val r = (shadowColor.r * 255).toByte()
        val g = (shadowColor.g * 255).toByte()
        val b = (shadowColor.b * 255).toByte()
        val a = shadowColor.a

        val mainPixels = result.pixels
        val shadowPixels = shadowPixmap.pixels

        for (y in 0..height - 1) {
            val shadowRow = shadowWidth * (y + shadowOffsetY) + shadowOffsetX
            for (x in 0..width - 1) {
                val mainPixel = (width * y + x) * 4
                val mainA = mainPixels.get(mainPixel + 3)
                if (mainA.toInt() == 0) continue
                val shadowPixel = (shadowRow + x) * 4

                shadowPixels.put(shadowPixel, r)
                shadowPixels.put(shadowPixel + 1, g)
                shadowPixels.put(shadowPixel + 2, b)
                shadowPixels.put(shadowPixel + 3, ((mainA and 0xff.toByte()) * a).toByte())
            }
        }

        repeat(parameters.renderCount) {
            shadowPixmap.drawPixmap(
                    result,
                    Math.max(-parameters.shadowOffsetX, 0),
                    Math.max(-parameters.shadowOffsetY, 0)
            )
        }

        result = shadowPixmap
    }

    fun build(): SlotGlyphPixmap = SlotGlyphPixmap(slot, resultGlyph, result)

}

private val FreeTypeFontGenerationParameters.renderMode: Int
    get() = if (mono) FreeType.FT_RENDER_MODE_MONO else FreeType.FT_RENDER_MODE_NORMAL

private val FreeTypeFontGenerationParameters.bordersEnabled: Boolean
    get() = borderWidth > 0

private val FreeTypeFontGenerationParameters.shadowsEnabled: Boolean
    get() = shadowOffsetX != 0 || shadowOffsetY != 0

fun FreeType.Glyph.render(renderMode: Int, color: Color, gamma: Float): Pixmap {
    toBitmap(renderMode)
    return bitmap.getPixmap(Pixmap.Format.RGBA8888, color, gamma)
}

fun Int.toSignableInt():Int = FreeType.toInt(this)