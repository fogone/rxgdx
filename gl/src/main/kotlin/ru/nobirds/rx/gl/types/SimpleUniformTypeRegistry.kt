package ru.nobirds.rx.gl.types

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.gl.UniformType
import ru.nobirds.rx.gl.utils.TextureSlotsManager

interface  UniformTypeRegistry {

    fun get(type:Int, size:Int): UniformType

}

class SimpleUniformTypeRegistry(val textureSlotsManager: TextureSlotsManager) : UniformTypeRegistry {

    private val index = mapOf(
            /* floats */
            GL20.GL_FLOAT to { s: Int -> PrimitiveUniformType(GL20.GL_FLOAT, s, Float::class, GL20::glUniform1f) },
            GL20.GL_FLOAT_VEC2 to { s: Int -> floatVectorUniform(GL20.GL_FLOAT_VEC2, s, 2, GL20::glUniform2fv, GL20::glUniform2fv) },
            GL20.GL_FLOAT_VEC3 to { s: Int -> floatVectorUniform(GL20.GL_FLOAT_VEC3, s, 3, GL20::glUniform3fv, GL20::glUniform3fv) },
            GL20.GL_FLOAT_VEC4 to { s: Int -> floatVectorUniform(GL20.GL_FLOAT_VEC4, s, 4, GL20::glUniform4fv, GL20::glUniform4fv) },

            GL20.GL_FLOAT_MAT2 to { s: Int -> floatMatrixUniform(GL20.GL_FLOAT_MAT2, s, 2, GL20::glUniformMatrix2fv, GL20::glUniformMatrix2fv) },
            GL20.GL_FLOAT_MAT3 to { s: Int -> floatMatrixUniform(GL20.GL_FLOAT_MAT3, s, 3, GL20::glUniformMatrix3fv, GL20::glUniformMatrix3fv) },
            GL20.GL_FLOAT_MAT4 to { s: Int -> floatMatrixUniform(GL20.GL_FLOAT_MAT4, s, 4, GL20::glUniformMatrix4fv, GL20::glUniformMatrix4fv) },

            /* ints */
            GL20.GL_INT to { s: Int -> PrimitiveUniformType(GL20.GL_INT, s, Int::class, GL20::glUniform1i) },
            GL20.GL_INT_VEC2 to { s: Int -> intVectorUniform(GL20.GL_INT_VEC2, s, 2, GL20::glUniform2iv, GL20::glUniform2iv) },
            GL20.GL_INT_VEC3 to { s: Int -> intVectorUniform(GL20.GL_INT_VEC3, s, 3, GL20::glUniform3iv, GL20::glUniform3iv) },
            GL20.GL_INT_VEC4 to { s: Int -> intVectorUniform(GL20.GL_INT_VEC4, s, 4, GL20::glUniform4iv, GL20::glUniform4iv) },

            GL20.GL_SAMPLER_2D to { s: Int -> if (s == 1) Sampler2DUniformType(textureSlotsManager) else Sampler2DArrayUniformType(s, textureSlotsManager) }
    )

    override fun get(type:Int, size:Int): UniformType {
        val typed = index[type] ?: throw IllegalArgumentException("Unsupported type: $type")
        return typed(size)
    }

}