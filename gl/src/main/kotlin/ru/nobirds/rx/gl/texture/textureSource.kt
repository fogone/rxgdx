package ru.nobirds.rx.gl.texture

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.PixmapIO
import com.badlogic.gdx.graphics.glutils.MipMapGenerator
import ru.nobirds.rx.gl.Disposable
import ru.nobirds.rx.gl.bind
import ru.nobirds.rx.gl.utils.Gl
import ru.nobirds.rx.utils.files.ImmutableResource
import ru.nobirds.rx.utils.files.extension
import ru.nobirds.rx.utils.files.readBytes

interface TextureSource : Disposable {

    val width: Int
    val height: Int

    fun loadTo(texture: Texture)

    companion object {

        fun empty(format: Pixmap.Format, height: Int, width: Int): TextureSource =
                PixmapTextureSource(Pixmap(width, height, format))

        fun from(pixmap: Pixmap): TextureSource = PixmapTextureSource(pixmap)

        private fun ImmutableResource.loadPixmapFromResource(): Pixmap {
            val bytes = readBytes()
            return Pixmap(bytes, 0, bytes.size)
        }

        private fun ImmutableResource.loadPixmap(): Pixmap =
                if (extension.equals("cim", true))
                    PixmapIO.readCIM(toFileHandle())
                else loadPixmapFromResource()

        private fun Pixmap.convertTo(format: Pixmap.Format? = null): Pixmap =
                if (format != null) convert(format) else this

        fun from(resource: ImmutableResource,
                 format: Pixmap.Format? = null,
                 generateMipMap: Boolean = false,
                 disposeAfterLoad: Boolean = false): TextureSource {

            val pixmap = resource.loadPixmap().convertTo(format)

            return if(generateMipMap)
                MipmapTextureSource(pixmap, disposeAfterLoad)
            else
                PixmapTextureSource(pixmap, disposeAfterLoad)
        }

    }

}


abstract class AbstractPixmapTextureSource(val pixmap: Pixmap, val disposeAfterLoad: Boolean = false) : TextureSource {

    private var disposed = false

    override val width: Int
        get() = pixmap.width

    override val height: Int
        get() = pixmap.height

    override fun loadTo(texture: Texture) {
        require(!disposed) { "This source can't be reused, because it was disposed" }

        texture.bind {
            Gl.glPixelStorei(GL20.GL_UNPACK_ALIGNMENT, 1)
            loadImpl(texture.target, pixmap)
        }

        if(disposeAfterLoad)
            dispose()
    }

    protected abstract fun loadImpl(target: Int, pixmap: Pixmap)

    override fun dispose() {
        disposed = true
        pixmap.dispose()
    }

}

class PixmapTextureSource(pixmap: Pixmap, disposeAfterLoad: Boolean = false) :
        AbstractPixmapTextureSource(pixmap, disposeAfterLoad) {

    override fun loadImpl(target: Int, pixmap: Pixmap) {
        Gl.glTexImage2D(
                target,
                0,
                pixmap.glInternalFormat,
                pixmap.width,
                pixmap.height,
                0,
                pixmap.glFormat,
                pixmap.glType,
                pixmap.pixels
        )
    }

}

class MipmapTextureSource(pixmap: Pixmap, disposeAfterLoad: Boolean = false) :
        AbstractPixmapTextureSource(pixmap, disposeAfterLoad) {

    override fun loadImpl(target: Int, pixmap: Pixmap) {
        MipMapGenerator.generateMipMap(target, pixmap, pixmap.width, pixmap.height)
    }

}