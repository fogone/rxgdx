package ru.nobirds.rx.gl

interface Disposable {

    fun dispose()

}