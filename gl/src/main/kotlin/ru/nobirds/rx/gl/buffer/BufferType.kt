package ru.nobirds.rx.gl.buffer

import com.badlogic.gdx.graphics.GL20

enum class BufferType(val gl:Int) {

    Array(GL20.GL_ARRAY_BUFFER),
    ElementArray(GL20.GL_ELEMENT_ARRAY_BUFFER)

}

