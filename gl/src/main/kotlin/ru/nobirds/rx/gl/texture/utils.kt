package ru.nobirds.rx.gl.texture

import com.badlogic.gdx.graphics.Pixmap

fun Pixmap.convert(format: Pixmap.Format): Pixmap {
    return if (this.format == format) this
    else Pixmap(width, height, format).apply {
        withBlending(Pixmap.Blending.None) {
            drawPixmap(this, 0, 0, 0, 0, this.width, this.height)
        }
    }
}

private inline fun <R> withBlending(blending: Pixmap.Blending, block: () -> R): R {
    val blend = Pixmap.getBlending()
    try {
        Pixmap.setBlending(blending)
        return block()
    } finally {
        Pixmap.setBlending(blend)
    }
}
