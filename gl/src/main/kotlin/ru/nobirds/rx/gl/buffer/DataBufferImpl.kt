package ru.nobirds.rx.gl.buffer

import ru.nobirds.rx.gl.DrawMode
import ru.nobirds.rx.gl.bind
import ru.nobirds.rx.gl.utils.Gl
import java.nio.Buffer

open class DataBufferImpl<in B: Buffer>(
        val bufferType: BufferType,
        buffer: B,
        val drawMode: DrawMode,
        override val bufferElementSize:Int
) : DataBuffer<B> {

    private val handle = Gl.glGenBuffer()

    private var sizeField:Int = 0
    override val size:Int
        get() = sizeField

    init {
        set(buffer)
    }

    override fun set(buffer: B) {
        bind {
            this.sizeField = buffer.limit()
            Gl.glBufferData(bufferType.gl, size, buffer, drawMode.gl)
        }
    }

    override fun bind() {
        Gl.glBindBuffer(bufferType.gl, handle)
    }

    override fun unbind() {
        Gl.glBindBuffer(bufferType.gl, 0)
    }

    override fun dispose() {
        Gl.glDeleteBuffer(handle)
    }

}