package ru.nobirds.rx.gl.buffer

import ru.nobirds.rx.gl.DrawMode
import java.nio.ByteBuffer

class ArrayBuffer(buffer: ByteBuffer, drawMode: DrawMode) :
        DataBufferImpl<ByteBuffer>(BufferType.Array, buffer, drawMode, 1)