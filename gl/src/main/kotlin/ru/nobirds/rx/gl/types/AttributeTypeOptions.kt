package ru.nobirds.rx.gl.types

import ru.nobirds.rx.gl.utils.GlType

data class AttributeTypeOptions(val normalized:Boolean = false, val glType: GlType? = null)