package ru.nobirds.rx.gl.utils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.GL30
import ru.nobirds.rx.utils.poolable

val Gl: GL20 by lazy { Gdx.gl }
val Gl3: GL30 by lazy { Gdx.gl30 }

object Gls {
    val gl3Supported: Boolean
        get() = Gdx.gl30 != null

    fun enableBlend() {
        Gl.glEnable(GL20.GL_BLEND)
        Gl.glBlendFunc (GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
    }

    fun clear() {
        Gl.apply {
            glClearColor(0f, 0f, 0f, 1f)
            glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)
        }
    }

    fun viewport(screenX: Float, screenY: Float, screenWidth: Float, screenHeight: Float) {
        Gl.glViewport(screenX.toInt(), screenY.toInt(), screenWidth.toInt(), screenHeight.toInt())
    }

}

class GlException(message:String, code:Int) : RuntimeException("GlError $code $message")

inline fun GL20.throwOnError(block: GL20.()->Unit) {
    block()

    val error = glGetError()

    if (error != GL20.GL_NO_ERROR) {
        val message = when (error) {
            GL20.GL_INVALID_VALUE -> "GL_INVALID_VALUE"
            GL20.GL_INVALID_OPERATION -> "GL_INVALID_OPERATION"
            GL20.GL_INVALID_FRAMEBUFFER_OPERATION -> "GL_INVALID_FRAMEBUFFER_OPERATION"
            GL20.GL_INVALID_ENUM -> "GL_INVALID_ENUM"
            GL20.GL_OUT_OF_MEMORY -> "GL_OUT_OF_MEMORY"
            else -> "UNKNOWN"
        }

        throw GlException(message, error)
    }
}


fun GL30.glDeleteVertexArray(handle:Int) = poolable {
    glDeleteVertexArrays(1, obtaintIntRef(handle).forRead())
}

fun GL30.glGenVertexArray():Int = glInt { glGenVertexArrays(1, it.forWrite()) }

private inline fun <G:GL20> G.glInt(func: G.(IntRef)->Unit): Int = poolable {
    val intRef = obtaintIntRef()
    func(intRef)
    intRef.value
}

fun GL20.glGetShaderiv(shader: Int, pname: Int): Int = glInt {
    glGetShaderiv(shader, pname, it.forWrite())
}

fun GL20.glGetProgramiv(program: Int, attribute: Int): Int = glInt {
    glGetProgramiv(program, attribute, it.forWrite())
}
