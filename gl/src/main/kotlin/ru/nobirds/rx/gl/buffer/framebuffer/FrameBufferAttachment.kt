package ru.nobirds.rx.gl.buffer.framebuffer

import ru.nobirds.rx.gl.Bindable
import ru.nobirds.rx.gl.Disposable
import ru.nobirds.rx.gl.Glable

interface FrameBufferAttachment : Bindable, Disposable, Glable {

    fun attach()

}