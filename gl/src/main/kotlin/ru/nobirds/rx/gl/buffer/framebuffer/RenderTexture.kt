package ru.nobirds.rx.gl.buffer.framebuffer

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.utils.Gl

class RenderTexture<out T: Texture>(val texture: T, val index: Int = 0) : FrameBufferAttachment, Texture by texture {

    override fun attach() {
        Gl.glFramebufferTexture2D(GL20.GL_FRAMEBUFFER, GL20.GL_COLOR_ATTACHMENT0 + index,
                GL20.GL_TEXTURE_2D, texture.gl, 0)
    }

}