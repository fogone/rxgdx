package ru.nobirds.rx.gl.types

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.utils.BufferUtils
import ru.nobirds.rx.gl.UniformType
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.utils.Gl
import ru.nobirds.rx.gl.utils.TextureSlotsManager
import ru.nobirds.rx.gl.utils.throwOnError


class Sampler2DUniformType(val textureSlotsManager: TextureSlotsManager) : UniformType {

    override fun set(location: Int, value: Any) {
        when (value) {
            is Int -> setTexture(location, value)
            is Texture -> setTexture(location, value.gl)
            is TextureWithSlot -> setTexture(location, value.texture, value.slot)
            else -> throw IllegalArgumentException(
                    "Unsupported type of parameter: required GLTexture compatible, but passed ${value.javaClass}")
        }
    }

    private fun setTexture(location: Int, value: Int, textureSlot:Int? = null) {
        val slot = if(textureSlot != null)
            textureSlotsManager.bind(value, textureSlot)
        else
            textureSlotsManager.bind(value)

        Gl.throwOnError { Gl.glUniform1i(location, slot) }
    }

    override val type: Int
        get() = GL20.GL_SAMPLER_2D

    override val size: Int
        get() = 1

}

class Sampler2DArrayUniformType(override val size: Int, val textureSlotsManager: TextureSlotsManager) : UniformType {

    override fun set(location: Int, value: Any) {
        when (value) {
            is IntArray -> setTextures(location, value)
            else -> throw IllegalArgumentException(
                    "Unsupported type of parameter: required array of GLTexture, but passed ${value.javaClass}")
        }
    }

    private fun setTextures(location: Int, value: IntArray) {
        require(value.size == size)

        val slots = value
                .map { textureSlotsManager.bind(it) }
                .toIntArray()

        // todo: use array after https://github.com/libgdx/libgdx/issues/3801 will be fixed
        val buffer = BufferUtils.newIntBuffer(slots.size)
        buffer.put(slots)
        buffer.flip()

        Gl.throwOnError { Gl.glUniform1iv(location, buffer.limit(), buffer) }
    }

    override val type: Int
        get() = GL20.GL_SAMPLER_2D

}