package ru.nobirds.rx.gl.utils

fun Int.isFlagSet(flag: Int):Boolean = this and flag == flag
