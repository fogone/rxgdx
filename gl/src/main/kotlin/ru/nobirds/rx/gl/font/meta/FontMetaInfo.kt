package ru.nobirds.rx.gl.font.meta


data class FontMetaInfo(
        val name: String,
        val metrics: FontMetaMetrics,
        val pages: FontMetaPages,
        val chars: FontMetaChars,
        val kerning: FontMetaKerning
)