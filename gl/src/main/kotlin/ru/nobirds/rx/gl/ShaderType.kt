package ru.nobirds.rx.gl

import com.badlogic.gdx.graphics.GL20

enum class ShaderType(val glType:Int) {
    vertex(GL20.GL_VERTEX_SHADER),
    fragment(GL20.GL_FRAGMENT_SHADER)
}

