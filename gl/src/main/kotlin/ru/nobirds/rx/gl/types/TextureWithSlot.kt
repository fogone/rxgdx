package ru.nobirds.rx.gl.types

data class TextureWithSlot(val texture: Int, val slot:Int)
