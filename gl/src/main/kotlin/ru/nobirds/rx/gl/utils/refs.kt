package ru.nobirds.rx.gl.utils

import ru.nobirds.rx.utils.PoolContext
import ru.nobirds.rx.utils.PoolFactory
import ru.nobirds.rx.utils.Poolable
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.IntBuffer
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun PoolContext.obtaintIntRef(value: Int = 0): IntRef = obtain { this.value = value }

class IntRef(initialValue: Int = 0) : Poolable, ReadWriteProperty<Any?, Int> {

    private val buffer: IntBuffer = ByteBuffer.allocateDirect(4).let {
        it.order(ByteOrder.nativeOrder())
        it.asIntBuffer()
    }

    override fun reset() {
        value = 0
        buffer.clear()
    }

    var value: Int
        get() = buffer.get(0)
        set(value) {
            buffer.put(0, value)
        }

    init {
        this.value = initialValue
    }

    fun forWrite(): IntBuffer = buffer.apply { clear() }
    fun forRead(): IntBuffer = buffer

    override fun getValue(thisRef: Any?, property: KProperty<*>): Int = value

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
        this.value = value
    }

    override fun toString(): String {
        return "IntRef($value)"
    }

    companion object : PoolFactory<IntRef> {
        override fun create(): IntRef = IntRef()
    }


}