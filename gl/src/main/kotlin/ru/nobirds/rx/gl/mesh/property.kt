package ru.nobirds.rx.gl.mesh

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun <T> Mesh.property(name:String, value:T, converter:T.()->Any? = { this }): ReadWriteProperty<Any, T> = object: ReadWriteProperty<Any, T> {

    private var value:T = value

    init {
        set(value)
    }

    private fun set(value: T) {
        val mesh = this@property

        if(value != null) {
            val converted = value.converter()
            if (converted != null) {
                this.value = value
                mesh[name] = converted
                return
            }
        }

        mesh.remove(name)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        set(value)
    }

    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        return value
    }

}