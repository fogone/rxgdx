package ru.nobirds.rx.gl.buffer.framebuffer

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.gl.bind
import ru.nobirds.rx.gl.utils.Gl

class PackedRenderBuffer(val width: Int, val height: Int) : FrameBufferAttachment {

    private val GL_DEPTH24_STENCIL8_OES = 0x88F0

    override val gl = createRenderBuffer()

    init {
        checkSupport()
        bind {
            Gl.glRenderbufferStorage(GL20.GL_RENDERBUFFER, GL_DEPTH24_STENCIL8_OES, width, height)
        }
    }

    private fun checkSupport() {
        val supported = Gdx.graphics.supportsExtension("GL_OES_packed_depth_stencil") ||
                Gdx.graphics.supportsExtension("GL_EXT_packed_depth_stencil")

        check(supported) { "Packed render buffer not supported" }
    }

    private fun createRenderBuffer(): Int {
        return Gl.glGenRenderbuffer()
    }

    override fun bind() {
        Gl.glBindRenderbuffer(GL20.GL_RENDERBUFFER, gl)
    }

    override fun unbind() {
        Gl.glBindRenderbuffer(GL20.GL_RENDERBUFFER, 0)
    }

    override fun attach() {
        Gl.glFramebufferRenderbuffer(GL20.GL_FRAMEBUFFER, GL20.GL_DEPTH_ATTACHMENT, GL20.GL_RENDERBUFFER, gl)
        Gl.glFramebufferRenderbuffer(GL20.GL_FRAMEBUFFER, GL20.GL_STENCIL_ATTACHMENT, GL20.GL_RENDERBUFFER, gl)
    }

    override fun dispose() {
        Gl.glDeleteRenderbuffer(gl)
    }
}