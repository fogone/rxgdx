package ru.nobirds.rx.gl

import ru.nobirds.rx.gl.utils.Gl

interface Shader : Disposable, Bindable {

    val vertexSize: Int

    fun <T:Any> set(name: String, value: T)

    fun set(values:Map<String, Any>) {
        for ((key, value) in values) {
            set(key, value)
        }
    }

    fun uniform(name: String):Uniform

    fun attribute(name: String):Attribute

    fun bindAttributes()

}

class ShaderImpl(val handle: Int, uniforms:List<Uniform>, attributes:List<Attribute>,
                 val ignoreUnknownUniforms:Boolean = false) : Shader {

    private val uniformsMap = uniforms.associateBy { it.name }
    private val attributesMap = attributes.associateBy { it.name }

    override val vertexSize: Int = attributes.sumBy { it.type.bytesSize }

    override fun <T:Any> set(name: String, value: T) {
        if (ignoreUnknownUniforms) uniformsMap[name]?.set(value)
        else uniform(name).set(value)
    }

    override fun uniform(name: String):Uniform {
        return uniformsMap[name] ?: throw IllegalArgumentException("Uniform with name $name not found, " +
                "available uniforms: ${uniformsMap.keys.joinToString()}")
    }

    override fun attribute(name: String):Attribute {
        return attributesMap[name] ?: throw IllegalArgumentException("Attribute with name ${name} not found, " +
                "available attributes: ${attributesMap.keys.joinToString()}")
    }

    override fun bind() {
        Gl.glUseProgram(handle)
    }

    override fun unbind() {
        disableAttributes()
        Gl.glUseProgram(0)
    }

    override fun bindAttributes() {
        var offset = 0

        for (attribute in attributesMap.values) {
            attribute.enable()
            attribute.structure(vertexSize, offset)
            offset += attribute.type.bytesSize
        }
    }

    private fun disableAttributes() {
        attributesMap.values.forEach(Attribute::disable)
    }

    override fun dispose() {
        Gl.glDeleteProgram(handle)
    }

}

