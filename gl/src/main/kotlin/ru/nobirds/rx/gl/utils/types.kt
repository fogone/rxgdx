package ru.nobirds.rx.gl.utils

import com.badlogic.gdx.graphics.GL20

data class GlType(val glHandle:Int, val size:Int)

object GlTypes {

    val FLOAT:GlType = GlType(GL20.GL_FLOAT, 4)
    val INT:GlType = GlType(GL20.GL_INT, 4)
    val SHORT:GlType = GlType(GL20.GL_SHORT, 2)
    val UNSIGNED_BYTE:GlType = GlType(GL20.GL_UNSIGNED_BYTE, 1)
    val BYTE:GlType = GlType(GL20.GL_BYTE, 1)

}