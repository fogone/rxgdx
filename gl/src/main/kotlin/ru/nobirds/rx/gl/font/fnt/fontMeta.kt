package ru.nobirds.rx.gl.font.fnt

import com.badlogic.gdx.graphics.Pixmap
import ru.nobirds.rx.gl.font.meta.FontMetaPadding
import ru.nobirds.rx.gl.font.meta.FontMetaSpacing

data class FontInfo(
        val face: String, val size: Int,
        val bold: Boolean, val italic: Boolean, val outline: Int,
        val charset: String, val unicode: Boolean,
        val disableSuperSampling: Boolean,
        val stretchHeight: Int, val smooth: Boolean,
        val padding: FontMetaPadding, val spacing: FontMetaSpacing
)

enum class FontChannelType(val value: Int) {

    Glyph(0), Outline(1), GlyphAndOutline(2), Zero(3), One(4);

}

data class FontCommons(
        val lineHeight: Int, val base: Int,
        val scaleWidth: Int, val scaleHeight: Int,
        val packed: Boolean,
        val alphaChannel: FontChannelType, val redChannel: FontChannelType,
        val greenChannel: FontChannelType, val blueChannel: FontChannelType)

data class FontPage(val id: Int, val page: () -> Pixmap)

data class FontChar(
        val id: Int,
        val x: Int, val y: Int,
        val width: Int, val height: Int,
        val xOffset: Int, val yOffset: Int,
        val xAdvance: Int,
        val page: Int, val channel: Int)

data class FontKerning(val first: Int, val second: Int, val amount: Int)