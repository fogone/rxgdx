package ru.nobirds.rx.gl

import ru.nobirds.rx.gl.utils.Gl

interface ShaderVariableType {

    val type:Int
    val size:Int

}

interface ShaderVariable<T:ShaderVariableType> {

    val name:String

    val type: T

    val location:Int

}

interface Uniform : ShaderVariable<UniformType> {

    fun set(value: Any)

}

interface UniformType : ShaderVariableType {

    fun set(location: Int, value: Any)

}

class UniformImpl(override val name:String, override val type: UniformType, override val location:Int) : Uniform {
    override fun set(value: Any) {
        type.set(location, value)
    }
}

interface AttributeType : ShaderVariableType {

    val bytesSize:Int

    fun structure(location: Int, stride:Int, offset: Int)

}

interface Attribute : ShaderVariable<AttributeType> {

    fun structure(stride:Int, offset: Int)

    fun enable()

    fun disable()

}

class AttributeImpl(override val name:String, override val type: AttributeType, override val location:Int) : Attribute {

    override fun structure(stride:Int, offset: Int) {
        type.structure(location, stride, offset)
    }

    override fun enable() {
        Gl.glEnableVertexAttribArray(location)
    }

    override fun disable() {
        Gl.glDisableVertexAttribArray(location)
    }
}


