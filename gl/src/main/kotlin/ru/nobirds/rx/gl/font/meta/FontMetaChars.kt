package ru.nobirds.rx.gl.font.meta

data class FontMetaChar(
        val char: Char,
        val x: Int, val y: Int,
        val width: Int, val height: Int,
        val xOffset: Int, val yOffset: Int,
        val xAdvance: Int,
        val pageId: Int
)

interface FontMetaChars {

    fun get(char: Char): FontMetaChar

    fun available(char: Char): Boolean

    val availableNow: Iterable<FontMetaChar>

}

class PredefinedFontMetaChars(chars: List<FontMetaChar>) : FontMetaChars {

    private val hash = chars.associateBy { it.char }

    override fun get(char: Char): FontMetaChar = hash[char] ?:
            throw IllegalArgumentException("There is no char $char")

    override fun available(char: Char): Boolean = char in hash

    override val availableNow: Iterable<FontMetaChar>
        get() = hash.values

}