package ru.nobirds.rx.gl

interface Bindable {

    fun bind()

    fun unbind()

}

inline fun <B: Bindable, R> B.bind(block:B.()->R):R {
    try {
        bind()
        return block()
    } finally {
        unbind()
    }
}

infix fun Bindable.then(other:Bindable):Bindable = object: Bindable {
    override fun bind() {
        this@then.bind()
        other.bind()
    }

    override fun unbind() {
        other.unbind()
        this@then.unbind()
    }
}
