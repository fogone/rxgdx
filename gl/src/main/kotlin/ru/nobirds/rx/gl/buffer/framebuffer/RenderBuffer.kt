package ru.nobirds.rx.gl.buffer.framebuffer

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.gl.Glable
import ru.nobirds.rx.gl.bind
import ru.nobirds.rx.gl.utils.Gl

enum class AttachmentType(override val gl:Int) : Glable {

    depth(GL20.GL_DEPTH_ATTACHMENT),
    stencil(GL20.GL_STENCIL_ATTACHMENT)

}

class RenderBuffer(val width: Int, val height: Int,
                   val attachmentType: AttachmentType,
                   val internalFormat:Int) : FrameBufferAttachment {

    override val gl = createRenderBuffer()

    init {
        bind {
            Gl.glRenderbufferStorage(GL20.GL_RENDERBUFFER, internalFormat, width, height)
        }
    }

    private fun createRenderBuffer(): Int {
        return Gl.glGenRenderbuffer()
    }

    override fun attach() {
        Gl.glFramebufferRenderbuffer(GL20.GL_FRAMEBUFFER, attachmentType.gl, GL20.GL_RENDERBUFFER, gl)
    }

    override fun bind() {
        Gl.glBindRenderbuffer(GL20.GL_RENDERBUFFER, gl)
    }

    override fun unbind() {
        Gl.glBindRenderbuffer(GL20.GL_RENDERBUFFER, 0)
    }

    override fun dispose() {
        Gl.glDeleteRenderbuffer(gl)
    }
}