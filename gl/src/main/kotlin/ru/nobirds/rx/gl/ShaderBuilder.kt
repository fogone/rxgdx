package ru.nobirds.rx.gl

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.gl.types.AttributeTypeOptions
import ru.nobirds.rx.gl.types.SimpleAttributeTypeRegistry
import ru.nobirds.rx.gl.types.SimpleUniformTypeRegistry
import ru.nobirds.rx.gl.utils.Gl
import ru.nobirds.rx.gl.utils.GlType
import ru.nobirds.rx.gl.utils.IntRef
import ru.nobirds.rx.gl.utils.TextureSlotsManager
import ru.nobirds.rx.gl.utils.glGetProgramiv
import ru.nobirds.rx.utils.files.ImmutableResource
import ru.nobirds.rx.utils.files.readText
import ru.nobirds.rx.utils.poolable

class ShaderModuleBuilder(val type:ShaderType) {

    private var text:String? = null

    fun text(text: String):ShaderModuleBuilder = apply {
        this.text = text
    }

    fun text(resource: ImmutableResource):ShaderModuleBuilder = apply {
        text(resource.readText())
    }

    fun build():ShaderModule {
        val shaderText = text ?: throw IllegalStateException("Shader text must be defined")
        return ShaderModule(type, shaderText)
    }

}

interface VertexStructureBuilder {

    fun attribute(name: String, normalized:Boolean = false, glType: GlType? = null)

}

class ShaderBuilder(textureSlotsManager: TextureSlotsManager) : VertexStructureBuilder {

    private val uniformTypeRegistry = SimpleUniformTypeRegistry(textureSlotsManager) // todo: make it setupable
    private val attributeTypeRegistry = SimpleAttributeTypeRegistry() // todo: make it setupable

    private val conveyor = mutableMapOf<ShaderType, ShaderModule>()

    private var ignoreUnknownUniforms = false

    private val attributes = linkedMapOf<String, AttributeTypeOptions>()

    fun ignoreUnknownUniforms(value:Boolean = true):ShaderBuilder = apply {
        this.ignoreUnknownUniforms = value
    }

    fun vertex(text:String): ShaderBuilder = vertex { text(text) }
    fun fragment(text:String): ShaderBuilder = fragment { text(text) }
    
    fun module(module: ShaderModule): ShaderBuilder = apply {
        conveyor.put(module.type, module)
    }
    
    inline fun vertex(builder:ShaderModuleBuilder.()->Unit): ShaderBuilder = module(ShaderType.vertex, builder)
    inline fun fragment(builder:ShaderModuleBuilder.()->Unit): ShaderBuilder = module(ShaderType.fragment, builder)

    inline fun module(type: ShaderType, builder:ShaderModuleBuilder.()->Unit): ShaderBuilder = module(
            ShaderModuleBuilder(type).apply(builder).build()
    )

    override fun attribute(name: String, normalized:Boolean, glType: GlType?) {
        attributes.put(name, AttributeTypeOptions(normalized, glType))
    }

    fun attributes(builder:VertexStructureBuilder.()->Unit) {
        this.builder()
    }

    fun build(): Shader {
        val shaders = conveyor.values
                .sortedBy { it.type.ordinal }
                .map { it.compile() }

        val program = createProgram()

        attachShaders(program, shaders)

        linkProgram(program)

        detachAndDeleteShaders(program, shaders)

        val uniforms = findUniforms(program)
        val attributes = findAttributes(program).associateBy { it.name }

        val sortedAttributes = sortAttributes(attributes)

        return ShaderImpl(program, uniforms, sortedAttributes, ignoreUnknownUniforms)
    }

    private fun sortAttributes(attributes: Map<String, Attribute>): List<Attribute> {
        return if (this.attributes.isNotEmpty()) this.attributes.keys.map {
            attributes[it] ?: throw IllegalStateException("Attribute with name $it not found in shader")
        } else attributes.values.sortedBy { it.location }
    }

    private fun detachAndDeleteShaders(program: Int, shaders: List<Int>) {
        for (shader in shaders) {
            Gl.glDetachShader(program, shader)
            Gl.glDeleteShader(shader)
        }
    }

    private fun attachShaders(program: Int, shaders: List<Int>) {
        for (shader in shaders) {
            Gl.glAttachShader(program, shader)
        }
    }

    private fun findUniforms(program: Int): List<Uniform> {
        val count = getProgramAttribute(program, GL20.GL_ACTIVE_UNIFORMS)
        return (0..count-1).map { findUniform(program, it) }
    }

    private fun findUniform(program: Int, index: Int): Uniform = poolable {
        val type: IntRef = obtain()
        val size: IntRef = obtain()

        val name = Gl.glGetActiveUniform(program, index, size.forWrite(), type.forWrite())

        val location = Gl.glGetUniformLocation(program, name)

        val realName = name.substringBefore("[")

        UniformImpl(realName, uniformTypeRegistry.get(type.value, size.value), location)
    }

    private fun findAttributes(program: Int): List<Attribute> {
        val count = getProgramAttribute(program, GL20.GL_ACTIVE_ATTRIBUTES)
        return (0..count-1).map { findAttribute(program, it) }
    }

    private fun findAttribute(program: Int, index: Int): Attribute = poolable {
        val type: IntRef = obtain()
        val size: IntRef = obtain()

        val name = Gl.glGetActiveAttrib(program, index, size.forWrite(), type.forWrite())

        val location = Gl.glGetAttribLocation(program, name)

        val options = attributes[name] ?:
                (if(attributes.isEmpty()) AttributeTypeOptions() else
                    throw IllegalStateException("Options for attribute $name not defined"))

        AttributeImpl(name, attributeTypeRegistry.get(type.value, size.value, options), location)
    }

    private fun linkProgram(program: Int) {
        Gl.glLinkProgram(program)

        val linked = getProgramAttribute(program, GL20.GL_LINK_STATUS)

        if (linked == GL20.GL_FALSE) {
            val log = Gl.glGetProgramInfoLog(program)
            throw ShaderException("Can't link shader program: $log")
        }
    }

    private fun createProgram(): Int {
        val program = Gl.glCreateProgram()
        if (program == GL20.GL_FALSE)
            throw ShaderException("Can't create shader program")
        return program
    }

    private fun getProgramAttribute(program: Int, attribute:Int): Int {
        return Gl.glGetProgramiv(program, attribute)
    }

}

fun shader(textureSlotsManager: TextureSlotsManager, builder: ShaderBuilder.()->Unit): Shader {
    val shaderBuilder = ShaderBuilder(textureSlotsManager)
    shaderBuilder.builder()
    return shaderBuilder.build()
}
