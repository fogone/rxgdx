package ru.nobirds.rx.gl

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.gl.utils.Gl
import ru.nobirds.rx.gl.utils.glGetShaderiv

class ShaderModule(val type: ShaderType, val source:String) : Disposable {

    private var handle:Int? = null

    fun compile(): Int {
        if (isValidShader(handle)) {
            return handle!!
        }

        this.handle = createShader(source)

        return compileShader(this.handle!!, source)
    }

    private fun isValidShader(handle: Int?): Boolean {
        if(handle == null)
            return false

        return Gl.glIsShader(handle)
    }

    private fun compileShader(shader: Int, source: String): Int {
        Gl.glShaderSource(shader, source)
        Gl.glCompileShader(shader)

        val compiled = Gl.glGetShaderiv(shader, GL20.GL_COMPILE_STATUS)

        if (compiled == GL20.GL_FALSE) {
            val errorMessage = Gl.glGetShaderInfoLog(shader)

            throw ShaderCompileException(source, errorMessage)
        }

        return shader
    }

    private fun createShader(source: String): Int {
        val shader = Gl.glCreateShader(type.glType)

        if (shader == GL20.GL_FALSE)
            throw ShaderCompileException(source, "Can't create shader of type $type")

        return shader
    }

    override fun dispose() {
        if (isValidShader(this.handle)) {
            Gl.glDeleteShader(this.handle!!)
            this.handle = null
        }
    }
}