package ru.nobirds.rx.gl.buffer

import ru.nobirds.rx.gl.Bindable
import ru.nobirds.rx.gl.Disposable
import ru.nobirds.rx.gl.NeedsGl3Support
import ru.nobirds.rx.gl.Shader
import ru.nobirds.rx.gl.bind
import ru.nobirds.rx.gl.then
import ru.nobirds.rx.gl.utils.Gl3
import ru.nobirds.rx.gl.utils.Gls
import ru.nobirds.rx.gl.utils.glDeleteVertexArray
import ru.nobirds.rx.gl.utils.glGenVertexArray

fun structuredArrayBuffer(shader: Shader, buffer:ArrayBuffer):StructuredArrayBuffer =
        if(Gls.gl3Supported)
            VertexArray(shader, buffer)
        else
            StructuredVertexBuffer(shader, buffer)

interface StructuredArrayBuffer : Disposable, Bindable {

    val vertexSize:Int

    val count:Int

    val buffer:ArrayBuffer

}

abstract class AbstractStructuredArrayBuffer(shader: Shader, override val buffer:ArrayBuffer) : StructuredArrayBuffer {

    override val vertexSize:Int = shader.vertexSize

    override val count: Int
        get() = buffer.sizeInBytes/ vertexSize

}

class StructuredVertexBuffer(val shader: Shader, buffer:ArrayBuffer) : AbstractStructuredArrayBuffer(shader, buffer) {

    override fun bind() {
        buffer.bind()
        shader.bindAttributes()
    }

    override fun unbind() {
        buffer.unbind()
    }

    override fun dispose() {
        buffer.dispose()
    }

}

@NeedsGl3Support
class VertexArray(shader: Shader, buffer:ArrayBuffer) : AbstractStructuredArrayBuffer(shader, buffer) {

    private val handle = Gl3.glGenVertexArray()

    init {
        (buffer then this).bind {
            shader.bindAttributes()
        }
    }

    override fun bind() {
        Gl3.glBindVertexArray(handle)
    }

    override fun unbind() {
        Gl3.glBindVertexArray(0)
    }

    override fun dispose() {
        Gl3.glDeleteVertexArray(handle)
        buffer.dispose()
    }

}

