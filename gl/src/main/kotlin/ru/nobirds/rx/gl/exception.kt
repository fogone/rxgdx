package ru.nobirds.rx.gl

open class ShaderException(message:String) : RuntimeException(message)
class ShaderCreationException(val type: ShaderType, val source: String) : ShaderException("Can't create shader $type: \n$source")
class ShaderCompileException(val source: String, val compilerMessage:String) : ShaderException("$compilerMessage: \n$source")