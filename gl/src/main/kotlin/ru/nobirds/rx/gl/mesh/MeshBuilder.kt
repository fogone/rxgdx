package ru.nobirds.rx.gl.mesh

import ru.nobirds.rx.gl.ArrayBufferDrawer
import ru.nobirds.rx.gl.DrawMode
import ru.nobirds.rx.gl.DrawPrimitive
import ru.nobirds.rx.gl.ElementsArrayBufferDrawer
import ru.nobirds.rx.gl.Shader
import ru.nobirds.rx.gl.buffer.ArrayBuffer
import ru.nobirds.rx.gl.buffer.ElementsArrayBuffer
import ru.nobirds.rx.gl.buffer.IntElementsArrayBuffer
import ru.nobirds.rx.gl.buffer.ShortElementsArrayBuffer
import ru.nobirds.rx.gl.buffer.StructuredArrayBuffer
import ru.nobirds.rx.gl.buffer.structuredArrayBuffer
import java.nio.ByteBuffer
import java.nio.IntBuffer
import java.nio.ShortBuffer

class MeshBuilder(val shader: Shader, val drawPrimitive: DrawPrimitive, val mode: DrawMode = DrawMode.static) {

    private var data: StructuredArrayBuffer? = null

    private var index: ElementsArrayBuffer<*>? = null

    private val values = hashMapOf<String, Any>()

    fun withBuffer(buffer: ByteBuffer): MeshBuilder =
            withBuffer(ArrayBuffer(buffer, mode))

    fun withBuffer(buffer: ArrayBuffer): MeshBuilder = apply {
        this.data = structuredArrayBuffer(shader, buffer)
    }

    fun withIndex(index: ShortBuffer): MeshBuilder = apply {
        this.index = ShortElementsArrayBuffer(index, mode)
    }

    fun withIndex(index: IntBuffer): MeshBuilder = apply {
        this.index = IntElementsArrayBuffer(index, mode)
    }

    fun withIndex(index: ElementsArrayBuffer<*>): MeshBuilder = apply {
        this.index = index
    }

    fun withValue(name: String, value:Any): MeshBuilder = apply {
        values.put(name, value)
    }

    fun build(): Mesh {
        val data = data ?: throw IllegalArgumentException("Buffer must be defined")
        val index = index

        val drawer = if (index != null)
            ElementsArrayBufferDrawer(data, index, drawPrimitive)
        else
            ArrayBufferDrawer(data, drawPrimitive)

        return Mesh(shader, drawer, values)
    }
}