package ru.nobirds.rx.gl.types

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.gl.UniformType
import ru.nobirds.rx.gl.utils.Gl
import ru.nobirds.rx.gl.utils.throwOnError
import java.nio.FloatBuffer

fun floatMatrixUniform(type: Int, size: Int, count:Int,
                       setArray: GL20.(Int, Int, Boolean, FloatArray, Int)->Unit,
                       setBuffer: GL20.(Int, Int, Boolean, FloatBuffer)->Unit): UniformType =
        FloatMatrixUniformType(type, size, count, setArray, setBuffer)

class FloatMatrixUniformType(
        override val type: Int,
        override val size: Int,
        val count:Int,
        val setArray: GL20.(Int, Int, Boolean, FloatArray, Int)->Unit,
        val setBuffer: GL20.(Int, Int, Boolean, FloatBuffer)->Unit) : UniformType {

    override fun set(location: Int, value: Any) {
        when (value) {
            is FloatArray -> Gl.throwOnError { setArray(location, size, false, validate(value, value.size), 0) }
            is FloatBuffer -> Gl.throwOnError { setBuffer(location, size, false, validate(value, value.limit())) }
            else -> throw IllegalArgumentException(
                    "Unsupported type of parameter: required float4 compatible, but passed ${value.javaClass}")
        }
    }

    private fun <T> validate(value: T, size: Int): T {
        if(size != count*count)
            throw IllegalArgumentException("Required size ${count*count}, but passed $size")

        return value
    }

}