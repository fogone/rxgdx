package ru.nobirds.rx.gl.font.fnt

import com.badlogic.gdx.graphics.Pixmap
import ru.nobirds.rx.gl.font.meta.FontMetaChar
import ru.nobirds.rx.gl.font.meta.FontMetaInfo
import ru.nobirds.rx.gl.font.meta.FontMetaMetrics
import ru.nobirds.rx.gl.font.meta.FontMetaPadding
import ru.nobirds.rx.gl.font.meta.FontMetaPages
import ru.nobirds.rx.gl.font.meta.FontMetaSpacing
import ru.nobirds.rx.gl.font.meta.PredefinedFontMetaChars
import ru.nobirds.rx.gl.font.meta.PredefinedFontMetaKerning
import ru.nobirds.rx.utils.files.ImmutableResource
import ru.nobirds.rx.utils.files.fileName
import ru.nobirds.rx.utils.files.toReader
import java.io.Reader

private class FontMetaPagesImpl(pages: List<FontPage>) : FontMetaPages {

    private val hash = pages.associateBy { it.id }

    override val ids: Iterable<Int> = pages.map { it.id }

    override fun get(id: Int): Pixmap = hash[id]?.page?.invoke() ?:
            throw IllegalArgumentException("There is no page with id $id")

}

object FontMetaParser {

    class Meta(val header: String, val values: Map<String, String>) {

        operator fun get(name: String): String? = values[name]

        fun ints(name: String): List<Int> = get(name)?.let {
            it.split(",").map { it.trim().toInt() }
        } ?: throw IllegalArgumentException("There is not $name value")

        fun str(name: String): String = get(name)?.let {
            require(it.startsWith("\"") && it.endsWith("\"")) { "Parameter $name not a string a has value $it" }
            it.substring(1..it.length - 2)
        } ?: throw IllegalArgumentException("There is not $name value")

        fun boolean(name: String): Boolean = get(name)?.let { it == "1" } ?:
                throw IllegalArgumentException("There is not $name value")

        fun integer(name: String): Int = get(name)?.let(String::toInt) ?:
                throw IllegalArgumentException("There is not $name value")

    }

    fun parse(resource: ImmutableResource, loader: (String) -> ByteArray): FontMetaInfo {
        return parse(resource.toReader(), resource.fileName, loader)
    }

    fun parse(reader: Reader, name: String, loader: (String) -> ByteArray): FontMetaInfo {
        val buffered = reader.buffered()

        val items = generateSequence { buffered.readLine() }
                .map { parseLine(it) }
                .groupBy { it.header }

        val info = info(items["info"]?.first() ?: throw IllegalArgumentException("No info"))

        val commons = commons(items["common"]?.first() ?: throw IllegalArgumentException("No common"))

        val pages = items["page"]?.map { page(it, loader) } ?: throw IllegalArgumentException("No info")

        val chars = items["char"]?.map { char(it) } ?: throw IllegalArgumentException("No chars")

        val kernings = items["kerning"]?.map { kerning(it) } ?: throw IllegalArgumentException("No kernings")

        val capitalLetterHeight = chars.fold(0) { height, char -> Math.max(char.height, height) } -
                (info.padding.bottom + info.padding.bottom)

        val descent = chars.fold(0) { descent, char -> Math.min(commons.base + char.yOffset, descent) } +
                info.padding.bottom

        val ascent = commons.base - capitalLetterHeight

        return FontMetaInfo(
                name,
                FontMetaMetrics(
                        info.face, info.size, info.bold,
                        info.italic, info.outline,
                        info.stretchHeight,
                        commons.lineHeight, commons.base,
                        commons.scaleWidth, commons.scaleHeight,
                        ascent, descent,
                        capitalLetterHeight,
                        info.padding, info.spacing
                ),
                FontMetaPagesImpl(pages),
                PredefinedFontMetaChars(chars.map {
                    FontMetaChar(
                            it.id.toChar(), it.x, it.y,
                            it.width, it.height, it.xOffset,
                            it.yOffset, it.xAdvance, it.page
                    )
                }),
                PredefinedFontMetaKerning(
                        kernings.groupBy { it.first.toChar() }
                                .mapValues { it.value.associateBy({ it.second.toChar() }, { it.amount }) }
                )
        )
    }

    private fun parseLine(text: String): Meta {
        require(text.isNotEmpty())

        val header = text.substringBefore(' ')

        val parts = text
                .substringAfter("$header ")
                .split("\\s".toRegex())
                .map { it.substringBefore('=') to it.substringAfter('=') }
                .toMap()

        return Meta(header, parts)
    }

    private fun info(info: Meta): FontInfo = FontInfo(
            info.str("face"),
            info.integer("size"),
            info.boolean("bold"),
            info.boolean("italic"),
            info.integer("outline"),
            info.str("charset"),
            info.boolean("unicode"),
            info.boolean("aa"),
            info.integer("stretchH"),
            info.boolean("smooth"),
            padding(info.ints("padding")),
            spacing(info.ints("spacing"))
    )

    private fun channelType(value: Int): FontChannelType =
            FontChannelType.values().first { it.value == value }

    private fun commons(common: Meta): FontCommons = FontCommons(
            common.integer("lineHeight"),
            common.integer("base"),
            common.integer("scaleW"),
            common.integer("scaleH"),
            common.boolean("packed"),
            channelType(common.integer("alphaChnl")),
            channelType(common.integer("redChnl")),
            channelType(common.integer("greenChnl")),
            channelType(common.integer("blueChnl"))
    )

    private fun page(page: Meta, loader: (String) -> ByteArray): FontPage = FontPage(page.integer("id"))
    { loader(page.str("file")).let { Pixmap(it, 0, it.size) } }

    private fun char(char: Meta): FontChar = FontChar(
            char.integer("id"),
            char.integer("x"),
            char.integer("y"),
            char.integer("width"),
            char.integer("height"),
            char.integer("xoffset"),
            char.integer("yoffset"),
            char.integer("xadvance"),
            char.integer("page"),
            char.integer("chnl")
    )

    private fun padding(padding: List<Int>): FontMetaPadding {
        require(padding.size == 4)

        val top = padding[0]
        val left = padding[1]
        val bottom = padding[2]
        val right = padding[3]

        return FontMetaPadding(top, left, bottom, right)
    }

    private fun spacing(spacing: List<Int>): FontMetaSpacing {
        require(spacing.size == 2)

        val horizontal = spacing[0]
        val vertical = spacing[1]

        return FontMetaSpacing(horizontal, vertical)
    }

    private fun kerning(kerning: Meta): FontKerning = FontKerning(
            kerning.integer("first"),
            kerning.integer("second"),
            kerning.integer("amount")
    )

}