package ru.nobirds.rx.gl.font.meta

import com.badlogic.gdx.graphics.Pixmap
import java.util.ArrayList

interface FontMetaPacker {

    fun pack(meta: FontMetaInfo): FontMetaInfo

}

private class SingleFontMetaPages(val id: Int, val page: Pixmap) : FontMetaPages {
    override val ids: Iterable<Int> = listOf(id)

    override fun get(id: Int): Pixmap {
        if(id == this.id) return page

        throw IllegalArgumentException("This is only one page with id ${this.id}")
    }
}

interface FontMetaPackLayout {

    val xOffset: Int

    val yOffset: Int

    fun update(width: Int, height: Int)

}

class HorizontalFontMetaPackLayout : FontMetaPackLayout {

    override var xOffset: Int = 0

    override val yOffset: Int = 0

    override fun update(width: Int, height: Int) {
        xOffset += width
    }

}

class SingleFontMetaPacker(val layoutFactory: () -> FontMetaPackLayout) : FontMetaPacker {

    private val defaultPageId = 1

    override fun pack(meta: FontMetaInfo): FontMetaInfo {
        val pages = meta.pages.all().toList()

        val chars = meta.chars.availableNow.groupBy { it.pageId }

        val result = Pixmap(0, 0, Pixmap.Format.RGBA8888)

        val resultChars = arrayListOf<FontMetaChar>()

        val layout = layoutFactory()

        pages.forEach { (id, pixmap) ->
            val pageChars = chars[id]?.map { it.copy(
                        x = it.x + layout.xOffset,
                        y = it.y + layout.yOffset,
                        pageId = defaultPageId
                )
            } ?: emptyList<FontMetaChar>()

            if (pageChars.isNotEmpty()) {
                result.drawPixmap(pixmap, layout.xOffset, layout.yOffset)
                resultChars.addAll(pageChars)
                layout.update(pixmap.width, pixmap.height)
            }
        }

        return meta.copy(
                pages = SingleFontMetaPages(defaultPageId, result),
                chars = PredefinedFontMetaChars(resultChars)
        )
    }

}