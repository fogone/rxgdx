package ru.nobirds.rx.test

import org.junit.Assert
import org.junit.Test
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.ReflectionComponentFactory
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.di.annotations.Configuration
import ru.nobirds.rx.di.annotations.annotationContext
import ru.nobirds.rx.event.CustomEventBinder
import ru.nobirds.rx.event.annotation.AnnotationEventBinder
import ru.nobirds.rx.event.annotation.EventAnnotationProcessor
import ru.nobirds.rx.module.AbstractModule
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.module.ReflectionModuleFactory
import ru.nobirds.rx.module.SetupEvent
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.module.moduleDefinition

class TestComponent(parent: Module) : AbstractComponent(parent) {

    var settedUp = false

    @OnSetup
    fun testSetup() {
        settedUp = true
    }

    fun blabla():String = "test string"

}

@DependentComponents(TestComponent3::class, TestComponent4::class)
class TestComponent2(parent: Module) : AbstractComponent(parent)

class TestComponent3(parent: Module) : AbstractComponent(parent)

@DependentComponents(TestComponent5::class)
class TestComponent4(parent: Module) : AbstractComponent(parent)

@DependentComponents(TestComponent6::class)
class TestComponent5(parent: Module) : AbstractComponent(parent) {

    val testComponent6:TestComponent6 by dependency()

}

class TestComponent6(parent: Module) : AbstractComponent(parent)

@DependentComponents(TestComponent::class, TestComponent2::class, TestComponent4::class)
class TestModule(id:String) : AbstractModule(id) {

    private val test:TestComponent by dependency()

    var settedUp = false

    @OnSetup
    fun testSetup() {
        settedUp = true
    }

    fun foo():String = test.blabla()

}

@Configuration
class EmptyConfiguration

class ComponentsTest {

    val context = annotationContext(EmptyConfiguration::class)
    val componentFactory = ReflectionComponentFactory(context)
    val moduleFactory = ReflectionModuleFactory(context, componentFactory)

    @Test
    fun findEventAnnotationsTest() {
        val bindings = EventAnnotationProcessor().process(TestModule::class)

        Assert.assertEquals(bindings.size, 1)
    }

    @Test
    fun moduleDefinitionCreationTest() {
        val moduleDefinition = moduleDefinition<TestModule>("test") {
            withEvent<SetupEvent> {
                // do nothing
            }
        }

        val binder = moduleDefinition.eventBinder as CustomEventBinder

        Assert.assertEquals(binder.bindings.size, 1)
        Assert.assertEquals((binder.parent as AnnotationEventBinder).bindings.size, 1)

        val testComponent = moduleDefinition.components.filter { it.type == TestComponent::class }.firstOrNull()

        Assert.assertNotNull(testComponent)

        val eventBinder = testComponent!!.eventBinder as CustomEventBinder

        Assert.assertEquals(eventBinder.bindings.size, 0)
        Assert.assertEquals((eventBinder.parent as AnnotationEventBinder).bindings.size, 1)
    }

    @Test
    fun creationTest() {
        val testModule = moduleFactory.create(moduleDefinition<TestModule>("test", {}))

        val components = testModule.components.toList()

        Assert.assertEquals(6, components.size)
        Assert.assertEquals("test string", testModule.foo())
    }

    @Test
    fun eventsTest() {
        val testModule = moduleFactory.create(moduleDefinition<TestModule>("test", {}))

        Assert.assertTrue(testModule.settedUp)

        val testComponent = testModule.findComponent<TestComponent>()

        Assert.assertTrue(testComponent.settedUp)
    }

}
