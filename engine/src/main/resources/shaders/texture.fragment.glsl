#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

uniform sampler2D u_texture;

varying vec2 v_position;
varying vec4 v_tint_color;

void main()
{
  gl_FragColor = texture2D(u_texture, v_position) * v_tint_color;
}