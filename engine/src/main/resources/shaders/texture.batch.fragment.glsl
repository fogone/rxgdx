#version 150

#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

uniform sampler2D u_textures[10];

flat in int v_texture_number;
in vec2 v_position;
in vec4 v_tint_color;

out vec4 fragmentColor;

void main()
{
  fragmentColor = texture2D(u_textures[v_texture_number], v_position) * v_tint_color;
}