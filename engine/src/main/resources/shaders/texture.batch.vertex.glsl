#version 150

in float a_texture_number; // todo: fix this hack and migrate to int
in vec2 a_position;
in vec2 a_texture_position;
in vec4 a_tint_color;

uniform mat4 u_projection;

flat out int v_texture_number;
out vec2 v_position;
out vec4 v_tint_color;

void main()
{
   v_texture_number = int(a_texture_number);
   v_position = a_texture_position;
   v_tint_color = a_tint_color;

   gl_Position = u_projection * vec4(a_position, 0.0, 1.0);
}
