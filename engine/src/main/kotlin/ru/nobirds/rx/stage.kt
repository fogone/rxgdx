package ru.nobirds.rx

import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.parents
import ru.nobirds.rx.render.RenderConveyor

interface Stage : Module {

    val layers:Sequence<Layer>

    fun load():Boolean

    fun update(delta: Float)

    fun render(delta: Float, renderer: RenderConveyor)

    val loaded:Boolean

}

fun Module.findStage():Stage? = parents().filterIsInstance<Stage>().firstOrNull()
