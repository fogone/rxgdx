package ru.nobirds.rx

import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.thisAndParents
import ru.nobirds.rx.render.RenderConveyor

interface Layer : Module {

    var projection: Projection

    var debug:Boolean

    fun render(delta: Float, renderer: RenderConveyor)

    fun renderDebug(delta: Float, renderer: RenderConveyor)

}

fun Module.findLayer():Layer? = thisAndParents().filterIsInstance<Layer>().firstOrNull()
