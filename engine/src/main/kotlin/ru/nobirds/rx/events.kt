package ru.nobirds.rx

import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.utils.ImmutableSize

open class LifecycleEvent() : AbstractEvent()
object PauseEvent : LifecycleEvent()
object ResumeEvent : LifecycleEvent()

class ResizeEvent(override val width: Float, override val height: Float) : LifecycleEvent(), ImmutableSize

class UpdateEvent(var delta: Float) : AbstractEvent() {

    fun delta(value:Float):UpdateEvent {
        this.delta = value
        return this
    }

}

inline fun Eventable.onUpdate(crossinline onNext:(UpdateEvent)->Unit): Subscription = onEvent(onNext)

class LoadEvent() : AbstractEvent() {

    var complete:Boolean = true
        private set

    fun complete(value:Boolean) {
        complete = complete && value
        if(!complete)
            handled()
    }

    fun renew(): LoadEvent {
        complete = true
        return this
    }

}

@OnEvent(UpdateEvent::class)
annotation class OnUpdate