package ru.nobirds.rx

import com.badlogic.gdx.math.Matrix4
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.reactive.emptyReactiveStream
import ru.nobirds.rx.utils.MutableVec2f

interface Projection : Invalidatable {

    val matrix: Matrix4

    fun project(vector: MutableVec2f): MutableVec2f
    fun unproject(vector: MutableVec2f): MutableVec2f

}

object NoneProjection : Projection {

    override val matrix: Matrix4 = Matrix4().idt()

    override fun project(vector: MutableVec2f): MutableVec2f = vector
    override fun unproject(vector: MutableVec2f): MutableVec2f = vector

    override val invalidated: ReactiveStream<Signal> = emptyReactiveStream()

}