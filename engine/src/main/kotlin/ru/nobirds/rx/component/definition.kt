package ru.nobirds.rx.component

import ru.nobirds.rx.event.CustomEventBinder
import ru.nobirds.rx.event.CustomEventBinding
import ru.nobirds.rx.event.Event
import ru.nobirds.rx.event.EventBinder
import kotlin.reflect.KClass

interface ComponentValues<in C : Component> {

    fun setValues(component: C)

}

class ProgrammaticComponentValues<in C : Component>(val setter: C.() -> Unit) : ComponentValues<C> {
    override fun setValues(component: C) {
        component.setter()
    }
}

class EmptyComponentValues<in C : Component>() : ComponentValues<C> {
    override fun setValues(component: C) {
    }
}

class CompositeComponentValues<in C : Component>(vararg val values: ComponentValues<C>) : ComponentValues<C> {

    override fun setValues(component: C) {
        for (value in values) {
            value.setValues(component)
        }
    }

}

class ComponentDefinition<C : Component>(
        val type: KClass<C>,
        val dependentComponents: Set<KClass<out Component>>,
        val componentValues: ComponentValues<C>,
        defaultEventBinder: EventBinder<C>) {

    private val customBindings = arrayListOf<CustomEventBinding<C, *>>()

    val eventBinder: EventBinder<C> = CustomEventBinder(defaultEventBinder, customBindings)

    operator fun plus(componentDefinition: ComponentDefinition<C>): ComponentDefinition<C> {
        require(type == componentDefinition.type) { "Illegal merge different components" }

        return ComponentDefinition(type,
                dependentComponents + componentDefinition.dependentComponents,
                CompositeComponentValues(componentValues, componentDefinition.componentValues),
                eventBinder)
    }


    inline fun <reified E : Event> withEvent(noinline binding: C.(E) -> Unit) {
        withEvent(E::class, binding)
    }

    fun <E : Event> withEvent(type: KClass<E>, binding: C.(E) -> Unit) {
        customBindings.add(CustomEventBinding(type, binding))
    }

}

