package ru.nobirds.rx.component

import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.di.Context
import ru.nobirds.rx.di.hierarchy
import ru.nobirds.rx.event.annotation.AnnotationEventBinder
import ru.nobirds.rx.event.annotation.EventAnnotationProcessor
import ru.nobirds.rx.module.Module
import kotlin.comparisons.compareBy
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubclassOf

interface ComponentFactory {

    fun create(parent: Module, components: Iterable<ComponentDefinition<out Component>>): List<Component>

}

class ReflectionComponentFactory(val context: Context) : ComponentFactory {

    override fun create(parent: Module, components: Iterable<ComponentDefinition<out Component>>): List<Component> {
        val definitions = findDependent(components)
        return definitions.map { create(parent, it) }
    }

    private fun findDependent(components: Iterable<ComponentDefinition<out Component>>): Iterable<ComponentDefinition<out Component>> {
        val knownComponents = components.map { it.type }.toSet()

        val dependentComponents = components.flatMap { it.dependentComponents }.toSet()

        val newComponentTypes = dependentComponents - knownComponents

        if (newComponentTypes.isNotEmpty()) {
            
            @Suppress("UNCHECKED_CAST")
            val newComponents = newComponentTypes.map { c -> ComponentDefinitionFactory.create(c as KClass<Component>) }

            return findDependent(components + newComponents)
        } else {
            return components
        }
    }

    private fun create(parent: Module, component: ComponentDefinition<out Component>): Component {
        @Suppress("UNCHECKED_CAST")
        val c = component as ComponentDefinition<Component>

        val instance = createWithConstructorInjection(parent, context, c.type)

        c.componentValues.setValues(instance)
        c.eventBinder.bind(instance)

        return instance
    }


    private fun <T:Any> createWithConstructorInjection(parent: Module, context: Context, type: KClass<T>):T {
        val constructor = type.constructors.firstOrNull { it.parameters.all { it.isOptional } }

        if(constructor != null)
            return constructor.call()

        val constructors = type.constructors

        if(constructors.size != 1)
            throw IllegalArgumentException("Component of type ${type.qualifiedName} " +
                    "have ${constructors.size} constructors, but for autowiring needs only one.")

        val constructorForAutowire = constructors.first()

        // todo: by parameter name
        val parameters = constructorForAutowire.parameters.map {
            val classifier = it.type.classifier

            if(classifier is KClass<*>) {
                if(classifier.isSubclassOf(Module::class))
                    parent
                else
                    context.find(classifier)
            } else {
                throw IllegalArgumentException("Unsupported parameter classifier $classifier")
            }
        }.toTypedArray()

        return constructorForAutowire.call(*parameters)
    }

}


object ComponentDefinitionFactory {

    private val eventAnnotationProcessor = EventAnnotationProcessor()

    private fun <C : Component> create(type: KClass<C>, values: ComponentValues<C>): ComponentDefinition<C> {
        val annotationEventBinder = AnnotationEventBinder<C>(eventAnnotationProcessor.process(type))

        val dependent = findDependencies(type)

        return ComponentDefinition(type, dependent, values, annotationEventBinder)
    }

    private fun <C : Component> findDependencies(type: KClass<C>): Set<KClass<out Component>> {
        return type.hierarchy()
                .map { it.findAnnotation<DependentComponents>() }
                .filterNotNull()
                .flatMap {
                    // todo: bug workaround
                    val dependencies: Array<out KClass<out Component>> = it.value
                    dependencies.asSequence()
                }
                .toSortedSet(compareBy { it.simpleName })
    }

    fun <C : Component> create(type: KClass<C>, initializer: C.() -> Unit): ComponentDefinition<C> {
        return create(type, ProgrammaticComponentValues(initializer))
    }

    fun <C : Component> create(type: KClass<C>): ComponentDefinition<C> {
        return create(type, EmptyComponentValues())
    }

}
