package ru.nobirds.rx.component

import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.findById
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.module.findOptionalComponent
import ru.nobirds.rx.property.Property

interface Component : Eventable {

    val parent: Module

}
inline fun <reified C: Component> Component.dependency():Lazy<C> = dependency<C, C> { this }

inline fun <reified C: Component, R> Component.dependency(crossinline accessor:C.()->R):Lazy<R> = lazy {
    parent.findComponent<C>().accessor()
}

inline fun <reified C: Component, R> Component.dependentProperty(crossinline accessor:C.()->Property<R>):Lazy<Property<R>> = dependency(accessor)

inline fun <reified C: Component> Component.dependencyOptional():Lazy<C?> = lazy {
    parent.findOptionalComponent<C>()
}

inline fun <reified M:Module> Component.injectById(id:String):Lazy<M> = lazy {
    parent.findById(id, M::class) ?: throw IllegalArgumentException("Module with id $id and type ${M::class} not found.")
}

inline fun <reified M:Module> Component.injectByIdOptional(id:String):Lazy<M?> = lazy {
    parent.findById(id, M::class)
}

