package ru.nobirds.rx.component

import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.EventableSupport
import ru.nobirds.rx.module.Module

abstract class AbstractComponent(override val parent:Module) : Component, Eventable by EventableSupport() {

    override fun parent(): Eventable = parent

    override fun toString(): String = "$parent -> @${this.javaClass.simpleName}"

}
