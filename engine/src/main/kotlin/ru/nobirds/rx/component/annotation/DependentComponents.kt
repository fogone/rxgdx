package ru.nobirds.rx.component.annotation

import ru.nobirds.rx.component.Component
import kotlin.reflect.KClass

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
annotation class DependentComponents(vararg val value:KClass<out Component>)

