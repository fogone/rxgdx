package ru.nobirds.rx.director

import com.badlogic.gdx.ApplicationListener
import ru.nobirds.rx.di.annotations.annotationContext
import ru.nobirds.rx.di.find

class ConfigurationBasedApplicationListener(val configuration: Any) : ApplicationListener {

    private lateinit var applicationListener: ApplicationListener

    override fun create() {
        this.applicationListener = annotationContext(configuration).find<ApplicationListener>()
        this.applicationListener.create()
    }

    override fun render() {
        applicationListener.render()
    }

    override fun pause() {
        applicationListener.pause()
    }

    override fun resume() {
        applicationListener.resume()
    }

    override fun resize(width: Int, height: Int) {
        applicationListener.resize(width, height)
    }

    override fun dispose() {
        applicationListener.dispose()
    }
}