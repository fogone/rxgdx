package ru.nobirds.rx.director

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.InputProcessor
import ru.nobirds.rx.PauseEvent
import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.ResumeEvent
import ru.nobirds.rx.SceneDirector
import ru.nobirds.rx.SceneDirectorInitializer
import ru.nobirds.rx.Stage
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.gl.utils.Gls
import ru.nobirds.rx.gl.utils.TextureSlotsManager
import ru.nobirds.rx.module.DisposeEvent
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.ModuleFactory
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.getValue
import ru.nobirds.rx.property.map
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.ReactiveStreamProducer
import ru.nobirds.rx.reactive.reactiveStreamProducer
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.sceneDirectorInitializer
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.MutableSize

fun director(initializer: SceneDirector.() -> Unit): ApplicationListener {
    return director(GenericSceneDirectorConfiguration(sceneDirectorInitializer(initializer)))
}

fun director(configuration: Any): ApplicationListener {
    return ConfigurationBasedApplicationListener(configuration)
}

internal data class NameAndScene(val name: String, val scene: Stage)

internal data class SceneAndDeletePrevious(val scene: NameAndScene, val deletePrevious: Boolean, val observable: ReactiveStreamProducer<Stage>) {

    fun notifyComplete() {
        observable.onNext(scene.scene)
        observable.onComplete()
    }

}

open class GenericSceneDirector(
        val moduleFactory: ModuleFactory,
        val renderConveyor: RenderConveyor,
        val initializers: List<SceneDirectorInitializer>, val textureSlotsManager: TextureSlotsManager
) : SceneDirector {

    private val scenes = hashMapOf<String, () -> ModuleDefinition<out Stage>>()

    private val loadedScenes = hashMapOf<String, Stage>()

    private var schedulesScene: SceneAndDeletePrevious? = null

    var pauseSceneName: String? = null

    private var pausedSceneName: String? = null

    private val currentSceneProperty: Property<NameAndScene?> = property(null)
    private var currentScene: NameAndScene? by currentSceneProperty

    override val scene: Stage? by currentSceneProperty.map { it?.scene }

    private val screenImpl: MutableSize = MutableSize.zero()

    override val screen: ImmutableSize
        get() = screenImpl

    private val inputProcessor = InputMultiplexer()

    override val delta: Float
        get() = Gdx.graphics.deltaTime

    override var debug: Boolean = false

    override fun addInputTranslator(translator: (() -> Eventable?) -> InputProcessor) {
        inputProcessor.addProcessor(translator { scene })
    }

    private fun setupInputProcessor() {
        val processor = Gdx.input.inputProcessor

        if (processor != null)
            inputProcessor.addProcessor(processor)

        Gdx.input.inputProcessor = inputProcessor
    }

    private fun removeInputProcessor() {
        val processor = Gdx.input.inputProcessor
        if (processor is InputMultiplexer) {
            processor.clear()
        }
    }

    override fun addScene(name: String, scene: () -> ModuleDefinition<out Stage>) {
        if (name in scenes)
            throw IllegalArgumentException("Scene with name $name already registered, remove it before")

        scenes.put(name, scene)
    }

    override fun closeScene(name: String) {
        loadedScenes.remove(name)?.fire(DisposeEvent, EventTraverseStrategies.full)
    }

    override fun removeScene(name: String) {
        closeScene(name)
        scenes.remove(name)
    }

    override fun scheduleScene(name: String, deletePrevious: Boolean): ReactiveStream<Stage> {
        val scene = findSceneByName(name)

        val promise = reactiveStreamProducer<Stage>()

        this.schedulesScene = SceneAndDeletePrevious(NameAndScene(name, scene), deletePrevious, promise)

        return promise
    }

    private fun findSceneByName(name: String): Stage {
        val loaded = loadedScenes[name]

        if (loaded != null)
            return loaded

        val stage = scenes[name] ?:
                throw IllegalArgumentException("Stage with name $name not found. Available stages: ${scenes.keys.joinToString()}")

        val scene = moduleFactory.create(stage())

        this.loadedScenes[name] = scene

        return scene
    }

    override fun resize(width: Int, height: Int) {
        screenImpl.size(width.toFloat(), height.toFloat())

        notifyResize()
    }

    private fun notifyResize() {
        scene?.fire(ResizeEvent(screen.width, screen.height), EventTraverseStrategies.full)
    }

    override fun create() {
        setupInputProcessor()
        initializers.forEach { it.initialize(this) }
    }

    override fun render() {
        checkChangeScene()

        updateScene()

        renderScene()
    }

    private fun renderScene() {
        Gls.clear()
        textureSlotsManager.clean()
        scene?.render(delta, renderConveyor)
    }

    private fun updateScene() {
        scene?.update(delta)
    }

    private fun checkChangeScene() {
        val next = schedulesScene
        if (next != null && next.scene.scene.load()) {

            // need unload prev scene
            val old = this.currentScene
            if (next.deletePrevious && old != null) {
                loadedScenes.remove(old.name)
                old.scene.fire(DisposeEvent, EventTraverseStrategies.full)
            }

            this.currentScene = next.scene
            this.schedulesScene = null

            notifyResize()

            next.notifyComplete()
        }
    }

    override fun pause() {
        scene?.fire(PauseEvent, EventTraverseStrategies.full)

        if (pauseSceneName != null) {
            pausedSceneName = currentScene?.name
            scheduleScene(pauseSceneName!!)
        }
    }

    override fun resume() {
        if (pausedSceneName != null) {
            scheduleScene(pausedSceneName!!)
            pausedSceneName = null
        }

        scene?.fire(ResumeEvent, EventTraverseStrategies.full)
    }

    override fun dispose() {
        currentScene = null
        loadedScenes.values.forEach { it.fire(DisposeEvent, EventTraverseStrategies.full) }
        removeInputProcessor()
    }

}