package ru.nobirds.rx.director

import ru.nobirds.rx.SceneDirector
import ru.nobirds.rx.SceneDirectorInitializer
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.AssetCoordinatorImpl
import ru.nobirds.rx.asset.AssetProvider
import ru.nobirds.rx.asset.DefaultAssetFactories
import ru.nobirds.rx.component.ComponentFactory
import ru.nobirds.rx.component.ReflectionComponentFactory
import ru.nobirds.rx.di.Context
import ru.nobirds.rx.di.annotations.Configuration
import ru.nobirds.rx.di.annotations.Definition
import ru.nobirds.rx.gl.utils.SimpleTextureSlotsManager
import ru.nobirds.rx.gl.utils.TextureSlotsManager
import ru.nobirds.rx.module.ModuleFactory
import ru.nobirds.rx.module.ReflectionModuleFactory
import ru.nobirds.rx.render.ImmediateRenderConveyor
import ru.nobirds.rx.render.ProgramaticRenderer
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.render.RendererRegistry
import ru.nobirds.rx.render.ShapesRenderer
import ru.nobirds.rx.render.texture.TextureRenderer
import ru.nobirds.rx.sceneDirectorInitializer
import ru.nobirds.rx.utils.files.GdxResourceManager
import ru.nobirds.rx.utils.files.ResourceManager

@Configuration
open class GenericSceneDirectorConfiguration(val initializer: SceneDirectorInitializer = sceneDirectorInitializer {}) {

    @Definition
    fun resources(): ResourceManager = GdxResourceManager()

    @Definition
    open fun assetsProvider(): AssetProvider = DefaultAssetFactories.createProviderWithDefaults()

    @Definition
    fun assets(resources: ResourceManager, assetProvider: AssetProvider): AssetCoordinator =
            AssetCoordinatorImpl(4, resources, assetProvider)

    @Definition
    fun componentFactory(context: Context): ComponentFactory =
            ReflectionComponentFactory(context)

    @Definition
    fun moduleFactory(context: Context, componentFactory: ComponentFactory): ModuleFactory =
            ReflectionModuleFactory(context, componentFactory)

    @Definition
    fun textureUnits(): TextureSlotsManager = SimpleTextureSlotsManager()

    @Definition
    fun renderConveyor(resources: ResourceManager, textureSlotsManager: TextureSlotsManager): RenderConveyor {
        val renderers = RendererRegistry()

        renderers.register(TextureRenderer(resources, textureSlotsManager))
        renderers.register(ShapesRenderer())
        renderers.register(ProgramaticRenderer())

        return ImmediateRenderConveyor(renderers)
    }

    @Definition
    fun defaultDirectorInitializer(): SceneDirectorInitializer = initializer

    @Definition
    fun director(
            moduleFactory: ModuleFactory,
            renderConveyor: RenderConveyor,
            initializers: List<SceneDirectorInitializer>,
            textureSlotsManager: TextureSlotsManager
    ): SceneDirector = GenericSceneDirector(moduleFactory, renderConveyor, initializers, textureSlotsManager)

}