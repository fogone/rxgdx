package ru.nobirds.rx

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.InputProcessor
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.utils.ImmutableSize

interface SceneDirector : ApplicationListener {

    val scene: Stage?

    val screen: ImmutableSize

    val delta: Float

    var debug: Boolean

    fun addScene(name: String, scene: () -> ModuleDefinition<out Stage>)

    fun closeScene(name: String)

    fun removeScene(name: String)

    fun scheduleScene(name: String, deletePrevious: Boolean = false): ReactiveStream<Stage>

    fun addInputTranslator(translator: (() -> Eventable?) -> InputProcessor)

}
