package ru.nobirds.rx.asset

import ru.nobirds.rx.gl.font.Font
import ru.nobirds.rx.gl.font.fnt.FontMetaParser
import ru.nobirds.rx.gl.font.freetype.FreeTypeFontGenerationParameters
import ru.nobirds.rx.gl.font.freetype.FreeTypeFontGenerator
import ru.nobirds.rx.gl.font.freetype.FreeTypeSupport
import ru.nobirds.rx.gl.font.meta.HorizontalFontMetaPackLayout
import ru.nobirds.rx.gl.font.meta.SingleFontMetaPacker
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.flatMap
import ru.nobirds.rx.reactive.map
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.files.path
import ru.nobirds.rx.utils.files.readBytes
import ru.nobirds.rx.utils.observable.GdxRunner

data class FontGeneratorAssetParameters(val file: String) : AssetParameters<FreeTypeFontGenerator>

class FontGeneratorAsset(parameters: FontGeneratorAssetParameters, val assetCoordinator: AssetCoordinator, val resources: ResourceManager) :
        AbstractAsset<FreeTypeFontGenerator, FontGeneratorAssetParameters>(parameters) {

    override fun load(reactiveStream: ReactiveStream<FontGeneratorAssetParameters>): ReactiveStream<FreeTypeFontGenerator> =
            reactiveStream.map(GdxRunner.runner) { FreeTypeSupport.generator(resources.forRead(parameters.file)) }

}

data class FontAssetParameters(val generator: FontGeneratorAssetParameters, val parameters: FreeTypeFontGenerationParameters) :
        AssetParameters<Font>


@Suppress("UNUSED_PARAMETER")
fun fontAssetFactory(parameters: FontAssetParameters, assetCoordinator: AssetCoordinator, resources: ResourceManager):FontAsset
    = FontAsset(parameters, assetCoordinator)

class FontAsset(parameters: FontAssetParameters, val assetCoordinator: AssetCoordinator) :
        AbstractAsset<Font, FontAssetParameters>(parameters) {

    override fun load(reactiveStream: ReactiveStream<FontAssetParameters>): ReactiveStream<Font> =
            reactiveStream
                    .flatMap { assetCoordinator.require(parameters.generator) }
                    //.map(assetCoordinator.newThreadRunner) { // todo?!
                    .map(GdxRunner.runner) {
                        it.generate(parameters.parameters, SingleFontMetaPacker { HorizontalFontMetaPackLayout() })
                    }
                    .map(GdxRunner.runner) { Font.from(it) }

}

data class BitmapFontAssetParameters(val fontFile:String) : AssetParameters<Font>

class BitmapFontAsset(parameters: BitmapFontAssetParameters, val assetCoordinator: AssetCoordinator, val resources: ResourceManager) :
        AbstractAsset<Font, BitmapFontAssetParameters>(parameters) {

    override fun load(reactiveStream: ReactiveStream<BitmapFontAssetParameters>): ReactiveStream<Font> = reactiveStream
            .map(assetCoordinator.newThreadRunner) {
                val fontFile = resources.forRead(it.fontFile)
                val fontDirectory = resources.sub(fontFile.path)

                FontMetaParser.parse(fontFile) {
                    fontDirectory.forRead(it).readBytes()
                }
            }.map(GdxRunner.runner) { Font.from(it) }

}