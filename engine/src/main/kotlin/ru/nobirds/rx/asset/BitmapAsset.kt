package ru.nobirds.rx.asset

import com.badlogic.gdx.graphics.Pixmap
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.map
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.files.readBytes

data class BitmapAssetParameters(val file: String) : AssetParameters<Pixmap>

class BitmapAsset(parameters: BitmapAssetParameters, val assetCoordinator: AssetCoordinator, val resources: ResourceManager) :
        AbstractAsset<Pixmap, BitmapAssetParameters>(parameters) {

    override fun load(reactiveStream: ReactiveStream<BitmapAssetParameters>): ReactiveStream<Pixmap> = reactiveStream
        .map(assetCoordinator.newThreadRunner) { resources.forRead(it.file).readBytes().let { bytes -> Pixmap(bytes, 0, bytes.size) } }

}