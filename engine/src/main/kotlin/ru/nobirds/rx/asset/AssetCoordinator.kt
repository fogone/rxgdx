package ru.nobirds.rx.asset

import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Runner
import ru.nobirds.rx.reactive.Runners
import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.reactive.subscribe
import ru.nobirds.rx.utils.files.ResourceManager
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

interface AssetCoordinator {

    val newThreadRunner: Runner

    val assetProvider: AssetProvider

    fun <T : Any> require(parameters: AssetParameters<T>): ReactiveStream<T>

    fun <T : Any> require(parameters: AssetParameters<T>, onLoad: (T) -> Unit): Subscription =
            require(parameters).subscribe(onLoad)

    fun notRequired(parameters: AssetParameters<*>)

}

class AssetCoordinatorImpl(threads: Int = 4,
                           val resources: ResourceManager,
                           override val assetProvider: AssetProvider = DefaultAssetFactories.createProviderWithDefaults()) : AssetCoordinator {

    private val executorService: ExecutorService = Executors.newFixedThreadPool(threads)

    override val newThreadRunner: Runner = Runners.executor(executorService)

    private val registry = ConcurrentHashMap<AssetParameters<*>, Asset<*, *>>()

    override fun <T : Any> require(parameters: AssetParameters<T>): ReactiveStream<T> {
        return asset(parameters).get()
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T : Any> asset(parameters: AssetParameters<T>): Asset<T, AssetParameters<T>> = registry.getOrPut(parameters) {
        assetProvider.createAssetByParameters(parameters, this, resources)
    } as Asset<T, AssetParameters<T>>

    override fun notRequired(parameters: AssetParameters<*>) {
        val asset = registry[parameters]

        if (asset != null) {
            asset.retain()
            if (!asset.used)
                registry.remove(parameters)
        }
    }

}
