package ru.nobirds.rx.asset

import ru.nobirds.rx.gl.texture.TextureRegion
import ru.nobirds.rx.gl.texture.region
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.flatMap
import ru.nobirds.rx.reactive.map
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.observable.GdxRunner

data class TextureRegionAssetParameters(val texture: TextureAssetParameters,
                                        val x:Int, val y:Int, val width:Int, val height:Int) : AssetParameters<TextureRegion>

@Suppress("UNUSED_PARAMETER")
fun textureRegionAssetFactory(
        parameters: TextureRegionAssetParameters,
        assetCoordinator: AssetCoordinator,
        resources: ResourceManager):TextureRegionAsset = TextureRegionAsset(parameters, assetCoordinator)

class TextureRegionAsset(parameters: TextureRegionAssetParameters, val assetCoordinator: AssetCoordinator) :
        AbstractAsset<TextureRegion, TextureRegionAssetParameters>(parameters) {

    override fun load(reactiveStream: ReactiveStream<TextureRegionAssetParameters>): ReactiveStream<TextureRegion> =
            reactiveStream
                    .flatMap { assetCoordinator.require(parameters.texture) }
                    .map(GdxRunner.runner) { it.region(parameters.x, parameters.y, parameters.width, parameters.height) }

}
