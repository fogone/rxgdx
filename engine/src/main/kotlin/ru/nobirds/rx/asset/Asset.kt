package ru.nobirds.rx.asset

import ru.nobirds.rx.gl.Disposable
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.reactive.buffered
import ru.nobirds.rx.reactive.reactiveStream
import ru.nobirds.rx.reactive.subscribe

interface AssetParameters<T:Any>

interface Asset<T:Any, out P:AssetParameters<T>> : Disposable {

    val used:Boolean

    val parameters:P

    fun get(): ReactiveStream<T>

    fun retain()

}

abstract class AbstractAsset<T:Any, P:AssetParameters<T>>(override final val parameters: P) : Asset<T, P> {

    private var counter:Int = 0

    protected var asset:T? = null

    protected val consumer = reactiveStream(parameters)

    override val used: Boolean
        get() = counter > 0

    private val impl by lazy { load(consumer).buffered() }

    private val subscription:Subscription by lazy { impl.subscribe { asset = it } }

    override fun get(): ReactiveStream<T> {
        counter++
        return impl
    }

    abstract protected fun load(reactiveStream: ReactiveStream<P>): ReactiveStream<T>

    override fun dispose() {
        asset?.let { asset ->
            disposeImpl(asset)

            if(asset is Disposable)
                asset.dispose()
        }

        subscription.unsubscribe()
    }

    override fun retain() {
        counter--
    }

    open protected fun disposeImpl(asset: T) {
        // do nothing
    }

}

