package ru.nobirds.rx.asset

import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.instanceOf
import kotlin.reflect.KClass

interface AssetFactory<T:Any> {

    fun support(parameters: AssetParameters<*>):Boolean

    fun createAsset(parameters: AssetParameters<*>, assetCoordinator: AssetCoordinator, resources: ResourceManager): Asset<T, AssetParameters<T>>

}

inline fun <T:Any, reified P: AssetParameters<T>> assetFactory(noinline factory:(P, AssetCoordinator, ResourceManager)-> Asset<T, P>): AssetFactory<T> =
        SimpleAssetFactory(P::class, factory)

class SimpleAssetFactory<T:Any, P: AssetParameters<T>>(val type: KClass<P>, val factory:(P, AssetCoordinator, ResourceManager)-> Asset<T, P>) : AssetFactory<T> {

    override fun support(parameters: AssetParameters<*>): Boolean
            = parameters instanceOf type

    @Suppress("UNCHECKED_CAST")
    override fun createAsset(parameters: AssetParameters<*>, assetCoordinator: AssetCoordinator, resources: ResourceManager): Asset<T, AssetParameters<T>> =
            factory(parameters as P, assetCoordinator, resources)

}

object DefaultAssetFactories {

    fun createProviderWithDefaults(): AssetProvider = AssetProvider().apply {
        register(this)
    }

    fun register(assetProvider: AssetProvider) {
        defaultFactories().forEach { assetProvider.register(it) }
    }

    private fun defaultFactories(): Sequence<AssetFactory<*>> = sequenceOf(
            assetFactory(::TextureAsset),
            assetFactory(::textureRegionAssetFactory),
            assetFactory(::FontGeneratorAsset),
            assetFactory(::fontAssetFactory),
            assetFactory(::BitmapAsset),
            assetFactory(::BitmapFontAsset)
    )

}

class AssetProvider {

    private val registry = arrayListOf<AssetFactory<*>>()

    fun register(factory: AssetFactory<*>) {
        registry.add(factory)
    }

    inline fun <T:Any, reified P: AssetParameters<T>> register(noinline factory:(P, AssetCoordinator, ResourceManager)-> Asset<T, P>) {
        register(assetFactory(factory))
    }
    
    @Suppress("UNCHECKED_CAST")
    fun <T:Any, P:AssetParameters<T>> createAssetByParameters(parameters: P, assetCoordinator: AssetCoordinator, resources: ResourceManager): Asset<T, P> =
            registry.firstOrNull { it.support(parameters) }?.createAsset(parameters, assetCoordinator, resources) as? Asset<T, P> ?:
                    throw IllegalArgumentException("Unsupported asset: $parameters")

}