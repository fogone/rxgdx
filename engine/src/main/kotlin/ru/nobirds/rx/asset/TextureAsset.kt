package ru.nobirds.rx.asset

import com.badlogic.gdx.graphics.Pixmap
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.texture.TextureSource
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.map
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.observable.GdxRunner

data class TextureAssetParameters(
        val name:String, val format: Pixmap.Format? = null, val genMipMap:Boolean = false) : AssetParameters<Texture>

class TextureAsset(parameters: TextureAssetParameters, val assetCoordinator: AssetCoordinator, val resources: ResourceManager) :
        AbstractAsset<Texture, TextureAssetParameters>(parameters) {

    override fun load(reactiveStream: ReactiveStream<TextureAssetParameters>): ReactiveStream<Texture> =
            reactiveStream
                    .map(assetCoordinator.newThreadRunner) { loadTextureSource(it) }
                    .map(GdxRunner.runner) { Texture.from(it) }

    private fun loadTextureSource(params: TextureAssetParameters) = TextureSource
            .from(resources.forRead(params.name), params.format, params.genMipMap, false) // todo

}
