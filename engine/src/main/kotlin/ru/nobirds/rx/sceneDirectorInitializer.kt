package ru.nobirds.rx

fun sceneDirectorInitializer(initializer: SceneDirector.()->Unit): SceneDirectorInitializer = object : SceneDirectorInitializer {
    override fun initialize(director: SceneDirector) {
        director.initializer()
    }
}

interface SceneDirectorInitializer {

    fun initialize(director: SceneDirector)

}