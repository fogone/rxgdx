package ru.nobirds.rx.module

import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.reactive.Subscription


object ConstructEvent : AbstractEvent()

object AfterModuleAttachedEvent : AbstractEvent()
object AfterModuleAttachedToSceneEvent : AbstractEvent()
object BeforeModuleDetachedEvent : AbstractEvent()

class ChildModuleAttached(val module: Module) : AbstractEvent()
class ChildModuleDetached(val module: Module) : AbstractEvent()

fun Module.onAttached(handler:(AfterModuleAttachedEvent)->Unit): Subscription = onEvent(handler)
fun Module.onAttachedToScene(handler:(AfterModuleAttachedToSceneEvent)->Unit): Subscription = onEvent(handler)
fun Module.onDetached(handler:(BeforeModuleDetachedEvent)->Unit): Subscription = onEvent(handler)

object SetupEvent : AbstractEvent()
object DisposeEvent : AbstractEvent()

fun Eventable.onSetup(handler:()->Unit): Subscription = onEvent<SetupEvent> { handler() }
fun Eventable.onDispose(handler:()->Unit): Subscription = onEvent<DisposeEvent> { handler() }

@OnEvent(SetupEvent::class)
annotation class OnSetup

@OnEvent(DisposeEvent::class)
annotation class OnDispose

