package ru.nobirds.rx.module

import ru.nobirds.rx.component.ComponentFactory
import ru.nobirds.rx.di.Context
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.jvm.javaConstructor

interface ModuleFactory {

    fun <M: Module> create(moduleDefinition: ModuleDefinition<M>):M

}

class ReflectionModuleFactory(val context: Context, val componentFactory: ComponentFactory) : ModuleFactory {

    override fun <M: Module> create(moduleDefinition: ModuleDefinition<M>):M {
        val constructors = moduleDefinition.module.constructors.filter {
            it.parameters.isNotEmpty() && it.parameters.first().let {
                (it.type.classifier as? KClass<*>)?.isSubclassOf(String::class) ?: false
            }
        }

        require(constructors.size == 1) { "More than one compatible constructor for module ${moduleDefinition.module}" }

        val constructor = constructors.first()

        val parametersList = listOf(moduleDefinition.id) + constructor.parameters
                .map { it.type }.drop(1).map { context.find(it.classifier as KClass<*>) }

        val instance = constructor.call(*parametersList.toTypedArray())

        moduleDefinition.eventBinder.bind(instance)

        val components = componentFactory.create(instance, moduleDefinition.components)

        instance.setup(components)

        val initializer = moduleDefinition.initializer

        instance.initializer()

        val children = moduleDefinition.children.map { create(it) }

        children.forEach { instance.attach(it) }

        return instance
    }

}