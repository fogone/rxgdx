package ru.nobirds.rx.module

import ru.nobirds.rx.component.Component
import ru.nobirds.rx.event.Eventable
import kotlin.reflect.KClass

interface Module : Eventable {

    val id:String

    var parent: Module?

    val isAttached:Boolean
        get() = parent != null

    val isAttachedToScene:Boolean
        get() = parent?.isAttachedToScene ?: false

    fun detach() {
        parent?.detach(this)
    }

    fun setup(components:Iterable<Component>)

    val components:Sequence<Component>

    operator fun <C:Component> get(type:KClass<C>):C?

    fun <C:Any> find(type:KClass<C>):Set<C>

    fun contains(component: KClass<out Component>): Boolean

    val children:Sequence<Module>

    fun attach(module: Module, order:Int? = null)

    fun detach(module: Module)

    fun changeOrder(module: Module, order:Int)

}
