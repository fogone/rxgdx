package ru.nobirds.rx.module

import ru.nobirds.rx.component.Component
import ru.nobirds.rx.di.fetchInstanceTypes
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.EventableSupport
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.onChange
import ru.nobirds.rx.property.property
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.reflect.KClass

internal class ModuleContainer {

    private var lastOrder = 0

    private val index = hashMapOf<Module, Int>()

    private val items = ConcurrentHashMap<Int, MutableList<Module>>()

    val modules:Sequence<Module>
        get() = items.values.asSequence().flatMap { it.asSequence() }

    fun add(module: Module, order: Int) {
        index.put(module, order)
        items.getOrPut(order) { CopyOnWriteArrayList() }.add(module)

        if (order > lastOrder) {
            lastOrder = order
        }
    }

    fun add(module: Module) {
        require(lastOrder < Int.MAX_VALUE)

        add(module, ++lastOrder)
    }

    fun remove(module: Module) {
        val order = index.remove(module)
        if (order != null) {
            items[order]?.remove(module)
        }
    }

    fun change(module: Module, order: Int) {
        remove(module)
        add(module, order)
    }

}

internal class ComponentRegistry {

    private var initialized = false

    private val registryByInterface = hashMapOf<KClass<*>, MutableSet<Component>>()
    private val registry = hashMapOf<KClass<*>, Component>()

    val components:Sequence<Component>
        get() = registry.values.asSequence()

    fun initialize(components:Iterable<Component>) {
        require(!initialized) { "This registry already initialized" }

        initialized = true

        for (component in components) {
            val componentClass = component.javaClass.kotlin
            registry.put(componentClass, component)
            registerInterfaces(componentClass, component)
        }
    }

    private fun registerInterfaces(type: KClass<*>, component: Component) {
        for (interfaceClass in type.fetchInstanceTypes()) {
            registryByInterface
                    .getOrPut(interfaceClass) { hashSetOf<Component>() }
                    .add(component)
        }
    }

    fun <C:Component> has(type:KClass<C>):Boolean = registry.containsKey(type)

    @Suppress("UNCHECKED_CAST")
    operator fun <C:Component> get(type:KClass<C>):C? = registry[type] as? C

    @Suppress("UNCHECKED_CAST")
    fun <C:Any> find(type:KClass<C>):Set<C> = registryByInterface[type] as? Set<C> ?: emptySet()

}

abstract class AbstractModule(id:String) : Eventable by EventableSupport(), Module {

    private val componentsRegistry = ComponentRegistry()

    private val childrenModules = ModuleContainer()

    override val components: Sequence<Component>
        get() = componentsRegistry.components

    override val children: Sequence<Module>
        get() = childrenModules.modules

    val idProperty: Property<String> = property(id)
    override var id:String by idProperty

    val parentProperty: Property<Module?> = property<Module?>(null) {
        onChange { _, old, new ->
            old?.detach(this@AbstractModule)
            if(new != null && new.isAttachedToScene && !(old?.isAttachedToScene?:false))
                fire(AfterModuleAttachedToSceneEvent, EventTraverseStrategies.full)
        }
    }
    override var parent: Module? by parentProperty

    override operator fun <C : Component> get(type: KClass<C>): C? = componentsRegistry[type]

    override fun <C : Any> find(type: KClass<C>): Set<C> = componentsRegistry.find(type)

    override fun contains(component: KClass<out Component>): Boolean = componentsRegistry.has(component)

    override fun attach(module: Module, order:Int?) {
        if(order != null) childrenModules.add(module, order)
        else childrenModules.add(module)

        module.parent = this

        fire(AfterModuleAttachedEvent, EventTraverseStrategies.singleAndDependent)
        fire(ChildModuleAttached(module), EventTraverseStrategies.bubble)
    }

    override fun changeOrder(module: Module, order: Int) {
        childrenModules.change(module, order)
    }

    override fun detach(module: Module) {
        fire(ChildModuleDetached(module), EventTraverseStrategies.bubble)
        module.fire(BeforeModuleDetachedEvent, EventTraverseStrategies.singleAndDependent)

        module.parent = null

        childrenModules.remove(module)
    }

    override final fun setup(components: Iterable<Component>) {
        componentsRegistry.initialize(components)
        fire(SetupEvent, EventTraverseStrategies.singleAndDependent)
    }

    override fun parent(): Eventable? = parent

    override fun dependent(): Sequence<Eventable> = componentsRegistry.components

    override fun children(): Sequence<Eventable> = childrenModules.modules

    override fun toString(): String = "#${javaClass.simpleName}[$id]"

}
