package ru.nobirds.rx.module

import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.ComponentDefinition
import ru.nobirds.rx.component.ComponentDefinitionFactory
import ru.nobirds.rx.event.CustomEventBinder
import ru.nobirds.rx.event.CustomEventBinding
import ru.nobirds.rx.event.Event
import ru.nobirds.rx.event.EventBinder
import kotlin.reflect.KClass


class ModuleDefinition<M : Module>(val id: String, val module: KClass<M>, defaultEventBinder: EventBinder<M>) {

    private val childrenImpl = arrayListOf<ModuleDefinition<out Module>>()
    private val componentsImpl = linkedMapOf<KClass<out Component>, ComponentDefinition<out Component>>()

    private val initializers = arrayListOf<M.() -> Unit>()
    private val customBindings = arrayListOf<CustomEventBinding<M, *>>()

    val eventBinder: EventBinder<M> = CustomEventBinder(defaultEventBinder, customBindings)

    val initializer: M.() -> Unit = {
        for (initializer in initializers) {
            initializer()
        }
    }

    @Suppress("UNCHECKED_CAST")
    val children: Iterable<ModuleDefinition<out Module>>
        get() = childrenImpl

    @Suppress("UNCHECKED_CAST")
    val components: Iterable<ComponentDefinition<out Component>>
        get() = componentsImpl.values

    fun withValues(initializer: M.() -> Unit) {
        initializers.add(initializer)
    }

    inline fun <reified E : Event> withEvent(noinline binding: M.(E) -> Unit) {
        withEvent(E::class, binding)
    }

    fun <E : Event> withEvent(type: KClass<E>, binding: M.(E) -> Unit) {
        customBindings.add(CustomEventBinding(type, binding))
    }

    fun withComponents(vararg types: KClass<out Component>) {
        for (type in types) {
            withComponent(type)
        }
    }

    fun <C : Component> withComponent(type: KClass<C>): ComponentDefinition<C> {
        return withComponent(ComponentDefinitionFactory.create(type))
    }

    fun <C : Component> withComponent(type: KClass<C>, initializer: C.() -> Unit): ComponentDefinition<C> {
        return withComponent(ComponentDefinitionFactory.create(type, initializer))
    }

    inline fun <reified C : Component> withComponent(): ComponentDefinition<C> {
        return withComponent(C::class)
    }

    inline fun <reified C : Component> withComponent(noinline initializer: C.() -> Unit): ComponentDefinition<C> {
        return withComponent(C::class, initializer)
    }

    fun <C : Component> withComponent(definition: ComponentDefinition<C>): ComponentDefinition<C> {

        @Suppress("UNCHECKED_CAST")
        val foundDefinition: ComponentDefinition<C>? = componentsImpl[definition.type] as? ComponentDefinition<C>

        val result = if (foundDefinition != null) {
            foundDefinition + definition
        } else definition

        componentsImpl.put(result.type, result)

        return definition
    }

    inline fun <reified M : Module> withModule(id: String, initializer: ModuleDefinition<M>.() -> Unit): ModuleDefinition<M> {
        return withModule(moduleDefinition(id, initializer))
    }

    fun <M : Module> withModule(module: ModuleDefinition<M>): ModuleDefinition<M> {
        childrenImpl.add(module)
        return module
    }

}

inline fun <reified M : Module> moduleDefinition(id: String, initializer: ModuleDefinition<M>.() -> Unit): ModuleDefinition<M> =
        ModuleDefinitionFactory.create(id, M::class).apply(initializer)


inline fun <reified M : Module> module(id: String, moduleFactory: ModuleFactory, initializer: M.() -> Unit): M {
    return moduleFactory.create(ModuleDefinitionFactory.create(id, M::class)).apply(initializer)
}

inline fun <reified M : Module> buildModule(id: String, moduleFactory: ModuleFactory, initializer: ModuleDefinition<M>.() -> Unit): M {
    return moduleFactory.create(moduleDefinition(id, initializer))
}

