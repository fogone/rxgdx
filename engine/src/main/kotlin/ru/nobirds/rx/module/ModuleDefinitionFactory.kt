package ru.nobirds.rx.module

import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.event.annotation.AnnotationEventBinder
import ru.nobirds.rx.event.annotation.EventAnnotationProcessor
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

object ModuleDefinitionFactory {

    private val eventAnnotationProcessor = EventAnnotationProcessor()

    fun <M: Module> create(id:String, type: KClass<M>): ModuleDefinition<M> {
        val eventBinder = AnnotationEventBinder<M>(eventAnnotationProcessor.process(type))

        val definition = ModuleDefinition(id, type, eventBinder)

        val dependentComponents = type.findAnnotation<DependentComponents>()?.value

        if (dependentComponents != null) {
            for (dependentComponent in dependentComponents) {
                definition.withComponent(dependentComponent)
            }
        }

        return definition
    }

}