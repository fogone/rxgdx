package ru.nobirds.rx.module

import ru.nobirds.rx.component.Component
import ru.nobirds.rx.findStage
import kotlin.reflect.KClass
import kotlin.reflect.full.isSuperclassOf

fun Module.parents(): Sequence<Module> =
        if (this.parent != null) generateSequence(this.parent!!) { it.parent } else emptySequence()

fun Module.thisAndParents(): Sequence<Module> =
        sequenceOf(this) + if (this.parent != null) generateSequence(this.parent!!) { it.parent } else emptySequence()

val Module.containers:Sequence<Module>
    get() = children.filter { it.children.any() }

@Suppress("UNCHECKED_CAST")
fun <M:Module> Module.findChildById(id:String, type: KClass<M>): M? {
    return if(this.id == id && type.isSuperclassOf(this::class)) this as M
    else children.map { it.findChildById(id, type) }.firstOrNull { it != null }
}

inline fun <reified M:Module> Module.injectById(id:String):Lazy<M> = lazy {
    findById(id, M::class) ?: throw IllegalArgumentException("Module with id $id not found")
}

inline fun <reified M:Module> Module.injectByIdOptional(id:String):Lazy<M?> = lazy {
    findById(id, M::class)
}

fun <M:Module> Module.findById(id:String, type: KClass<M>):M? = findStage()?.findChildById(id, type)

inline fun <reified C: Component> Module.dependency():Lazy<C> = lazy { findComponent<C>() }

inline fun <reified C: Component> Sequence<Module>.filterByComponent():Sequence<Module> = filter { it.hasComponent<C>() }

inline fun <reified T: Component> Module.hasComponent():Boolean = findOptionalComponent<T>() != null
inline fun <reified T: Component> Module.findComponent():T =
        findOptionalComponent<T>()
                ?: throw IllegalArgumentException("Component of type ${T::class.qualifiedName} not found in container ${this}")

inline fun <reified T: Component> Module.findOptionalComponent():T? = get(T::class)
inline fun <reified T: Any> Module.findComponentsByInterface():Set<T> = find(T::class)
