package ru.nobirds.rx.render.texture

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.utils.BufferUtils
import ru.nobirds.rx.gl.DrawMode
import ru.nobirds.rx.gl.buffer.ArrayBuffer
import ru.nobirds.rx.gl.buffer.ElementsArrayBuffer
import ru.nobirds.rx.gl.buffer.ShortElementsArrayBuffer
import ru.nobirds.rx.gl.mesh.Mesh
import ru.nobirds.rx.gl.mesh.property
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.utils.TextureSlotsManager
import ru.nobirds.rx.utils.array
import java.nio.ByteBuffer
import java.nio.ShortBuffer

interface TextureMeshImplementer {

    val recordSize:Int

    fun createMesh(arrayBuffer: ArrayBuffer, elementsBuffer: ElementsArrayBuffer<ShortBuffer>): Mesh

    fun clean()

    fun setUniforms(mesh: Mesh)

    fun isApplicableTexture(texture: Texture): Boolean

    fun hasAvailableTextureSlots(): Boolean

    fun write(texture: Texture,
              buffer: ByteBuffer,
              x1:Float, y1:Float,
              x2:Float, y2:Float,
              x3:Float, y3:Float,
              x4:Float, y4:Float,
              u1:Float, v1:Float,
              u2:Float, v2:Float,
              color:Float)

}

class TextureMeshImpl(projection: Matrix4,
                      val textureSlotsManager: TextureSlotsManager,
                      val textureMeshImplementer: TextureMeshImplementer,
                      val maxRecordsCount:Int = 5000) : TextureMesh {

    private val vertexCount = 4
    private val quadVertexCount = 6

    private var recordsCount = 0

    private val buffer = BufferUtils.newByteBuffer(maxRecordsCount * textureMeshImplementer.recordSize)

    private val index = BufferUtils.newShortBuffer(maxRecordsCount * quadVertexCount)

    private val deltas = arrayOf(0, 1, 2, 2, 3, 0)

    private fun ShortBuffer.putIndex(index:Int) {
        for (delta in deltas) {
            put((index+delta).toShort())
        }
    }

    private val arrayBuffer: ArrayBuffer = ArrayBuffer(buffer, DrawMode.dynamic)
    private val elementsBuffer: ElementsArrayBuffer<ShortBuffer> = ShortElementsArrayBuffer(index, DrawMode.dynamic)

    private val mesh: Mesh = textureMeshImplementer.createMesh(arrayBuffer, elementsBuffer)

    var projection: Matrix4 by mesh.property("u_projection", projection) { array }

    override fun draw(texture: Texture,
                      x1:Float, y1:Float,
                      x2:Float, y2:Float,
                      x3:Float, y3:Float,
                      x4:Float, y4:Float,
                      u1:Float, v1:Float,
                      u2:Float, v2:Float,
                      tint: Color) {

        val noMoreSpaceInTextureSlotsManager = !textureMeshImplementer.hasAvailableTextureSlots()

        if (isBatchFull() || !textureMeshImplementer.isApplicableTexture(texture) || noMoreSpaceInTextureSlotsManager) {
            flush()

            if(noMoreSpaceInTextureSlotsManager)
                textureSlotsManager.clean()
        }

        textureMeshImplementer.write(
                texture,
                buffer,
                x1, y1,
                x2, y2,
                x3, y3,
                x4, y4,
                u1, v1,
                u2, v2,
                tint.toFloatBits()
        )

        index.putIndex(recordsCount * vertexCount)

        recordsCount++
    }

    private fun isBatchFull(): Boolean {
        return recordsCount == maxRecordsCount
    }

    private fun clean() {
        buffer.clear()
        index.clear()

        recordsCount = 0

        textureMeshImplementer.clean()
    }

    fun flush() {
        if (recordsCount > 0) {
            buffer.flip()
            arrayBuffer.set(buffer)

            index.flip()
            elementsBuffer.set(index)

            textureMeshImplementer.setUniforms(mesh)

            mesh.draw()
        }
        clean()
    }

}