package ru.nobirds.rx.render.texture

import ru.nobirds.rx.gl.DrawPrimitive
import ru.nobirds.rx.gl.NeedsGl3Support
import ru.nobirds.rx.gl.Shader
import ru.nobirds.rx.gl.buffer.ArrayBuffer
import ru.nobirds.rx.gl.buffer.ElementsArrayBuffer
import ru.nobirds.rx.gl.mesh.Mesh
import ru.nobirds.rx.gl.mesh.mesh
import ru.nobirds.rx.gl.shader
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.utils.GlTypes
import ru.nobirds.rx.gl.utils.TextureSlotsManager
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.files.readText
import ru.nobirds.rx.utils.put
import java.nio.ByteBuffer
import java.nio.ShortBuffer

private class TextureArray(val size: Int) {

    private var dirty = false

    private val index = hashMapOf<Int, Int>()
    private val array = IntArray(size)

    private var nextIndex = 0

    val full:Boolean
        get() = nextIndex + 1 >= size

    fun position(texture: Int):Int = index.getOrPut(texture) { add(texture) }

    private fun add(texture: Int): Int {
        require(!full)

        dirty = true

        val idx = nextIndex++

        for (i in idx..array.size - 1) {
            array[i] = texture
        }

        return idx
    }

    fun clear() {
        dirty = true
        nextIndex = 0
        index.clear()
    }

    fun isDirty():Boolean = dirty

    fun get():IntArray = array

    operator fun contains(texture: Int):Boolean = texture in index

    fun findUnbindedCount(textureSlotsManager: TextureSlotsManager): Int {
        return index.count { !textureSlotsManager.binded(it.key) }
    }

}

@NeedsGl3Support
class BatchTextureMeshImplementer(
        val textureSlotsManager: TextureSlotsManager, val resources: ResourceManager) : TextureMeshImplementer {

    private val textures = TextureArray(10)

    override val recordSize: Int =  4 * 9

    override fun createMesh(arrayBuffer: ArrayBuffer, elementsBuffer: ElementsArrayBuffer<ShortBuffer>): Mesh {
        return mesh(createShader(), DrawPrimitive.triangles) {
            withBuffer(arrayBuffer)
            withIndex(elementsBuffer)
        }
    }

    override fun clean() {
        textures.clear()
    }

    override fun write(texture: Texture,
                       buffer: ByteBuffer,
                       x1: Float, y1: Float,
                       x2: Float, y2: Float,
                       x3: Float, y3: Float,
                       x4: Float, y4: Float,
                       u1: Float, v1: Float,
                       u2: Float, v2: Float,
                       color:Float) {

        val index = textures.position(texture.gl).toFloat()

        buffer.put(
                index, x1, y1, u1, v1, color,
                index, x2, y2, u1, v2, color,
                index, x3, y3, u2, v2, color,
                index, x4, y4, u2, v1, color)
    }

    override fun isApplicableTexture(texture: Texture): Boolean {
        return texture.gl in textures || !textures.full
    }

    override fun hasAvailableTextureSlots(): Boolean {
        return textureSlotsManager.hasAvailableSlots(textures.findUnbindedCount(textureSlotsManager))
    }

    override fun setUniforms(mesh: Mesh) {
        if (textures.isDirty()) {
            mesh["u_textures"] = textures.get()
        }
    }

    private fun createShader(): Shader {
        return shader(textureSlotsManager) {
            vertex(resources.forRead("shaders/texture.batch.vertex.glsl").readText())
            fragment(resources.forRead("shaders/texture.batch.fragment.glsl").readText())

            attributes {
                attribute("a_texture_number")
                attribute("a_position")
                attribute("a_texture_position")
                attribute("a_tint_color", true, GlTypes.UNSIGNED_BYTE)
            }
        }
    }
}
