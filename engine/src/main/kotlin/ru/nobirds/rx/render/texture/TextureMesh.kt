package ru.nobirds.rx.render.texture

import com.badlogic.gdx.graphics.Color
import ru.nobirds.rx.gl.texture.Texture

interface TextureMesh {

    fun draw(texture: Texture,
             x1:Float, y1:Float,
             x2:Float, y2:Float,
             x3:Float, y3:Float,
             x4:Float, y4:Float,
             u1:Float, v1:Float,
             u2:Float, v2:Float,
             tint: Color = Color.WHITE)

}
