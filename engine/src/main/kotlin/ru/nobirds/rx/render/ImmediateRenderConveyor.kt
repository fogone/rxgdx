package ru.nobirds.rx.render

import ru.nobirds.rx.NoneProjection
import ru.nobirds.rx.Projection
import ru.nobirds.rx.gl.utils.Gls


class ImmediateRenderConveyor(val renderers:RendererRegistry, val blend:Boolean = true) : RenderConveyor {

    private var projection:Projection = NoneProjection

    private var renderer:Renderer? = null

    override fun begin(projection:Projection) {
        this.projection = projection

        if(blend)
            Gls.enableBlend()
    }

    override fun end() {
        renderer?.flush()
    }

    override fun render(renderable: RenderConveyorItem) {
        val support = renderer?.support(renderable) ?: false

        if (!support) {
//            val from = renderer?.javaClass?.name

            renderer?.flush()
            renderer = renderers.find(renderable)

            // println("$from -> ${renderer?.javaClass?.name}")
        }

        renderer?.render(projection, renderable)
    }
}

class RendererRegistry {

    private val renderers = arrayListOf<Renderer>()

    fun register(renderer: Renderer) {
        this.renderers.add(renderer)
    }

    fun find(item: RenderConveyorItem):Renderer = renderers.first { it.support(item) }

}
