package ru.nobirds.rx.render

import ru.nobirds.rx.component.Component
import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.event.onEvent
import kotlin.properties.Delegates

abstract class AbstractRenderEvent() : AbstractEvent() {

    var delta: Float = 0f

    var renderConveyor: RenderConveyor by Delegates.notNull()

    fun withDeltaAndRenderer(delta: Float, renderer: RenderConveyor): AbstractRenderEvent = apply {
        this.delta = delta
        this.renderConveyor = renderer
    }

}

class RenderEvent() : AbstractRenderEvent()
class DebugRenderEvent() : AbstractRenderEvent()

fun Component.onRender(block:(RenderEvent)->Unit) = onEvent(block)

@OnEvent(RenderEvent::class)
annotation class OnRender