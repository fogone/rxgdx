package ru.nobirds.rx.render.texture

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Affine2
import ru.nobirds.rx.NoneProjection
import ru.nobirds.rx.Projection
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.texture.TextureRegion
import ru.nobirds.rx.gl.utils.Gls
import ru.nobirds.rx.gl.utils.TextureSlotsManager
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.render.RenderConveyorItem
import ru.nobirds.rx.render.Renderer
import ru.nobirds.rx.utils.ImmutableBounds
import ru.nobirds.rx.utils.ImmutableTransform
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.poolable
import ru.nobirds.rx.utils.x1
import ru.nobirds.rx.utils.x2
import ru.nobirds.rx.utils.x3
import ru.nobirds.rx.utils.x4
import ru.nobirds.rx.utils.y1
import ru.nobirds.rx.utils.y2
import ru.nobirds.rx.utils.y3
import ru.nobirds.rx.utils.y4

data class TextureRenderConveyorItem(var texture: Texture? = null,
                                     var x1: Float = 0f, var y1: Float = 0f,
                                     var x2: Float = 0f, var y2: Float = 0f,
                                     var x3: Float = 0f, var y3: Float = 0f,
                                     var x4: Float = 0f, var y4: Float = 0f,
                                     var u1: Float = 0f, var v1: Float = 0f,
                                     var u2: Float = 0f, var v2: Float = 0f,
                                     var tint: Color = Color.WHITE) : RenderConveyorItem {

    fun set(region: TextureRegion, transform: ImmutableTransform, tint: Color = Color.WHITE) {
        val affine = transform.toAffine()

        val width = region.width.toFloat()
        val height = region.height.toFloat()

        set(region, affine, width, height, tint)
    }

    fun set(region: TextureRegion, transform: ImmutableTransform,
            bounds: ImmutableBounds, tint: Color = Color.WHITE) = poolable {
        val tmp = obtain<Affine2>().set(transform.toAffine())

        tmp.translate(bounds.x, bounds.y)

        set(region, tmp, bounds.width, bounds.height, tint)
    }

    fun set(texture: Texture, affine: Affine2,
            width: Float, height: Float,
            u1: Float, v1: Float,
            u2: Float, v2: Float,
            tint: Color = Color.WHITE) {

        val x1 = affine.x1()
        val y1 = affine.y1()
        val x2 = affine.x2(height)
        val y2 = affine.y2(height)
        val x3 = affine.x3(width, height)
        val y3 = affine.y3(width, height)
        val x4 = affine.x4(width)
        val y4 = affine.y4(width)

        set(texture,
                x1, y1, x2, y2, x3, y3, x4, y4,
                u1, v1, u2, v2, tint)

    }

    fun set(region: TextureRegion, affine: Affine2, width: Float, height: Float, tint: Color = Color.WHITE) {
        set(region.texture, affine, width, height,
                region.region.from.u, region.region.to.v, region.region.to.u, region.region.from.v, tint)
    }

    fun set(region: TextureRegion, affine: Affine2, tint: Color = Color.WHITE) {
        set(region.texture, affine, region.width.toFloat(), region.height.toFloat(),
                region.region.from.u, region.region.to.v, region.region.to.u, region.region.from.v, tint)
    }

    fun set(texture: Texture,
            x1: Float, y1: Float,
            x2: Float, y2: Float,
            x3: Float, y3: Float,
            x4: Float, y4: Float,
            u1: Float, v1: Float,
            u2: Float, v2: Float,
            tint: Color = Color.WHITE) {
        this.texture = texture

        this.x1 = x1
        this.x2 = x2
        this.x3 = x3
        this.x4 = x4

        this.y1 = y1
        this.y2 = y2
        this.y3 = y3
        this.y4 = y4

        this.u1 = u1
        this.u2 = u2
        this.v1 = v1
        this.v2 = v2

        this.tint = tint
    }
}

class TextureRenderer(val resources: ResourceManager, textureSlotsManager: TextureSlotsManager) : Renderer {

    private val textureMeshImplementer =
            if(Gls.gl3Supported)
                BatchTextureMeshImplementer(textureSlotsManager, resources)
            else
                SingleTextureMeshImplementer(textureSlotsManager, resources)

    private val textureMesh = TextureMeshImpl(NoneProjection.matrix, textureSlotsManager, textureMeshImplementer)

    override fun support(item: RenderConveyorItem): Boolean = item is TextureRenderConveyorItem

    override fun render(projection: Projection, item: RenderConveyorItem) {
        textureMesh.projection = projection.matrix
        with(item as TextureRenderConveyorItem) {
            textureMesh.draw(texture!!, x1, y1, x2, y2, x3, y3, x4, y4, u1, v1, u2, v2, tint)
        }
    }

    override fun flush() {
        textureMesh.flush()
    }

}

fun RenderConveyor.render(region: TextureRegion, transform: ImmutableTransform, tint: Color = Color.WHITE) = poolable {
    render(obtain<TextureRenderConveyorItem> { set(region, transform, tint) })
}

fun RenderConveyor.render(region: TextureRegion, transform: ImmutableTransform,
                          bounds: ImmutableBounds, tint: Color = Color.WHITE) = poolable {
    render(obtain<TextureRenderConveyorItem> { set(region, transform, bounds, tint) })
}
