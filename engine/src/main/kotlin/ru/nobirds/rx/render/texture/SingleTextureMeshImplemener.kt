package ru.nobirds.rx.render.texture

import ru.nobirds.rx.gl.DrawPrimitive
import ru.nobirds.rx.gl.Shader
import ru.nobirds.rx.gl.buffer.ArrayBuffer
import ru.nobirds.rx.gl.buffer.ElementsArrayBuffer
import ru.nobirds.rx.gl.mesh.Mesh
import ru.nobirds.rx.gl.mesh.mesh
import ru.nobirds.rx.gl.shader
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.utils.GlTypes
import ru.nobirds.rx.gl.utils.TextureSlotsManager
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.files.readText
import ru.nobirds.rx.utils.put
import java.nio.ByteBuffer
import java.nio.ShortBuffer

class SingleTextureMeshImplementer(
        val textureSlotsManager: TextureSlotsManager, val resources: ResourceManager) : TextureMeshImplementer {

    override fun createMesh(arrayBuffer: ArrayBuffer, elementsBuffer: ElementsArrayBuffer<ShortBuffer>): Mesh {
        return mesh(createShader(), DrawPrimitive.triangles) {
            withBuffer(arrayBuffer)
            withIndex(elementsBuffer)
        }
    }

    private var textureChanged: Boolean = false
    private var texture: Texture? = null
        set(value) {
            field = value
            textureChanged = true
        }

    override val recordSize: Int = 4 * 8

    override fun write(texture: Texture,
                       buffer: ByteBuffer,
                       x1:Float, y1:Float,
                       x2:Float, y2:Float,
                       x3:Float, y3:Float,
                       x4:Float, y4:Float,
                       u1:Float, v1:Float,
                       u2:Float, v2:Float,
                       color:Float) {

        buffer.put(
                x1, y1, u1, v1, color,
                x2, y2, u1, v2, color,
                x3, y3, u2, v2, color,
                x4, y4, u2, v1, color)

        this.texture = texture
    }

    override fun isApplicableTexture(texture: Texture): Boolean = this.texture?.let { texture.gl == it.gl } ?: true

    override fun hasAvailableTextureSlots(): Boolean {
        return texture?.let {
            if(textureSlotsManager.binded(it.gl)) true
            else textureSlotsManager.hasAvailableSlots(1)
        } ?: true
    }

    override fun clean() {
        this.texture = null
    }

    override fun setUniforms(mesh: Mesh) {
        texture?.let {
            if (textureChanged) {
                mesh["u_texture"] = it
            }
        }
    }

    private fun createShader(): Shader {
        return shader(textureSlotsManager) {
            vertex(resources.forRead("shaders/texture.vertex.glsl").readText())
            fragment(resources.forRead("shaders/texture.fragment.glsl").readText())

            attributes {
                attribute("a_position")
                attribute("a_texture_position")
                attribute("a_tint_color", true, GlTypes.UNSIGNED_BYTE)
            }
        }
    }


}
