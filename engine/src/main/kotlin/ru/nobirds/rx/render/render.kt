package ru.nobirds.rx.render

import ru.nobirds.rx.Projection

interface RenderConveyorItem

interface Renderer {

    fun support(item:RenderConveyorItem):Boolean

    fun render(projection: Projection, item:RenderConveyorItem)

    fun flush()

}

interface RenderConveyor {

    fun begin(projection:Projection)

    fun end()

    fun render(renderable: RenderConveyorItem)

}

fun RenderConveyor.render(projection:Projection, block:()->Unit) {
    try {
        begin(projection)
        block()
    } finally {
        end()
    }
}

data class ProgramaticRenderConveyorItem(var block: () -> Unit) : RenderConveyorItem

class ProgramaticRenderer() : Renderer {

    override fun support(item: RenderConveyorItem): Boolean = item is ProgramaticRenderConveyorItem

    override fun render(projection: Projection, item: RenderConveyorItem) {
        (item as ProgramaticRenderConveyorItem).block()
    }

    override fun flush() {
        // do nothing
    }
}