package ru.nobirds.rx.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import ru.nobirds.rx.NoneProjection
import ru.nobirds.rx.Projection
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.ImmutableTransform
import ru.nobirds.rx.utils.poolable
import ru.nobirds.rx.utils.x1
import ru.nobirds.rx.utils.x2
import ru.nobirds.rx.utils.x3
import ru.nobirds.rx.utils.x4
import ru.nobirds.rx.utils.y1
import ru.nobirds.rx.utils.y2
import ru.nobirds.rx.utils.y3
import ru.nobirds.rx.utils.y4
import kotlin.properties.Delegates

class ShapesMesh(projection: Matrix4) {

    var projection: Matrix4 by Delegates.observable(projection) { _, _, n ->
        shapeRenderer.projectionMatrix = n
    }

    private val shapeRenderer = ShapeRenderer().apply {
        setAutoShapeType(true)
    }

    fun flush() {
        shapeRenderer.flush()
    }

    fun draw(vertices: FloatArray,
             color: Color) {

        if (!shapeRenderer.isDrawing)
            shapeRenderer.begin()

        if (shapeRenderer.color != color)
            shapeRenderer.color = color

        shapeRenderer.polyline(vertices)
    }

}

data class ShapesRenderConveyorItem(var vertices: FloatArray = FloatArray(0),
                                    var color: Color = Color.WHITE) : RenderConveyorItem {

    fun set(transform: ImmutableTransform, size: ImmutableSize, color: Color) = poolable {
        val affine = transform.toAffine()

        val width = size.width
        val height = size.height

        val x1 = affine.x1()
        val y1 = affine.y1()
        val x2 = affine.x2(height)
        val y2 = affine.y2(height)
        val x3 = affine.x3(width, height)
        val y3 = affine.y3(width, height)
        val x4 = affine.x4(width)
        val y4 = affine.y4(width)

        set(x1, y1, x2, y2, x3, y3, x4, y4, color)
    }

    fun set(x: Float, y: Float, width: Float, height: Float, color: Color) {
        set(x, y,
                x + width, y,
                x + width, y + height,
                x, y + height,
                color)
    }

    fun set(x1: Float, y1: Float,
            x2: Float, y2: Float,
            x3: Float, y3: Float,
            x4: Float, y4: Float,
            color: Color) {

        if (vertices.size != 10) {
            vertices = FloatArray(10)
        }

        vertices[0] = x1; vertices[1] = y1
        vertices[2] = x2; vertices[3] = y2
        vertices[4] = x3; vertices[5] = y3
        vertices[6] = x4; vertices[7] = y4
        vertices[8] = x1; vertices[9] = y1

        this.color = color
    }

    fun set(vertices: FloatArray, color: Color) {
        this.vertices = vertices
        this.color = color
    }
}

class ShapesRenderer() : Renderer {

    private val mesh = ShapesMesh(NoneProjection.matrix)

    override fun support(item: RenderConveyorItem): Boolean = item is ShapesRenderConveyorItem

    override fun render(projection: Projection, item: RenderConveyorItem) {
        mesh.projection = projection.matrix
        with(item as ShapesRenderConveyorItem) {
            mesh.draw(vertices, color)
        }
    }

    override fun flush() {
        mesh.flush()
    }
}

fun RenderConveyor.renderRect(transform: ImmutableTransform, size: ImmutableSize, color: Color) = poolable {
    render(obtain<ShapesRenderConveyorItem> { set(transform, size, color) })
}