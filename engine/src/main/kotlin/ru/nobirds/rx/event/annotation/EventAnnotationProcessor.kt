package ru.nobirds.rx.event.annotation

import ru.nobirds.rx.event.Eventable
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.findAnnotation

class EventAnnotationProcessor {

    fun process(eventableType: KClass<out Eventable>): List<AnnotationEventBinding> {
        val type = eventableType

        val methods = type.members

        val bindings = methods
                .flatMap { findBindings(it) }
                .filterNotNull()

        return bindings
    }

    private data class EventFilterAndParameters(val filter: EventFilter<Annotation>, val parameters: Annotation)

    private fun findBindings(method: KCallable<*>): List<AnnotationEventBinding> {
        return method.annotations
                .map { it.findEventFilterAndParameters() }
                .filterNotNull()
                .map { AnnotationEventBinding(method, it.filter, it.parameters) }
    }

    private fun Annotation.findEventFilterAndParameters(): EventFilterAndParameters? {
        val eventFilter = findEventFilter()

        if (eventFilter != null) {
            return EventFilterAndParameters(eventFilter, this)
        }

        return annotationClass.annotations.asSequence().map { it.findEventFilterAndParameters() }.firstOrNull()
    }

    private fun Annotation.findEventFilter(): EventFilter<Annotation>? {
        val filter = annotationClass.findAnnotation<FilterEvent>()

        val eventFilterType = filter?.value

        val instance = eventFilterType?.createInstance()

        @Suppress("UNCHECKED_CAST")
        return instance as EventFilter<Annotation>? // todo
    }
}