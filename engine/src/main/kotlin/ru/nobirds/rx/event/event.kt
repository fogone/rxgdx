package ru.nobirds.rx.event

import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.reactive.filterIs
import ru.nobirds.rx.reactive.subscribe
import ru.nobirds.rx.utils.Poolable

interface Event : Poolable {

    // var bubbling: Boolean
    // var handled: Boolean

    val live:Boolean

    fun handled():Event

}

abstract class AbstractEvent() : Event {

    // override var bubbling: Boolean = false
    private var handled: Boolean = false

    override val live: Boolean
        get() = !handled

    override fun handled(): Event {
        handled = true
        return this
    }

    override fun reset() {
        handled = false
    }

}

inline fun <reified E:Event> Eventable.onEvent(crossinline handler:(E)->Unit): Subscription
        = events().filterIs<E>().subscribe { handler(it) }

//inline fun <reified E:Event> Eventable.onBubbledEvent(noinline handler:(E)->Unit):Subscription
//        = events().filter { it.bubbling }.filterIs<E>().subscribe(handler)
