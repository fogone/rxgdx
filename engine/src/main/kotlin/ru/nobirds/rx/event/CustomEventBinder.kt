package ru.nobirds.rx.event

import ru.nobirds.rx.reactive.filterIs
import ru.nobirds.rx.reactive.subscribe
import kotlin.reflect.KClass

data class CustomEventBinding<in B : Eventable, E : Event>(val type: KClass<E>, val binding: B.(E) -> Unit) {

    fun bind(eventable: B) {
        eventable.events().filterIs(type).subscribe {
            eventable.binding(it)
        }
    }

}

class CustomEventBinder<in B: Eventable>(val parent: EventBinder<B>, val bindings: List<CustomEventBinding<B, *>>) : EventBinder<B> {

    override fun bind(eventable: B) {
        parent.bind(eventable)
        for (binding in bindings) {
            binding.bind(eventable)
        }
    }

}
