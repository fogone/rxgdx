package ru.nobirds.rx.event

interface EventTraverseStrategy {

    fun traverse(eventable: Eventable, event: Event)

}

object EventTraverseStrategies {

    val single = object : EventTraverseStrategy {
        override fun traverse(eventable: Eventable, event: Event) {
            if (event.live)
                eventable.notify(event)
        }
    }

    val singleAndDependent = object : EventTraverseStrategy {
        override fun traverse(eventable: Eventable, event: Event) {
            if (event.live)
                eventable.notify(event)

            if (event.live)
                for (dependent in eventable.dependent()) {
                    if (event.live)
                        traverse(dependent, event)
                    else
                        break
                }
        }
    }

    val dependentAndChildren = object : EventTraverseStrategy {
        override fun traverse(eventable: Eventable, event: Event) {
            if (event.live)
                for (dependent in eventable.dependent()) {
                    if (event.live)
                        dependent.notify(event)
                    else
                        break

                    if (event.live)
                        traverse(dependent, event)
                    else
                        break
                }

            if (event.live)
                for (child in eventable.children()) {
                    if (event.live)
                        child.notify(event)
                    else
                        break

                    if (event.live)
                        traverse(child, event)
                    else
                        break
                }
        }
    }

    val full = object : EventTraverseStrategy {
        override fun traverse(eventable: Eventable, event: Event) {
            if (event.live)
                eventable.notify(event)

            if (event.live)
                for (dependent in eventable.dependent()) {
                    if (event.live)
                        traverse(dependent, event)
                    else
                        break
                }

            if (event.live)
                for (child in eventable.children()) {
                    if (event.live)
                        traverse(child, event)
                    else
                        break
                }
        }
    }

    val bubble = object : EventTraverseStrategy {
        override fun traverse(eventable: Eventable, event: Event) {
            if (event.live)
                eventable.notify(event)

            if (event.live)
                for (dependent in eventable.dependent()) {
                    if (event.live)
                        dependent.notify(event)
                }

            if (event.live) {
                eventable.parent()?.let { traverse(it, event) }
            }
        }
    }

}