package ru.nobirds.rx.event.annotation

import ru.nobirds.rx.event.Event
import kotlin.reflect.KClass
import kotlin.reflect.full.isSuperclassOf

interface EventFilter<A : Annotation> {

    fun handle(event: Event, annotation: A): Boolean

}

annotation class FilterEvent(val value: KClass<out EventFilter<out Annotation>>)

internal class OnEventFilter : EventFilter<OnEvent> {
    override fun handle(event: Event, annotation: OnEvent): Boolean {
        val types: Array<out KClass<out Event>> = annotation.value
        val eventType = event::class

        return types.any { it.isSuperclassOf(eventType) }
    }
}

@FilterEvent(OnEventFilter::class)
annotation class OnEvent(vararg val value: KClass<out Event>)


