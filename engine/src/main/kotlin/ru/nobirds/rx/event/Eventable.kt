package ru.nobirds.rx.event

import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.reactiveStreamProducer

interface Eventable {

    fun notify(event: Event)

    fun events(): ReactiveStream<Event>

    fun parent(): Eventable? = null

    fun dependent(): Sequence<Eventable> = emptySequence()

    fun children(): Sequence<Eventable> = emptySequence()

}

fun Eventable.fire(event: Event, strategy: EventTraverseStrategy) {
    strategy.traverse(this, event)
}

open class EventableSupport() : Eventable {

    private val impl = reactiveStreamProducer<Event>()

    override fun notify(event: Event) {
        impl.onNext(event)
    }

    override fun events(): ReactiveStream<Event> = impl

}