package ru.nobirds.rx.event.annotation

import ru.nobirds.rx.event.EventBinder
import ru.nobirds.rx.event.Eventable

class AnnotationEventBinder<in B:Eventable>(val bindings: List<AnnotationEventBinding>) : EventBinder<B> {

    override fun bind(eventable: B) {
        bindings.forEach { it.bind(eventable) }
    }

}