package ru.nobirds.rx.event.annotation

import ru.nobirds.rx.event.Event
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.reactive.filter
import ru.nobirds.rx.reactive.subscribe
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

data class AnnotationEventBinding(val method: KCallable<*>, val filter: EventFilter<Annotation>, val parameters:Annotation) {

    fun bind(eventable: Eventable) {
        val invoke = createInvoke(eventable)

        eventable
                .events()
                .filter { filter.handle(it, parameters) }
                .subscribe(invoke)
    }

    private fun createInvoke(eventable: Eventable): (Event) -> Unit {
        val methodParameters = method.parameters.drop(1) // skip receiver

        require(methodParameters.size <= 1) { "Method $method has more than one parameter and could not be binded" }
        if (methodParameters.size == 1) {

            // todo:
            val parameterType = methodParameters.first().type
            require((parameterType.classifier as? KClass<*>)?.isSubclassOf(Event::class) ?: false)
                { "Method $method has incompatible parameter $parameterType" }

            return { method.call(eventable, it) }
        } else {
            return { method.call(eventable) }
        }
    }

}