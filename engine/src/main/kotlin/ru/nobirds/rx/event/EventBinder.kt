package ru.nobirds.rx.event

interface EventBinder<in B: Eventable> {

    fun bind(eventable: B)

}