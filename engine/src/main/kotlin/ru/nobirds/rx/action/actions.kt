package ru.nobirds.rx.action

import com.badlogic.gdx.math.Interpolation

interface Action {

    fun process(delta: Float): Boolean

    fun renew() {}

}

abstract class AbstractTemporalAction(val duration:Float, val interpolation: Interpolation = Interpolation.linear) : Action {

    private var time = 0f
    private var processed = 0f

    override fun process(delta: Float): Boolean {
        time += delta

        val done = time >= duration

        processed = if(done) 1f else interpolation.apply(time / duration)

        update(processed)

        return done
    }

    abstract fun update(processed: Float)

    override fun renew() {
        this.time = 0f
        this.processed = 0f
    }
}

abstract class AbstractRelativeTemporalAction(duration:Float, interpolation: Interpolation = Interpolation.linear) :
        AbstractTemporalAction(duration, interpolation) {

    private var stored = 0f

    override fun update(processed: Float) {
        delta(processed - stored)
        this.stored = processed
    }

    abstract fun delta(percentDelta: Float)

    override fun renew() {
        super.renew()
        this.stored = 0f
    }

}
