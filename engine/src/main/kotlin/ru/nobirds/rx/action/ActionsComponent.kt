package ru.nobirds.rx.action

import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.UpdateEvent
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.reactive.filter
import ru.nobirds.rx.reactive.subscribe
import ru.nobirds.rx.utils.removeAll

class ActionCompleteEvent(val action: Action) : AbstractEvent()

class ActionsComponent(parent: Module) : AbstractComponent(parent) {

    private val actions = arrayListOf<Action>()

    fun run(action: Action) {
        actions.add(action)
    }

    fun runAndSubscribeComplete(action: Action, complete:(Action)->Unit): Subscription {
        run(action)
        return events()
                .filter { it is ActionCompleteEvent && it.action == action }
                .subscribe { complete((it as ActionCompleteEvent).action) }
    }

    @OnUpdate
    fun update(event:UpdateEvent) {
        processActions(event.delta)
    }

    private fun processActions(delta: Float) {
        if (actions.isNotEmpty()) {
            actions.removeAll { it.process(delta) }.forEach {
                fire(ActionCompleteEvent(it), EventTraverseStrategies.single) // todo?
            }
        }
    }

}

fun Module.actions():ActionsComponent = findComponent()

object ActionFactories {

    val sequential:(List<Action>)->Action = { actions -> SequentialAction(actions) }
    val parallel:(List<Action>)->Action = { actions -> ParallelAction(actions) }
    val circle:(List<Action>)->Action = { actions -> CircleAction(actions) }

}

internal class CircleAction(val actions: List<Action>) : Action {

    private val action = SequentialAction(actions)

    override fun process(delta: Float): Boolean {
        if (action.process(delta)) {
            action.renew()
        }

        return false
    }

}

internal val emptyAction = object : Action {
    override fun process(delta: Float): Boolean = true
}


class ActionBuilder(val component: ActionsComponent, val factory:(List<Action>)->Action) {

    private val actions = arrayListOf<Action>()

    fun action(action:Action) {
        this.actions.add(action)
    }

    fun actions(builder:ActionBuilder.()->Unit, factory:(List<Action>)->Action) {
        val actionBuilder = ActionBuilder(component, factory)
        actionBuilder.builder()
        action(actionBuilder.build())
    }

    fun sequential(builder:ActionBuilder.()->Unit) {
        actions(builder, ActionFactories.sequential)
    }

    fun parallel(builder:ActionBuilder.()->Unit) {
        actions(builder, ActionFactories.parallel)
    }

    fun action(action:(Float)->Boolean) {
        action(object: Action {
            override fun process(delta: Float): Boolean = action(delta)
        })
    }

    fun just(block:()->Unit) {
        action { _ -> block(); true }
    }

    fun empty() {
        action(emptyAction)
    }

    fun build():Action = factory(actions)

}



fun ActionsComponent.runSequential(builder:ActionBuilder.()->Unit) {
    runActions(builder, ActionFactories.sequential)
}

fun ActionsComponent.runCircle(builder:ActionBuilder.()->Unit) {
    runActions(builder, ActionFactories.circle)
}

fun ActionsComponent.runParallel(builder:ActionBuilder.()->Unit) {
    runActions(builder, ActionFactories.parallel)
}

fun ActionsComponent.runActions(builder:ActionBuilder.()->Unit, factory:(List<Action>)->Action) {
    val actionBuilder = ActionBuilder(this, factory)
    actionBuilder.builder()
    run(actionBuilder.build())
}