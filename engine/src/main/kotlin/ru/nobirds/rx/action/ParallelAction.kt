package ru.nobirds.rx.action

import ru.nobirds.rx.utils.removeAll

class ParallelAction(val actions: List<Action>) : Action {

    private val active:MutableList<Action> = actions.toMutableList()

    override fun process(delta: Float): Boolean {
        active.removeAll { it.process(delta) }
        return active.isEmpty()
    }

    override fun renew() {
        active.clear()
        active.addAll(actions)
        active.forEach { it.renew() }
    }

}