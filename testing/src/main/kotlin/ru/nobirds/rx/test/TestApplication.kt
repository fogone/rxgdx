package ru.nobirds.rx.test

import com.badlogic.gdx.Application
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.backends.lwjgl.LwjglApplication

interface TestApplication : Application {

    fun join()

}

object EmptyApplicationAdapter : ApplicationAdapter()

class TestLwjglApplication : LwjglApplication(EmptyApplicationAdapter, "", 0, 0), TestApplication {

    override fun join() = mainLoopThread.join()

}