package ru.nobirds.rx.test

import org.junit.runners.model.RunnerScheduler

class GdxScheduler(val application: TestApplication) : RunnerScheduler {

    override fun schedule(childStatement: Runnable) {
        application.postRunnable(childStatement)
    }

    override fun finished() {
        application.exit()
        application.join()
    }
}

