package ru.nobirds.rx.test

import org.junit.runners.BlockJUnit4ClassRunner

open class GdxRunner(type: Class<*>, factory: () -> TestApplication) : BlockJUnit4ClassRunner(type) {

    init {
        setScheduler(GdxScheduler(factory()))
    }

}

class LwjglGdxRunner(type: Class<*>) : GdxRunner(type, ::TestLwjglApplication)