package ru.nobirds.rx.ui.layout

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.input.GestureDetector
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleFactory
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.module.module
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindTwoWay
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.HorizontalAlign
import ru.nobirds.rx.ui.Label
import ru.nobirds.rx.ui.UiModule
import ru.nobirds.rx.ui.VerticalAlign
import ru.nobirds.rx.ui.buildScene
import ru.nobirds.rx.ui.camera.alignAtCenterOfWorld
import ru.nobirds.rx.ui.camera.camera
import ru.nobirds.rx.ui.camera.freeTransform
import ru.nobirds.rx.ui.debugGuiLayer
import ru.nobirds.rx.ui.input.GesturesToEventProcessor
import ru.nobirds.rx.ui.input.InputToEventsProcessor
import ru.nobirds.rx.ui.input.MouseMovedEvent
import ru.nobirds.rx.ui.label
import ru.nobirds.rx.ui.layer
import ru.nobirds.rx.ui.ninepatch.NinePatchComponent
import ru.nobirds.rx.ui.ninepatch.NinePatchLayout
import ru.nobirds.rx.ui.projection
import ru.nobirds.rx.ui.sprite
import ru.nobirds.rx.ui.startLwjglApplication
import ru.nobirds.rx.ui.withScreenBoundsScale
import ru.nobirds.rx.ui.withScreenSize
import ru.nobirds.rx.utils.onChangeToState
import ru.nobirds.rx.utils.stateManager

fun main(args: Array<String>) {

    startLwjglApplication {
        debug = true

        addInputTranslator(::InputToEventsProcessor)
        addInputTranslator { GestureDetector(GesturesToEventProcessor(it)) }

        buildScene("test-scene") {
            layer("main-layer") {

                withScreenSize()

                camera("camera") {
                    freeTransform()
                    alignAtCenterOfWorld()
                }

                projection("camera")

                container("sprite-container") {
                    withScreenBoundsScale()

                    withAlignLayout(VerticalAlign.top, HorizontalAlign.center)

                    vbox("vbox1") {

                        withValues {
                            horizontalAlign = HorizontalAlign.center
                        }

                        withModule<TestButton>("test-button") {
                            withValues {
                                text = "very-very long string..."
                            }
                        }

                        hbox("label-hbox-ttf") {
                            label("test-label", """
Concerns greatest margaret him absolute entrance nay.
Door neat week do find past he. Be no surprise he honoured
indulged. Unpacked endeavor six steepest had husbands her.
Painted no or affixed it so civilly. Exposed neither pressed
so cottage as proceed at offices. Nay they gone sir game four.
Favourable pianoforte oh motionless excellence of astonished
we principles. Warrant present garrets limited cordial in
inquiry to. Supported me sweetness behaviour shameless excellent
so arranging.
Much did had call new drew that kept. Limits expect wonder law
she. Now has you views woman noisy match money rooms. To up remark
it eldest length oh passed. Off because yet mistake feeling has men.
Consulted disposing to moonlight ye extremity. Engage piqued in on
coming.""") {
                                withValues {
                                    font = "OpenSans-CondLight.ttf"
                                    size = 32
                                    align = HorizontalAlign.center
                                }
                            }
                        }

                        vbox("inner-vbox") {
                            (0..8).forEach { i ->
                                sprite("v-sprite$i", "whiteflag.png")
                            }
                        }

                        hbox("inner-hbox") {
                            (0..8).forEach { i ->
                                sprite("h-sprite$i", "whiteflag.png")
                            }
                        }

                        hbox("label-hbox") {
                            label("test-label", """
Concerns greatest margaret him absolute entrance nay.
Door neat week do find past he. Be no surprise he honoured
indulged. Unpacked endeavor six steepest had husbands her.
Painted no or affixed it so civilly. Exposed neither pressed
so cottage as proceed at offices. Nay they gone sir game four.
Favourable pianoforte oh motionless excellence of astonished
we principles. Warrant present garrets limited cordial in
inquiry to. Supported me sweetness behaviour shameless excellent
so arranging.
Much did had call new drew that kept. Limits expect wonder law
she. Now has you views woman noisy match money rooms. To up remark
it eldest length oh passed. Off because yet mistake feeling has men.
Consulted disposing to moonlight ye extremity. Engage piqued in on
coming.""") {
                                withValues {
                                    font = "com/badlogic/gdx/utils/arial-15.fnt" //"OpenSans-CondLight.ttf"
                                    align = HorizontalAlign.center
                                }
                            }
                        }
                    }
                }


            }

            withModule(debugGuiLayer())
        }

        scheduleScene("test-scene")
    }
}

@DependentComponents(NinePatchComponent::class)
class ButtonStateComponent(parent: Module) : AbstractComponent(parent) {

    private val background: NinePatchComponent by dependency()

    val normalImageNameProperty: Property<String?> = property(null)
    var normalImageName: String? by normalImageNameProperty

    val pressedImageNameProperty: Property<String?> = property(null)
    var pressedImageName: String? by pressedImageNameProperty

    val selectedImageNameProperty: Property<String?> = property(null)
    var selectedImageName: String? by selectedImageNameProperty

    private val states = stateManager<String> {
        onChangeToState("normal") {
            normalImageName?.let {
                setBackground(it)
            }
        }
        onChangeToState("pressed") {
            pressedImageName?.let {
                setBackground(it)
            }
        }
        onChangeToState("selected") {
            selectedImageName?.let {
                setBackground(it)
            }
        }
    }

    private fun setBackground(file: String) {
        background.ninePatchName = file
    }

    fun switchTo(state: String) {
        states.switchTo(state)
    }
}
/*

@DependentComponents(ButtonStateComponent::class, NinePatchComponent::class, NinePatchLayout::class)
class StateButton(id: String) : UiModule(id) {

    val states: ButtonStateComponent by dependency()

    val label: LabelComponent by dependency()

    val textProperty = property("")
    var text by textProperty

}

fun ModuleDefinition<*>.testButton(id: String, init: StateButton.() -> Unit) = withModule<StateButton>(id) {

    withComponent<ButtonStateComponent> {
        normalImageName = "btn/btn_default_normal.9.png"
        pressedImageName = "btn/btn_default_pressed.9.png"
        selectedImageName = "btn/btn_default_selected.9.png"

        switchTo("normal")
    }

    withComponent<LabelComponent> {
        name = "OpenSans-CondLight.ttf"
        color = Color.BLACK
        size = 33
    }

    withValues {
        textProperty.bindTwoWay(label.textProperty)
    }

    withValues(init)

    withEvent<MouseMovedEvent> { event ->
        val state = if (bounds.contains(event.worldX, event.worldY))
            "pressed"
        else
            "normal"

        states.switchTo(state)
    }
}
*/

@DependentComponents(NinePatchComponent::class, NinePatchLayout::class, ButtonStateComponent::class)
class TestButton(id:String, moduleFactory: ModuleFactory) : UiModule(id) {

    private val states: ButtonStateComponent by dependency()

    private val label: Label = module("sub-label", moduleFactory) {
        font = "OpenSans-CondLight.ttf"
        color = Color.BLACK
        size = 33
    }

    val textProperty = property("")
    var text by textProperty

    @OnSetup
    fun init() {
        states.normalImageName = "btn/btn_default_normal.9.png"
        states.pressedImageName = "btn/btn_default_pressed.9.png"
        states.selectedImageName = "btn/btn_default_selected.9.png"

        states.switchTo("normal")

        textProperty.bindTwoWay(label.label.textProperty)

        attach(label)
    }

    @OnEvent(MouseMovedEvent::class)
    fun updateState(event: MouseMovedEvent) {
        val state = if (bounds.contains(event.worldX, event.worldY))
            "pressed"
        else
            "normal"

        states.switchTo(state)
    }

}

