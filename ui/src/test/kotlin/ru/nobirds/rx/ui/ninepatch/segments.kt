package ru.nobirds.rx.ui.ninepatch

import org.junit.Assert
import org.junit.Test

class ReduceTest {

    @Test
    fun segments() {
        val segments = sequenceOf(0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0).segments().segments

        Assert.assertEquals(3, segments.size)

        val segment0 = segments[0]

        Assert.assertEquals(false, segment0.filled)
        Assert.assertEquals(0, segment0.from)
        Assert.assertEquals(5, segment0.size)

        val segment1 = segments[1]

        Assert.assertEquals(true, segment1.filled)
        Assert.assertEquals(5, segment1.from)
        Assert.assertEquals(4, segment1.size)

        val segment2 = segments[2]

        Assert.assertEquals(false, segment2.filled)
        Assert.assertEquals(9, segment2.from)
        Assert.assertEquals(3, segment2.size)
    }
}