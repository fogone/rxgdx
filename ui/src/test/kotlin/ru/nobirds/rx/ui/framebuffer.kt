package ru.nobirds.rx.ui

import com.badlogic.gdx.ApplicationAdapter
import ru.nobirds.rx.gl.buffer.framebuffer.FrameBuffer
import ru.nobirds.rx.gl.buffer.framebuffer.frameBuffer
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.utils.Gls
import ru.nobirds.rx.gl.utils.SimpleTextureSlotsManager
import ru.nobirds.rx.utils.files.GdxResourceManager

fun main(args: Array<String>) {
    startLwjglApplication(object : ApplicationAdapter() {

        private val textureSlotsManager by lazy { SimpleTextureSlotsManager() }

        private lateinit var simpleMesh: SimpleMesh
        private lateinit var textureWithMaskMesh: TextureWithMaskMesh
        private lateinit var buffer: FrameBuffer
        private lateinit var mask: Texture

        override fun create() {
            val resourceManager = GdxResourceManager()

            this.simpleMesh = SimpleMesh(resourceManager, textureSlotsManager)

            this.buffer = frameBuffer(320, 200) {
                mask = withTexture()
            }

            val texture = Texture.Companion.from(resourceManager.forRead("whiteflag.png"))
            this.textureWithMaskMesh = TextureWithMaskMesh(resourceManager, textureSlotsManager, texture, mask)
        }

        override fun render() {
            Gls.clear()
            Gls.enableBlend()

//            this.simpleMesh.draw()
            this.simpleMesh.drawTo(buffer)
            this.textureWithMaskMesh.draw()
        }

    })
}