package ru.nobirds.rx.ui

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import ru.nobirds.rx.SceneDirector

fun startLwjglApplication(builder: SceneDirector.()->Unit) {
    LwjglApplication(uiDirector(builder), lwjglApplicationConfiguration())
}

fun startLwjglApplication(listener: ApplicationListener) {
    LwjglApplication(listener, lwjglApplicationConfiguration())
}

private fun lwjglApplicationConfiguration(): LwjglApplicationConfiguration {
    return LwjglApplicationConfiguration().apply {
        vSyncEnabled = true
        fullscreen = false
        useGL30 = true
    }
}