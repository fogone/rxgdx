package ru.nobirds.rx.ui

import ru.nobirds.rx.SceneDirector
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.moduleDefinition
import ru.nobirds.rx.ui.camera.alignAtCenterOfWorld
import ru.nobirds.rx.ui.camera.camera
import ru.nobirds.rx.ui.debug.FpsComponent
import ru.nobirds.rx.ui.input.TapGestureEvent
import ru.nobirds.rx.ui.layout.container
import ru.nobirds.rx.ui.layout.withAlignLayout

class EnableDisableDebugByClickComponent(parent: Module, val director: SceneDirector) : AbstractComponent(parent) {

    @OnEvent(TapGestureEvent::class)
    fun onTap(event: TapGestureEvent) {
        if (event.inBounds) {
            director.debug = !director.debug
            event.handled()
        }
    }

}

fun debugGuiLayer(): ModuleDefinition<out LayerModule> = moduleDefinition("debug-gui-layer") {

    withScreenSize()
    withValues {
        debug = false
    }

    camera("gui-debug-camera") {
        alignAtCenterOfWorld()
    }

    projection("gui-debug-camera")

    container("debug container") {
        withComponent<EnableDisableDebugByClickComponent>()

        withWorldBoundsScale()
        withAlignLayout(VerticalAlign.top, HorizontalAlign.right)

        container("inner debug container") {
            withBounds(50f, 50f)
            withAlignLayout(VerticalAlign.center, HorizontalAlign.center)

            label("fps-label", "0") {
                withComponent<FpsComponent>()
                withValues {
                    text = "0"
                    font = "OpenSans-CondLight.ttf"
                    size = 24
                }
            }
        }
    }
}

