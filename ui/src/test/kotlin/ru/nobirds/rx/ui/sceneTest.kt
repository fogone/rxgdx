package ru.nobirds.rx.ui

import com.badlogic.gdx.input.GestureDetector
import ru.nobirds.rx.SceneDirector
import ru.nobirds.rx.action.ActionsComponent
import ru.nobirds.rx.action.actions
import ru.nobirds.rx.action.runCircle
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.ModuleFactory
import ru.nobirds.rx.ui.action.move
import ru.nobirds.rx.ui.action.rotate
import ru.nobirds.rx.ui.action.scaleTo
import ru.nobirds.rx.ui.alignment.withPivotAtCenter
import ru.nobirds.rx.ui.camera.camera
import ru.nobirds.rx.ui.camera.freeTransform
import ru.nobirds.rx.ui.input.AbstractScreenEvent
import ru.nobirds.rx.ui.input.GesturesToEventProcessor
import ru.nobirds.rx.ui.input.InputToEventsProcessor
import ru.nobirds.rx.ui.input.TapGestureEvent
import ru.nobirds.rx.utils.ImmutableVec2f
import ru.nobirds.rx.utils.Interpolations
import ru.nobirds.rx.utils.immutableVec2
import ru.nobirds.rx.utils.poolable
import ru.nobirds.rx.utils.random
import ru.nobirds.rx.utils.times

fun main(args: Array<String>) {
    startLwjglApplication {
        debug = true

        addInputTranslator(::InputToEventsProcessor)
        addInputTranslator { GestureDetector(GesturesToEventProcessor(it)) }

        buildScene("test-scene") {
            layer("main-layer") {
                withComponent<ImageCopierComponent>()
                withScreenSize()

                camera("camera") {
                    freeTransform()
                }

                projection("camera")
            }

            withModule(debugGuiLayer())
        }

        scheduleScene("test-scene")
    }
}

class ImageCopierComponent(parent: Module, val moduleFactory: ModuleFactory, val director: SceneDirector) : AbstractComponent(parent) {

    private var counter = 0

    private fun newSprite(x:Float, y:Float): ModuleDefinition<Sprite> = spriteDefinition("sprite-${counter++}", "whiteflag.png") {
        withComponent<ActionsComponent>()
        withComponent<ClickToDetachComponent>()
        withPivotAtCenter()
        withValues {
            transform.position(x, y)
        }
    }

    @OnEvent(TapGestureEvent::class)
    fun attachImage(event: AbstractScreenEvent) = poolable {
        event.handled()

        val sprite = moduleFactory.create(newSprite(event.worldX, event.worldY))

        parent.attach(sprite)

        val time = 3f.random() + 3f

        val target = immutableVec2(director.screen.width.random(), director.screen.height.random())

        val actions = sprite.actions()

        actions.runCircle {
            parallel {
                sequential {
                    scaleTo(ImmutableVec2f.Companion.zero, time / 2f, Interpolations.random())
                    scaleTo(3f.random().run { ImmutableVec2f.Companion.readonly(this, this) }, time / 2f, Interpolations.random())
                }
                sequential {
                    move(target, time / 2f, Interpolations.random())
                    move(target * -1f, time / 2f, Interpolations.random())
                }
                rotate(1800f.random(), time, Interpolations.random())
            }
        }

    }
}

class ClickToDetachComponent(parent: Module) : AbstractComponent(parent) {

    @OnEvent(TapGestureEvent::class)
    fun detach(event: TapGestureEvent) {
        parent.detach()
        event.handled()
    }

}

