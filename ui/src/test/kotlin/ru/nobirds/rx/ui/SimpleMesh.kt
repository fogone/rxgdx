package ru.nobirds.rx.ui

import com.badlogic.gdx.graphics.Color
import ru.nobirds.rx.gl.DrawPrimitive
import ru.nobirds.rx.gl.Shader
import ru.nobirds.rx.gl.buffer.framebuffer.FrameBuffer
import ru.nobirds.rx.gl.mesh.Mesh
import ru.nobirds.rx.gl.mesh.mesh
import ru.nobirds.rx.gl.mesh.property
import ru.nobirds.rx.gl.shader
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.utils.GlTypes
import ru.nobirds.rx.gl.utils.TextureSlotsManager
import ru.nobirds.rx.utils.buffer
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.files.readText
import java.nio.ByteBuffer

class SimpleMesh(val resourceManager: ResourceManager, textureSlotsManager: TextureSlotsManager) {

    private val shader: Shader = shader(textureSlotsManager) {
        vertex(resourceManager.forRead("shaders/simple.vertex.glsl").readText())
        fragment(resourceManager.forRead("shaders/simple.fragment.glsl").readText())

        attributes {
            attribute("a_position")
            attribute("a_color", true, GlTypes.UNSIGNED_BYTE)
        }

        ignoreUnknownUniforms()
    }

    private val buffer: ByteBuffer = buffer(
            // position, color
            -1f, -1f, Color.CYAN.toFloatBits(),
            -1f,  1f, Color.MAGENTA.toFloatBits(),
             1f,  1f, Color.BLACK.toFloatBits(),
             1f, -1f, Color.YELLOW.toFloatBits()
    )

    private val mesh: Mesh = mesh(shader, DrawPrimitive.triangleFan) {
        withBuffer(buffer)
        withIndex(buffer(0, 1, 2, 3).asShortBuffer())
    }

    private var speed: Float by mesh.property("u_speed", 100f)
    private var time: Float by mesh.property("u_time", time())

    fun draw() {
        updateValues()
        mesh.draw()
    }

    private fun updateValues() {
        time = time()
    }

    private fun time() = (System.currentTimeMillis()).toShort().toFloat()

    fun drawTo(frameBuffer: FrameBuffer) {
        updateValues()
        mesh.drawTo(frameBuffer)
    }

}

class TextureWithMaskMesh(val resourceManager: ResourceManager,
                          textureSlotsManager: TextureSlotsManager,
                          val texture: Texture, var mask: Texture) {

    private val shader: Shader = shader(textureSlotsManager) {
        vertex(resourceManager.forRead("shaders/texture-with-mask.vertex.glsl").readText())
        fragment(resourceManager.forRead("shaders/texture-with-mask.fragment.glsl").readText())

        attributes {
            attribute("a_position")
        }

        ignoreUnknownUniforms()
    }

    private val buffer: ByteBuffer = buffer(
            // position
            -1f, -1f,
            -1f, 1f,
            1f, 1f,
            1f, -1f
    )

    private val mesh: Mesh = mesh(shader, DrawPrimitive.triangleFan) {
        withBuffer(buffer)
        withIndex(buffer(0, 1, 2, 3).asShortBuffer())
    }.apply {
        set("u_texture", texture)
        set("u_mask", mask)
    }

    fun draw() {
        mesh.draw()
    }

    fun drawTo(frameBuffer: FrameBuffer) {
        mesh.drawTo(frameBuffer)
    }

}