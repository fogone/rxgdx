attribute vec2 a_position;
attribute vec4 a_color;

uniform float u_time;
uniform float u_speed;

varying vec4 v_color;

void main()
{

  float time = floor(u_time/u_speed)*u_speed;

//  v_color = a_color * time;
  v_color = vec4(a_color.x + a_color.x * sin(time), a_color.y + a_color.y * cos(time), a_color.z + a_color.z * sin(time), a_color.a);

  gl_Position = vec4(a_position, 0.0, 1.0);
}
