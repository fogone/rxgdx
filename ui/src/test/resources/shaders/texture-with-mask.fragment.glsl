#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

uniform sampler2D u_texture;
uniform sampler2D u_mask;

varying vec4 v_position;

void main()
{
  gl_FragColor = texture2D(u_texture, (v_position.xy + 1.0)/2.0) * texture2D(u_mask, (v_position.xy + 1.0)/2.0);
}
