package ru.nobirds.rx.ui.action

import com.badlogic.gdx.math.Interpolation
import ru.nobirds.rx.action.AbstractRelativeTemporalAction
import ru.nobirds.rx.action.ActionBuilder
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.ui.PositionComponent
import ru.nobirds.rx.utils.ImmutableVec2f

class MoveAction(target: Module, val transform:ImmutableVec2f, duration:Float, interpolation: Interpolation = Interpolation.linear) :
        AbstractRelativeTemporalAction(duration, interpolation) {

    private val position:PositionComponent = target.findComponent()

    override fun delta(percentDelta: Float) {
        position.translate(transform.x * percentDelta, transform.y * percentDelta)
    }

}

fun ActionBuilder.move(transform:ImmutableVec2f, duration:Float, interpolation: Interpolation = Interpolation.linear) {
    action(MoveAction(component.parent, transform, duration, interpolation))
}

