package ru.nobirds.rx.ui.ninepatch

data class Segment(val type: SegmentType, val index:Int, val from: Int, val size: Int) {

    val filled:Boolean
        get() = type == SegmentType.fill

}

fun Sequence<Segment>.asSegments(): Segments = Segments(this.toList())

data class Segments(val segments: List<Segment>) {

    init {
        checkSegments(segments)
    }

    val filled:Sequence<Segment>
        get() = segments.asSequence().filter { it.filled }

    val empty:Sequence<Segment>
        get() = segments.asSequence().filter { !it.filled }

    private fun checkSegments(segments: List<Segment>) {
        assert(segments.size >= 3) { "No segments found" }
    }

}

enum class SegmentType {

    fill, empty

}

data class NinePatchSegments(val columnSegments: Segments, val rowSegments: Segments,
                             val contentColumnSegments: Segments, val contentRowSegments: Segments)