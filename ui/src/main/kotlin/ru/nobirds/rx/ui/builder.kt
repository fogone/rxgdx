package ru.nobirds.rx.ui

import ru.nobirds.rx.module.ModuleDefinition

fun ModuleDefinition<*>.uiModule(id:String, initializer:ModuleDefinition<UiModule>.()->Unit = {}):ModuleDefinition<UiModule> = withModule(id, initializer)


/*
fun Module.attachViewport(id:String? = null, vararg components:KClass<out Component>, block:ViewportModule.()->Unit = {}):ViewportModule
        = attachModule(ViewportModule(id, *components), block)
*/

//fun Module.attachCamera(id:String? = null, vararg components:KClass<out Component>, block: Camera.()->Unit = {}): Camera
//        = attachModule(Camera(id, *components), block)
//
//inline fun <reified T:Component> Module.setupComponent(block:T.()->Unit):T {
//    val component = findComponent<T>()
//    component.block()
//    return component
//}
//
//
//fun Module.setupPosition(x:Float, y:Float):PositionComponent = setupComponent {
//    position(x, y)
//}
//
//fun Module.setupBoundsDebug(block: DebugBoundsRenderComponent.()->Unit): DebugBoundsRenderComponent = setupComponent(block)
//fun Module.setupBounds(block:BoundsComponent.()->Unit):BoundsComponent = setupComponent(block)
//fun Module.setupBounds(width:Float, height:Float):BoundsComponent = setupBounds {
//    size(width, height)
//}
//fun Module.setupBounds(x:Float, y:Float, width:Float, height:Float):BoundsComponent = setupBounds {
//    bounds(x, y, width, height)
//}
//
//fun Module.setupPreferBounds(block:PreferBoundsComponent.()->Unit):PreferBoundsComponent = setupComponent(block)
//fun Module.setupPreferBounds(prefWidth:Float, prefHeight:Float):PreferBoundsComponent = setupPreferBounds {
//    pref(prefWidth, prefHeight)
//}
//
//fun Module.setupFillParent(block:FillParentComponent.()->Unit):FillParentComponent = setupComponent(block)
//fun Module.setupFillParent(width: Boolean, height: Boolean):FillParentComponent = setupFillParent { fillParent(width, height) }

// fun Module.setupCamera(block: CameraComponent.()->Unit): CameraComponent = setupComponent(block)