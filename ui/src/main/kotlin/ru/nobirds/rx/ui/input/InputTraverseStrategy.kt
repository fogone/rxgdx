package ru.nobirds.rx.ui.input

import ru.nobirds.rx.event.Event
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.EventTraverseStrategy
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.findComponentsByInterface
import ru.nobirds.rx.utils.ImmutableBounds

class InputTraverseStrategy() : EventTraverseStrategy {

    override fun traverse(eventable: Eventable, event: Event) {
        require(event is AbstractScreenEvent) { "This strategy support only screen events" }

        if (eventable is Module && event is AbstractScreenEvent) {
            val x = event.worldX
            val y = event.worldY

            val lastChildren = findLastChildren(eventable, x, y)

            for (child in lastChildren) {
                if(event.live)
                    child.fire(event, EventTraverseStrategies.bubble)
            }
        }
    }

    private fun inBounds(module: Module, x: Float, y: Float): Boolean {
        val bounds = module.findComponentsByInterface<ImmutableBounds>()

        return bounds.any { it.contains(x, y) }
    }

    private fun findLastChildren(module: Module, x: Float, y: Float):Sequence<Module> {
        val inBounds = inBounds(module, x, y)

        if (module.children.any()) {
            val children = module.children.flatMap { findLastChildren(it, x, y) }
            if(children.any())
                return children
        }

        if(inBounds)
            return sequenceOf(module)

        return emptySequence()
    }
}
