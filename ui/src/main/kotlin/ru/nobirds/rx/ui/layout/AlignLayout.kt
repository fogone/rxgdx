package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.component.ComponentDefinition
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.module.findOptionalComponent
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.HorizontalAlign
import ru.nobirds.rx.ui.SpacingComponent
import ru.nobirds.rx.ui.UiModule
import ru.nobirds.rx.ui.VerticalAlign

@DependentComponents(AlignLayout::class)
open class Container(id: String) : UiModule(id) {

    val alignLayout: AlignLayout by dependency()

    fun align(vertical: VerticalAlign, horizontal: HorizontalAlign) {
        alignLayout.align(vertical, horizontal)
    }

}


fun ModuleDefinition<*>.container(id: String, initializer: ModuleDefinition<Container>.() -> Unit): ModuleDefinition<Container> = withModule(id, initializer)

fun ModuleDefinition<*>.withAlignLayout(initializer: AlignLayout.() -> Unit): ComponentDefinition<AlignLayout> = withComponent(initializer)

class AlignLayout(parent: Module) : LayoutComponent(parent) {

    val verticalAlignProperty: Property<VerticalAlign> = property(VerticalAlign.none)
    var verticalAlign: VerticalAlign by verticalAlignProperty

    val horizontalAlignProperty: Property<HorizontalAlign> = property(HorizontalAlign.none)
    var horizontalAlign: HorizontalAlign by horizontalAlignProperty

    override fun updatePreferSize() {
        preferBounds.pref(bounds.width, bounds.height)
        preferBounds.max(bounds.width, bounds.height)
    }

    override fun updateLayout() {
        for (module in childrenWithBounds) {
            val bounds = module.findComponent<BoundsComponent>()
            val margin = module.findOptionalComponent<SpacingComponent>()?.margin ?: 0f

            val x = when (horizontalAlign) {
                HorizontalAlign.left -> 0f + margin
                HorizontalAlign.right -> this.bounds.width - bounds.width - margin
                HorizontalAlign.center -> (this.bounds.width - bounds.width) / 2f
                HorizontalAlign.none -> bounds.x
            }

            val y = when (verticalAlign) {
                VerticalAlign.bottom -> 0f + margin
                VerticalAlign.top -> this.bounds.height - bounds.height - margin
                VerticalAlign.center -> (this.bounds.height - bounds.height) / 2f
                VerticalAlign.none -> bounds.y
            }

            bounds.position(x, y)
        }
    }

    fun align(vertical: VerticalAlign, horizontal: HorizontalAlign) {
        this.verticalAlign = vertical
        this.horizontalAlign = horizontal
    }

}

fun ModuleDefinition<*>.withAlignLayout(
        vertical: VerticalAlign, horizontal: HorizontalAlign):ComponentDefinition<AlignLayout> = withComponent {
    align(vertical, horizontal)
}
