package ru.nobirds.rx.ui

import ru.nobirds.rx.LoadEvent
import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.TextureAssetParameters
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.gl.texture.TextureRegion
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.module.moduleDefinition
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.render.OnRender
import ru.nobirds.rx.render.RenderEvent
import ru.nobirds.rx.render.texture.render

@DependentComponents(SpriteComponent::class)
class Sprite(id: String) : UiModule(id) {

    val sprite: SpriteComponent by dependency()

}

fun ModuleDefinition<*>.sprite(id: String, textureName: String? = null): ModuleDefinition<Sprite> = withModule(id) {
    withComponent<SpriteComponent> {
        this.textureName = textureName
    }
}

fun spriteDefinition(id: String, textureName: String? = null, builder: ModuleDefinition<Sprite>.() -> Unit = {}): ModuleDefinition<Sprite> = moduleDefinition<Sprite>(id) {
    withComponent<SpriteComponent> {
        this.textureName = textureName
    }
    builder()
}

@DependentComponents(
        TransformComponent::class,
        BoundsComponent::class,
        PreferBoundsComponent::class
)
class SpriteComponent(parent: Module, val assets: AssetCoordinator) : AbstractComponent(parent) {

    private val transform: TransformComponent by dependency()
    private val bounds: BoundsComponent by dependency()
    private val prefer: PreferBoundsComponent by dependency()

    val textureNameProperty: Property<String?> = property<String?>(null)

    var textureName: String? by textureNameProperty

    val generateMipMapProperty: Property<Boolean> = property(false)
    val generateMipMap: Boolean by generateMipMapProperty

    private var subscription: Subscription? = null
        set(value) {
            field?.unsubscribe()
            field = value
        }

    private var textureParameters: TextureAssetParameters? = null
    private var texture: TextureRegion? = null

    private val invalidatedParameters = invalidated(false, textureNameProperty, generateMipMapProperty)

    private fun updateSize(width: Int, height: Int) {
        bounds.size(width.toFloat(), height.toFloat())
        prefer.min(width.toFloat(), height.toFloat())
        prefer.pref(width.toFloat(), height.toFloat())
    }

    private fun requestTexture(parameters: TextureAssetParameters) {
        subscription = assets.require(parameters) {
            this.texture = TextureRegion.from(it)
            updateSize(it.width, it.height)
        }
    }

    private fun requestUnloadTexture(parameters: TextureAssetParameters) {
        subscription = null
        assets.notRequired(parameters)
    }

    @OnRender
    fun render(event: RenderEvent) {
        texture?.let {
            event.renderConveyor.render(it, transform)
        }
    }

    @OnEvent(LoadEvent::class)
    fun load(event: LoadEvent) {
        event.complete(texture != null)
    }

    @OnUpdate
    fun update() {
        invalidatedParameters.with {
            textureParameters?.let {
                requestUnloadTexture(it)
            }
            textureName?.let {
                val textureParameters = TextureAssetParameters(it, null, generateMipMap)
                requestTexture(textureParameters)
                this.textureParameters = textureParameters
            }
        }
    }

}