package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.module.findOptionalComponent
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.PreferBoundsComponent

class StackLayoutComponent(parent: Module) : LayoutComponent(parent) {

    override fun updatePreferSize() {
        childrenWithBounds.forEach {
            val size = it.findComponent<BoundsComponent>()
            val preferBounds = it.findOptionalComponent<PreferBoundsComponent>()

            if (preferBounds != null) {
                this.preferBounds.preferBounds(preferBounds)
            } else {
                this.preferBounds.size(size)
            }
        }
    }

    override fun updateLayout() {
        childrenWithBounds.forEach {
            val bounds = it.findComponent<BoundsComponent>()
            bounds.bounds(0f, 0f, this.bounds.width, this.bounds.height)
        }
    }

}