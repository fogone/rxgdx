package ru.nobirds.rx.ui.viewport

import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.ComponentDefinition
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.MutableSize

class WorldSizeChangedEvent(override val width: Float, override val height: Float) : AbstractEvent(), ImmutableSize

class WorldComponent(parent: Module) : AbstractComponent(parent), MutableSize, Invalidatable {

    val widthProperty: Property<Float> = property(0f)
    override var width: Float by widthProperty

    val heightProperty: Property<Float> = property(0f)
    override var height: Float by heightProperty

    private val invalidatedWorld: InvalidatedProperty = invalidated(true, widthProperty, heightProperty)

    override val invalidated: ReactiveStream<Signal> = changed(invalidatedWorld)

    @OnUpdate
    fun update() {
        invalidatedWorld.with {
            parent.fire(WorldSizeChangedEvent(width, height),
                    EventTraverseStrategies.full)
        }
    }

}

fun ModuleDefinition<Module>.setupWorld(width: Float, height: Float): ComponentDefinition<WorldComponent> = withComponent { size(width, height) }

@DependentComponents(WorldComponent::class)
class ScreenWorldScaleComponent(parent: Module) : AbstractComponent(parent) {

    private val world: WorldComponent by dependency()

    @OnEvent(ResizeEvent::class)
    fun updateWorld(event: ResizeEvent) {
        world.size(event)
    }

}
