package ru.nobirds.rx.ui.viewport

import com.badlogic.gdx.math.Vector2
import ru.nobirds.rx.utils.MutableBounds
import ru.nobirds.rx.utils.poolable


interface ViewportScalingStrategy {

    fun scale(viewport: MutableBounds, world:Vector2, screen:Vector2)

}

object ScalingStrategies {

    fun strategy(scale:(Vector2, Vector2, Vector2)-> Vector2): ViewportScalingStrategy = object: ViewportScalingStrategy {
        override fun scale(viewport: MutableBounds, world:Vector2, screen:Vector2) {
            poolable {
                val vector = obtain<Vector2>()
                scale(world, screen, vector)
                viewport.size(vector.x, vector.y)
            }
        }
    }

    fun scalingStrategy(scale:(Vector2, Vector2, Vector2)-> Vector2): ViewportScalingStrategy = strategy { world, screen, result ->
        scale(world, screen, result)
    }

    fun scalingValueStrategy(scale: (Vector2, Vector2) -> Float): ViewportScalingStrategy =
            scalingStrategy { w, s, r ->
                val scaleValue = scale(w, s)
                r.set(w.x * scaleValue, w.y * scaleValue)
            }

    val none: ViewportScalingStrategy = object: ViewportScalingStrategy {
        override fun scale(viewport: MutableBounds, world: Vector2, screen: Vector2) {
        }
    }

    val fit: ViewportScalingStrategy = scalingValueStrategy { w, s ->
        if (s.y / s.x > w.y / w.x)
            s.x / w.x
        else
            s.y / w.y
    }

    val fill: ViewportScalingStrategy = scalingValueStrategy { w, s ->
        if (s.y / s.x < w.y / w.x)
            s.x / w.x
        else
            s.y / w.y
    }

    val fillX: ViewportScalingStrategy = scalingValueStrategy { w, s -> s.x / w.x }
    val fillY: ViewportScalingStrategy = scalingValueStrategy { w, s -> s.y / w.y }

    val stretch: ViewportScalingStrategy = scalingStrategy { _, s, r  -> r.set(s.x, s.y)  }
    val stretchX: ViewportScalingStrategy = scalingStrategy { w, s, r  -> r.set(w.x, s.y)  }
    val stretchY: ViewportScalingStrategy = scalingStrategy { w, s, r  -> r.set(s.x, w.y)  }

}


/*
@DependentComponents(ViewportComponent::class, WorldComponent::class, ScreenComponent::class)
abstract class AbstractViewportScaleComponent() : AbstractComponent(), Invalidatable {

    protected val viewport:ViewportComponent by dependency()
    protected val world:WorldComponent by dependency()
    protected val screen:ScreenComponent by dependency()

    override val invalidated: Observable<Signal> by lazy {  changed(viewport, world, screen) }

    private val invalidatedStrategy:InvalidatedProperty = invalidated()

    override fun setup() {
        invalidatedStrategy.bindOneWaySignal(invalidated)

        onUpdate {
            invalidatedStrategy.with {
                update()
            }
        }
    }

    abstract fun update()

}

class WorldViewportScaleComponent() : AbstractViewportScaleComponent() {

    val strategyProperty: Property<ViewportScalingStrategy> = property(ScalingStrategies.none)
    var strategy: ViewportScalingStrategy by strategyProperty

    override val invalidated: Observable<Signal> by lazy {  changed(strategyProperty, viewport, world, screen) }

    private val screenVector:Vector2 = vector()
    private val worldVector:Vector2 = vector()

    override fun update() {
        worldVector.set(world.worldWidth, world.worldHeight)
        screenVector.set(screen.screenWidth, screen.screenHeight)
        strategy.scale(viewport, worldVector, screenVector)
    }

}

class ScreenViewportScaleComponent() : AbstractViewportScaleComponent() {

    val unitsPerPixelProperty:Property<Float> = property(1f)
    var unitsPerPixel:Float by unitsPerPixelProperty

    override fun update() {
        viewport.set(0f, 0f, screen.screenWidth, screen.screenHeight)
        world.set(screen.screenWidth * unitsPerPixel, screen.screenHeight * unitsPerPixel)
    }
}
*/


/*

    fun extended(minWorldWidth:Float, minWorldHeight:Float,
                 maxWorldWidth:Float, maxWorldHeight:Float): ViewportScalingStrategy = object: ViewportScalingStrategy {

        override fun scale(viewport: Viewport, w: Vector2, s: Vector2) {
            var worldWidth = minWorldWidth
            var worldHeight = minWorldHeight

            val scale = if (s.y / s.x > worldHeight / worldWidth)
                s.x / worldWidth
            else
                s.y / worldHeight

            var viewportWidth = w.x * scale
            var viewportHeight = w.y * scale

            when {
                viewportWidth < s.x -> {
                    val toViewportSpace = viewportHeight / worldHeight
                    val toWorldSpace = worldHeight / viewportHeight

                    var lengthen = (s.x - viewportWidth) * toWorldSpace
                    if (maxWorldWidth > 0) lengthen = Math.min(lengthen, maxWorldWidth - minWorldWidth)
                    worldWidth += lengthen
                    viewportWidth += Math.round(lengthen * toViewportSpace)
                }
                viewportHeight < s.y -> {
                    val toViewportSpace = viewportWidth / worldWidth
                    val toWorldSpace = worldWidth / viewportWidth

                    var lengthen = (s.y - viewportHeight) * toWorldSpace
                    if (maxWorldHeight > 0) lengthen = Math.min(lengthen, maxWorldHeight - minWorldHeight)
                    worldHeight += lengthen
                    viewportHeight += Math.round(lengthen * toViewportSpace)
                }
            }

            viewport.size(viewportWidth, viewportHeight)
        }

    }


*/