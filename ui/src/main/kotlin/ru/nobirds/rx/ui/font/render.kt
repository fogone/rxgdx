package ru.nobirds.rx.ui.font

import com.badlogic.gdx.math.Affine2
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.render.texture.TextureRenderConveyorItem
import ru.nobirds.rx.utils.ImmutableTransform
import ru.nobirds.rx.utils.poolable

fun RenderConveyor.render(layout: TextLayout, transform: ImmutableTransform) = poolable {
    val affine = obtain<Affine2>()
    val subAffine = obtain<Affine2>()

    val textureItem = obtain<TextureRenderConveyorItem>()

    for (run in layout.runes) {
        affine.set(transform.toAffine()).translate(run.x, run.y)

        for (glyph in run.glyphs) {
            val transformation = subAffine.set(affine).translate(glyph.xOffset, glyph.yOffset)
            textureItem.set(glyph.glyph.region, transformation, run.color)
            render(textureItem)
        }
    }

}