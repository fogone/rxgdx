package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.module.findOptionalComponent
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.PreferBoundsComponent
import ru.nobirds.rx.ui.UiModule
import ru.nobirds.rx.ui.VerticalAlign
import ru.nobirds.rx.ui.lazyProperty

@DependentComponents(HorizontalLayoutComponent::class)
class HBox(id: String) : UiModule(id) {

    val horizontalLayout: HorizontalLayoutComponent by dependency()

    var verticalAlign: VerticalAlign by lazyProperty { horizontalLayout.verticalAlignProperty }

}

fun ModuleDefinition<*>.hbox(id: String, initializer: ModuleDefinition<HBox>.() -> Unit): ModuleDefinition<HBox> = withModule(id, initializer)

class HorizontalLayoutComponent(parent: Module) : LayoutComponent(parent) {

    val verticalAlignProperty: Property<VerticalAlign> = property(VerticalAlign.none)
    var verticalAlign: VerticalAlign by verticalAlignProperty

    override fun updatePreferSize() {
        var width = spacing.padding.left + spacing.padding.right + spacing.margin
        var height = 0f

        for (it in childrenWithBounds) {
            val bounds = it.findComponent<BoundsComponent>()
            val prefBounds = it.findOptionalComponent<PreferBoundsComponent>()

            width += prefBounds?.prefWidth ?: bounds.width // sum of heights
            height = Math.max(height, prefBounds?.prefHeight ?: bounds.height) // max height
        }

        height += spacing.padding.top + spacing.padding.bottom

        preferBounds.pref(width, height)
    }

    override fun updateLayout() {
        val innerHeight = bounds.height - spacing.padding.top - spacing.padding.bottom

        var x = bounds.width - spacing.padding.left + spacing.margin

        childrenWithBounds.forEach {
            val childBounds = it.findComponent<BoundsComponent>()
            val childPreferBounds = it.findOptionalComponent<PreferBoundsComponent>()

            var width = childBounds.width
            var height = childBounds.height

            if (childPreferBounds != null) {
                width = childPreferBounds.prefWidth

                height = Math.max(Math.min(childPreferBounds.prefHeight, innerHeight), childPreferBounds.minHeight)

                if (childPreferBounds.maxHeight > 0f && height > childPreferBounds.maxHeight)
                    height = childPreferBounds.maxHeight
            }

            val y = spacing.padding.top + when (verticalAlign) {
                VerticalAlign.bottom -> innerHeight - height
                VerticalAlign.center -> (innerHeight - height) / 2f
                else -> 0f
            }

            x -= width + spacing.margin

            childBounds.bounds(x, y, width, height)
        }

    }

}