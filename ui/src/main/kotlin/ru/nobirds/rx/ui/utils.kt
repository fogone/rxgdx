package ru.nobirds.rx.ui

import ru.nobirds.rx.Layer
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.ui.viewport.ScreenWorldScaleComponent
import ru.nobirds.rx.ui.viewport.ViewportComponent
import ru.nobirds.rx.ui.viewport.ViewportScaleComponent
import ru.nobirds.rx.ui.viewport.WorldComponent
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun ModuleDefinition<out Layer>.withScreenSize(): ModuleDefinition<out Layer> {
    withComponents(
            ViewportComponent::class,
            WorldComponent::class,
            ViewportScaleComponent::class,
            ScreenWorldScaleComponent::class
    )

//    withComponent<ViewportScaleComponent> {
//        scale = Strategies.fillX
//    }

    return this
}

fun <R, T> R.lazyProperty(accessor:()->Property<T>):ReadWriteProperty<R, T> = object: ReadWriteProperty<R, T> {

    private val property by lazy(accessor)

    override fun setValue(thisRef: R, property: KProperty<*>, value: T) {
        this.property.value = value
    }

    override fun getValue(thisRef: R, property: KProperty<*>): T {
        return this.property.value
    }

}