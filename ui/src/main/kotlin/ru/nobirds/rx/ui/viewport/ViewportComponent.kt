package ru.nobirds.rx.ui.viewport

import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.SceneDirector
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.gl.utils.Gls
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.render.OnRender
import ru.nobirds.rx.render.ProgramaticRenderConveyorItem
import ru.nobirds.rx.render.RenderEvent
import ru.nobirds.rx.utils.ImmutableBounds
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.MutableBounds
import ru.nobirds.rx.utils.MutableSize

class ViewportChangedEvent(val viewport: ImmutableBounds) : AbstractEvent()

class ViewportComponent(parent: Module) : AbstractComponent(parent), MutableBounds, Invalidatable {

    val xProperty: Property<Float> = property(0f)
    override var x: Float by xProperty

    val yProperty: Property<Float> = property(0f)
    override var y: Float by yProperty

    val widthProperty: Property<Float> = property(0f)
    override var width: Float by widthProperty

    val heightProperty: Property<Float> = property(0f)
    override var height: Float by heightProperty

    private val viewportInvalidated = invalidated(false, xProperty, yProperty, widthProperty, heightProperty)

    override val invalidated: ReactiveStream<Signal> = changed(viewportInvalidated)

    private val renderItem = ProgramaticRenderConveyorItem {
        Gls.viewport(x, y, width, height)
    }

    @OnUpdate
    fun update() {
        viewportInvalidated.with {
            updateViewport()
        }
    }

    private fun updateViewport() {
        parent.fire(ViewportChangedEvent(this), EventTraverseStrategies.full)
    }

    @OnRender
    fun render(event: RenderEvent) {
        event.renderConveyor.render(renderItem)
    }

}


interface ViewportScaleStrategy {

    fun scale(world: ImmutableSize, screen: ImmutableSize, viewport: MutableBounds)

}

fun boundsScaleStrategy(scale: (ImmutableSize, ImmutableSize) -> ImmutableBounds): ViewportScaleStrategy = object : ViewportScaleStrategy {
    override fun scale(world: ImmutableSize, screen: ImmutableSize, viewport: MutableBounds) {
        viewport.bounds(scale(world, screen))
    }
}

object Strategies {

    val none = boundsScaleStrategy { _, screen -> ImmutableBounds.readonly(screen) }
    val fillX = boundsScaleStrategy { world, screen ->
        val k = screen.height / world.width
        ImmutableBounds.readonly(world.width * k, world.height * k)
    }

    // todo!!
}

@DependentComponents(ViewportComponent::class)
class ViewportScaleComponent(parent: Module, val director: SceneDirector) : AbstractComponent(parent) {

    private val viewport: ViewportComponent by dependency()

    private var world: MutableSize = MutableSize.zero()

    var scale: ViewportScaleStrategy = Strategies.none

    private val invalidatedWorld = invalidated(false)

    @OnEvent(WorldSizeChangedEvent::class)
    fun updateWorld(event:WorldSizeChangedEvent) {
        world.size(event)
    }

    @OnEvent(ResizeEvent::class, WorldSizeChangedEvent::class)
    fun invalidateWorld() {
        invalidatedWorld.invalidate()
    }

    @OnUpdate
    fun update() {
        invalidatedWorld.with {
            updateBounds()
        }
    }

    private fun updateBounds() {
        scale.scale(world, director.screen, viewport)
    }

}

