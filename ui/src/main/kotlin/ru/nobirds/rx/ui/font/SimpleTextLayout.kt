package ru.nobirds.rx.ui.font

import com.badlogic.gdx.graphics.Color
import ru.nobirds.rx.gl.font.Font
import ru.nobirds.rx.ui.HorizontalAlign
import ru.nobirds.rx.utils.ImmutableSize

private class SimpleTextLayoutLine(var xOffset: Float, var yOffset: Float) : TextLayoutLine {

    override val runes: MutableList<TextRunImpl> = arrayListOf()

    override var width: Float = 0f
    override var height: Float = 0f

    fun append(text: String, font: Font, color: Color) {
        require(!text.contains('\n'))

        val run = createRun(text, font, color)

        this.width += run.width
        this.height = Math.max(this.height, run.height)

        runes.add(run)
    }

    private fun createRun(text: String, font: Font, color: Color): TextRunImpl {
        var xOffset = 0f
        var maxGlyphHeight = 0f

        var last: Char? = null

        val glyphs = text.map { char ->
            val glyph = font.glyph(char)

            val kerning = last?.let { font.kerning(it, char).toFloat() } ?: 0f

            val charXOffset = kerning + glyph.char.xOffset.toFloat()
            val charYOffset = -(glyph.char.height.toFloat() + glyph.char.yOffset.toFloat())

            val fontGlyphWithOffset = FontGlyphWithOffsetImpl(glyph, xOffset + charXOffset, charYOffset)

            xOffset += glyph.char.width + kerning // + glyph.char.xAdvance
            maxGlyphHeight = Math.max(maxGlyphHeight, -charYOffset)
            last = char

            fontGlyphWithOffset
        }

        return TextRunImpl(this.xOffset, this.yOffset, color, glyphs, xOffset, maxGlyphHeight)
    }


}

class SimpleTextLayoutBuilder(val alignment: HorizontalAlign = HorizontalAlign.none) : TextLayoutBuilder {

    private var width = 0f
    private var height = 0f

    private var yOffset = 0f

    private val lines = arrayListOf<SimpleTextLayoutLine>()
    private var currentLine: SimpleTextLayoutLine? = null

    override fun append(text: String, font: Font, color: Color): SimpleTextLayoutBuilder = apply {
        if (text.contains("\n")) {
            val lines = text.split("\n")

            for (line in lines) {
                appendInCurrentLine(line, font, color)
                endLine()
            }
        } else {
            appendInCurrentLine(text, font, color)
        }
    }

    private fun appendInCurrentLine(text: String, font: Font, color: Color) {
        getOrCreateCurrentLine().append(text, font, color)
    }

    private fun getOrCreateCurrentLine(): SimpleTextLayoutLine = currentLine ?: SimpleTextLayoutLine(0f, yOffset).apply {
        lines.add(this)
        currentLine = this
    }

    private fun endLine() {
        currentLine?.let {
            yOffset -= it.height
            width = Math.max(width, it.width)
            height += it.height
        }
        currentLine = null
    }

    override fun build(): TextLayout {
        endLine()

        val size = ImmutableSize.readonly(width, height)

        Aligner.align(size, lines, alignment)

        val runes = lines.flatMap { it.runes }.map {
            it.apply { y += size.height }
        }

        return TextLayoutImpl(size, runes)
    }

}

