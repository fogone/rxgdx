package ru.nobirds.rx.ui

import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.module.hasComponent
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal

@DependentComponents(BoundsComponent::class)
class FillParentComponent(parent:Module) : AbstractComponent(parent), Invalidatable {

    private val bounds:BoundsComponent by dependency()

    val fillParentWidthProperty: Property<Boolean> = property(false)
    var fillParentWidth: Boolean by fillParentWidthProperty

    val fillParentHeightProperty: Property<Boolean> = property(false)
    var fillParentHeight: Boolean by fillParentHeightProperty

    override val invalidated: ReactiveStream<Signal> = changed(fillParentWidthProperty, fillParentHeightProperty)

    private var invalidatedFillParent: InvalidatedProperty = invalidated()

    private fun invalidateFillParent() {
        if(fillParentWidth || fillParentHeight)
            invalidatedFillParent.invalidate()
    }

    @OnSetup
    fun setup() {
        invalidatedFillParent.bindOneWaySignal(invalidated)

        onEvent<ParentSizeChangedEvent> {
            invalidateFillParent()
        }

        onUpdate {
            invalidatedFillParent.with {
                updateSize()
            }
        }
    }

    protected fun updateSize() {
        val parent = parent.parent

        if ((fillParentWidth || fillParentHeight)
                && parent != null && parent.hasComponent<BoundsComponent>()) {

            val parentBounds = parent.findComponent<BoundsComponent>()

            if(fillParentWidth) {
                bounds.width = parentBounds.width
            }

            if (fillParentHeight) {
                bounds.height = parentBounds.height
            }
        }
    }

    fun fillParent(width:Boolean = true, height: Boolean = true) {
        fillParentWidth = width
        fillParentHeight = height
    }


}