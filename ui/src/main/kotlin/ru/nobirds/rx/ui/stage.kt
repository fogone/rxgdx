package ru.nobirds.rx.ui

import ru.nobirds.rx.Layer
import ru.nobirds.rx.LoadEvent
import ru.nobirds.rx.SceneDirector
import ru.nobirds.rx.Stage
import ru.nobirds.rx.UpdateEvent
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.module.AbstractModule
import ru.nobirds.rx.module.DisposeEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.OnDispose
import ru.nobirds.rx.module.moduleDefinition
import ru.nobirds.rx.render.RenderConveyor

fun SceneDirector.buildScene(name:String, builder:ModuleDefinition<out Stage>.()->Unit) {
    addScene(name) {
        moduleDefinition<StageModule>(name, builder)
    }
}

fun ModuleDefinition<out Stage>.layer(name: String, builder:ModuleDefinition<LayerModule>.()->Unit):ModuleDefinition<out Layer> = withModule<LayerModule>(name, builder)

open class StageModule(id:String, val director: SceneDirector) : AbstractModule(id), Stage {

    override val layers: Sequence<Layer>
        get() = children.filterIsInstance<Layer>()

    override var parent: Module? = null
        set(value) {
            throw UnsupportedOperationException("Impossible set parent for root")
        }

    private val updateEvent = UpdateEvent(0f)
    private val loadEvent = LoadEvent()

    override val isAttachedToScene: Boolean
        get() = true

    override val loaded: Boolean
        get() = load()

    @OnDispose
    fun dispose() {
        fire(DisposeEvent, EventTraverseStrategies.dependentAndChildren)
    }

    override fun load(): Boolean {
        fire(loadEvent.renew(), EventTraverseStrategies.dependentAndChildren)
        return loadEvent.complete
    }

    override fun update(delta: Float) {
        fire(updateEvent.delta(delta), EventTraverseStrategies.dependentAndChildren)
    }

    override fun render(delta: Float, renderer: RenderConveyor) {
        for (layer in layers) {
            layer.render(delta, renderer)
            if(director.debug)
                layer.renderDebug(delta, renderer)
        }
    }

}

