package ru.nobirds.rx.ui.alignment

import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.ComponentDefinition
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.reactive.subscribe
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.PivotComponent

@DependentComponents(BoundsComponent::class, PivotComponent::class)
class PivotAtCenterOfBoundsComponent(parent: Module) : AbstractComponent(parent) {

    private val bounds: BoundsComponent by dependency()
    private val pivot: PivotComponent by dependency()

    @OnSetup
    fun setup() {
        bounds.invalidated.subscribe {
            pivot.position(bounds.width/2f, bounds.height/2f)
        }
    }
}

fun ModuleDefinition<*>.withPivotAtCenter(): ComponentDefinition<PivotAtCenterOfBoundsComponent> = withComponent()
