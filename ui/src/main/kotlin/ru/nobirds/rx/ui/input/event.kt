package ru.nobirds.rx.ui.input

import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.Event
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.utils.MutableVec2f
import ru.nobirds.rx.utils.PoolContext

inline fun <reified E: Event> PoolContext.fire(target: Eventable?, crossinline initializer:E.()->Unit):Boolean {
    if(target == null)
        return false

    val event = obtain(initializer)

    // event.handled = false
    // event.inBounds = false

    target.fire(event, EventTraverseStrategies.full)

    return !event.live
}

abstract class AbstractScreenEvent() : AbstractEvent() {
    abstract val x: Float
    abstract val y: Float

    abstract var worldX: Float
    abstract var worldY: Float

    var inBounds:Boolean = true

    override fun reset() {
        super.reset()
        inBounds = true
        worldX = 0f
        worldY = 0f
    }
}

class TouchUpEvent(override var x: Float = 0f, override var y: Float = 0f, var pointer: Int = 0, var button: Int = 0, override var worldX:Float = x, override  var worldY:Float = y) : AbstractScreenEvent()
class TouchDownEvent(override var x: Float = 0f, override var y: Float = 0f, var pointer: Int = 0, var button: Int = 0, override var worldX:Float = x, override  var worldY:Float = y) : AbstractScreenEvent()

class TouchDraggedEvent(override var x: Float = 0f, override var y: Float = 0f, var pointer: Int = 0, override var worldX:Float = x, override  var worldY:Float = y) : AbstractScreenEvent()

class MouseMovedEvent(override var x: Float = 0f, override var y: Float = 0f, override var worldX:Float = x, override  var worldY:Float = y) : AbstractScreenEvent()

class KeyTypedEvent(var character: Char = 0.toChar()) : AbstractEvent()
class KeyUpEvent(var keycode: Int = 0) : AbstractEvent()

class KeyDownEvent(var keycode: Int = 0) : AbstractEvent()

class ScrolledEvent(var amount: Int = 0) : AbstractEvent()

class ZoomGestureEvent(var initialDistance: Float = 0f, var distance: Float = 0f) : AbstractEvent()
class TapGestureEvent(override var x: Float = 0f, override var y: Float = 0f, var count: Int = 0, var button: Int = 0, override var worldX:Float = x, override  var worldY:Float = y) : AbstractScreenEvent()
class TouchDownGestureEvent(override var x: Float = 0f, override var y: Float = 0f, var pointer: Int = 0, var button: Int = 0, override var worldX:Float = x, override  var worldY:Float = y) : AbstractScreenEvent()
class FlingGestureEvent(var velocityX: Float = 0f, var velocityY: Float = 0f, var button: Int = 0) : AbstractEvent()
class PanGestureEvent(override var x: Float = 0f, override var y: Float = 0f, var deltaX: Float = 0f, var deltaY: Float = 0f, override var worldX:Float = x, override  var worldY:Float = y) : AbstractScreenEvent()
class PanStopGestureEvent(override var x: Float = 0f, override var y: Float = 0f, var pointer: Int = 0, var button: Int = 0, override var worldX:Float = x, override  var worldY:Float = y) : AbstractScreenEvent()
class LongPressGestureEvent(override var x: Float = 0f, override var y: Float = 0f, override var worldX:Float = x, override  var worldY:Float = y) : AbstractScreenEvent()
class PinchGestureEvent(
        val initialPointer1: MutableVec2f = MutableVec2f.zero(),
        val initialPointer2: MutableVec2f = MutableVec2f.zero(),
        val pointer1: MutableVec2f = MutableVec2f.zero(),
        val pointer2: MutableVec2f = MutableVec2f.zero()) : AbstractEvent()

class PinchStopEvent() : AbstractEvent()