package ru.nobirds.rx.ui

import com.badlogic.gdx.ApplicationListener
import ru.nobirds.rx.SceneDirector
import ru.nobirds.rx.SceneDirectorInitializer
import ru.nobirds.rx.asset.AssetFactory
import ru.nobirds.rx.asset.AssetProvider
import ru.nobirds.rx.asset.DefaultAssetFactories
import ru.nobirds.rx.asset.assetFactory
import ru.nobirds.rx.di.annotations.Configuration
import ru.nobirds.rx.di.annotations.Definition
import ru.nobirds.rx.director.GenericSceneDirectorConfiguration
import ru.nobirds.rx.director.director
import ru.nobirds.rx.sceneDirectorInitializer
import ru.nobirds.rx.ui.ninepatch.ninePatchAssetFactory
import ru.nobirds.rx.ui.ninepatch.ninePatchFactoryAssetFactory

object DefaultUiAssetFactories {

    fun createProviderWithDefaults(): AssetProvider = AssetProvider().apply {
        register(this)
    }

    fun register(assetProvider: AssetProvider) {
        DefaultAssetFactories.register(assetProvider)
        defaultFactories().forEach { assetProvider.register(it) }
    }

    private fun defaultFactories(): Sequence<AssetFactory<*>> = sequenceOf(
            assetFactory(::ninePatchFactoryAssetFactory),
            assetFactory(::ninePatchAssetFactory)
    )

}

fun uiDirector(initializer: SceneDirector.()->Unit): ApplicationListener {
    return director(UiGenericSceneDirectorConfiguration(sceneDirectorInitializer(initializer)))
}

@Configuration
class UiGenericSceneDirectorConfiguration(val initializer: SceneDirectorInitializer = sceneDirectorInitializer {}) {

    @Configuration
    fun genericSceneDirectorConfiguration(): GenericSceneDirectorConfiguration = GenericSceneDirectorConfiguration(initializer)

    @Definition(override = true)
    fun assetsProvider(): AssetProvider = DefaultUiAssetFactories.createProviderWithDefaults()

}