package ru.nobirds.rx.ui

import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.observable.ObservableSize
import ru.nobirds.rx.utils.round

open class PreferBoundsComponent(parent: Module) : AbstractComponent(parent) {

    val roundProperty: Property<Boolean> = property(false)
    var round:Boolean by roundProperty

    val min: ObservableSize = ObservableSize()
    val pref: ObservableSize = ObservableSize()
    val max: ObservableSize = ObservableSize()

    var minWidth:Float by min.widthProperty
        private set
    var minHeight:Float by min.heightProperty
        private set

    var prefWidth:Float by pref.widthProperty
        private set
    var prefHeight:Float by pref.heightProperty
        private set

    var maxWidth:Float by max.widthProperty
        private set
    var maxHeight:Float by max.heightProperty
        private set

    val changed: ReactiveStream<Signal> = changed(roundProperty, min, pref, max)

    @OnUpdate
    fun update() {
        if(round) round()
    }

    private fun computePref(value: Float, min:Float, max:Float): Float {
        val realMin = if(min == 0f) value else min
        val readMax = if(max == 0f) value else max

        return Math.min(Math.max(value, realMin), Math.max(realMin, readMax))
    }

    fun prefWidth(value:Float) {
        prefWidth = computePref(value, minWidth, maxWidth)
    }

    fun prefHeight(value:Float) {
        prefHeight = computePref(value, minHeight, maxHeight)
    }

    private fun computeMin(value: Float, max: Float): Float {
        return Math.min(value, max)
    }

    fun minWidth(value:Float) {
        minWidth = computeMin(value, maxWidth)
        prefWidth(prefWidth)
    }

    fun minHeight(value:Float) {
        this.minHeight = computeMin(value, maxHeight)
        prefHeight(prefHeight)
    }

    fun maxWidth(value:Float) {
        this.maxWidth = computeMax(value, minWidth)
        prefWidth(prefWidth)
    }

    fun maxHeight(value:Float) {
        this.maxHeight = computeMax(value, minHeight)
        prefWidth(prefWidth)
    }

    private fun computeMax(value: Float, min:Float):Float {
        return Math.max(value, min)
    }

    fun pref(width:Float, height:Float) {
        prefWidth(width)
        prefHeight(height)
    }

    fun min(width:Float, height:Float) {
        minWidth(width)
        minHeight(height)
    }

    fun max(width:Float, height:Float) {
        maxWidth(width)
        maxHeight(height)
    }

    fun preferBounds(preferSize:PreferBoundsComponent) {
        pref(preferSize.prefWidth, preferSize.prefHeight)
        min(preferSize.minWidth, preferSize.minHeight)
        max(preferSize.maxWidth, preferSize.maxHeight)
    }

    fun size(size: ImmutableSize) {
        pref(size.width, size.height)
    }

    fun round() {
        minWidth = minWidth.round()
        minHeight = minWidth.round()

        prefWidth = prefWidth.round()
        prefHeight = prefHeight.round()

        maxWidth = maxWidth.round()
        maxHeight = maxHeight.round()
    }

    override fun toString(): String = "${super.toString()} [$minWidth x $minHeight, $prefWidth x $prefHeight, $maxWidth x $maxHeight]"
}