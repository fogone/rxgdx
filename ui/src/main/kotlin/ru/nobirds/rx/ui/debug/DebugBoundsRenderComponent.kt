package ru.nobirds.rx.ui.debug

import com.badlogic.gdx.graphics.Color
import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.BitmapFontAssetParameters
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.gl.font.Font
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.render.DebugRenderEvent
import ru.nobirds.rx.render.renderRect
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.TransformComponent
import ru.nobirds.rx.ui.font.EmptyTextLayout
import ru.nobirds.rx.ui.font.SimpleTextLayoutBuilder
import ru.nobirds.rx.ui.font.TextLayout
import ru.nobirds.rx.ui.font.render

@DependentComponents(BoundsComponent::class)
class DebugBoundsRenderComponent(parent: Module, val assets:AssetCoordinator) : AbstractComponent(parent), DebugComponent {

    private val fontParameters = BitmapFontAssetParameters("com/badlogic/gdx/utils/arial-15.fnt")

    private var font: Font? = null

    private var layout: TextLayout = EmptyTextLayout

    val colorProperty: Property<Color> = property(Color.GREEN)
    var color: Color by colorProperty

    val enabledProperty: Property<Boolean> = property(true)
    override var enabled: Boolean by enabledProperty

    val showBoundsProperty: Property<Boolean> = property(false)
    var showBounds: Boolean by showBoundsProperty

    private val bounds: BoundsComponent by dependency()
    private val transform: TransformComponent by dependency()

    private val layoutInvalidated by lazy { invalidated(true, bounds, colorProperty, showBoundsProperty) }

    private var subscription: Subscription? = null
        set(value) {
            field?.unsubscribe()
            field = value
        }

    @OnSetup
    fun setup() {
        requestFont()
    }

    @OnUpdate
    fun update() {
        font?.let { f ->
            layoutInvalidated.with {
                this.layout = SimpleTextLayoutBuilder()
                        .append(computeDebugText(parent.id), f, color)
                        .build()
            }
        }
    }

    private fun computeDebugText(parentId: String) = parentId +
            if(showBounds) "\n[${bounds.x},${bounds.y},${bounds.width},${bounds.height}]" else ""

    private fun requestFont() {
        subscription = assets.require(fontParameters) {
            this.font = it
            layoutInvalidated.invalidate()
        }
    }

    @OnEvent(DebugRenderEvent::class)
    fun render(event: DebugRenderEvent) {
        if(enabled) {
            val renderConveyor = event.renderConveyor

            renderConveyor.renderRect(transform, bounds, color)

            renderConveyor.render(layout, transform)
        }
    }

}