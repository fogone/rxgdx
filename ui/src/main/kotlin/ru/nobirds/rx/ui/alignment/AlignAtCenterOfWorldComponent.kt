package ru.nobirds.rx.ui.alignment

import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.ui.PositionComponent
import ru.nobirds.rx.ui.viewport.WorldSizeChangedEvent

@DependentComponents(PositionComponent::class)
class AlignAtCenterOfWorldComponent(parent: Module) : AbstractComponent(parent) {

    private val position: PositionComponent by dependency()

    @OnEvent(WorldSizeChangedEvent::class)
    fun updatePosition(event:WorldSizeChangedEvent) {
        position.position(event.width / 2f, event.height / 2f)
    }

}

@DependentComponents(PositionComponent::class)
class AlignAtCenterOfScreenComponent(parent:Module) : AbstractComponent(parent) {

    private val position: PositionComponent by dependency()

    @OnEvent(ResizeEvent::class)
    fun updatePosition(event:ResizeEvent) {
        position.position(event.width / 2f, event.height / 2f)
    }

}

