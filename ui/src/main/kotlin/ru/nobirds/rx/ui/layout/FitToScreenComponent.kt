package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.ui.BoundsComponent

@DependentComponents(BoundsComponent::class)
class FitToScreenComponent(parent: Module) : AbstractComponent(parent) {

    private val bounds: BoundsComponent by dependency()

    @OnEvent(ResizeEvent::class)
    fun updateBounds(event: ResizeEvent) {
        bounds.bounds(0f, 0f, event.width, event.height)
    }
}