package ru.nobirds.rx.ui

import ru.nobirds.rx.Layer
import ru.nobirds.rx.NoneProjection
import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.Projection
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.module.AbstractModule
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.module.findById
import ru.nobirds.rx.module.findComponentsByInterface
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.ObservableValue
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWay
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.render.DebugRenderEvent
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.render.RenderEvent
import ru.nobirds.rx.render.render
import ru.nobirds.rx.ui.input.AbstractScreenEvent
import ru.nobirds.rx.ui.input.InputTraverseStrategy
import ru.nobirds.rx.utils.obtainVec2
import ru.nobirds.rx.utils.poolable


@DependentComponents(ProjectionFinderComponent::class)
open class LayerModule(id: String) : AbstractModule(id), Layer {

    val projectionFinder: ProjectionFinderComponent by dependency()

    val projectionProperty: Property<Projection> = property(NoneProjection)
    override var projection: Projection by projectionProperty

    val debugProperty: Property<Boolean> = property(true)
    override var debug: Boolean by debugProperty

    private val renderEvent = RenderEvent()
    private val renderDebugEvent = DebugRenderEvent()

    private val inputTraverseStrategy = InputTraverseStrategy()

    private var inEventProcessing = false

    @OnEvent(AbstractScreenEvent::class)
    fun projectScreenEvents(event: AbstractScreenEvent) {
        withEventProcessing {
            if (event.live) {
                event.inBounds = true
                projectAndBubble(event)
            }

            if (event.live) {
                event.inBounds = false
                fire(event, EventTraverseStrategies.dependentAndChildren)
            }
        }
    }

    private inline fun withEventProcessing(block: () -> Unit) {
        if (!inEventProcessing) {
            try {
                inEventProcessing = true
                block()
            } finally {
                inEventProcessing = false
            }
        }
    }

    private fun projectAndBubble(e: AbstractScreenEvent) = poolable {
        projection.unproject(obtainVec2(e.x, e.y)).apply {
            e.worldX = x
            e.worldY = y
        }

        fire(e, inputTraverseStrategy)
    }

    override fun render(delta: Float, renderer: RenderConveyor) {
        renderer.render(projection) {
            fire(renderEvent.withDeltaAndRenderer(delta, renderer), EventTraverseStrategies.full)
        }
    }

    override fun renderDebug(delta: Float, renderer: RenderConveyor) {
        if (debug) {
            renderer.render(projection) {
                fire(renderDebugEvent.withDeltaAndRenderer(delta, renderer), EventTraverseStrategies.full)
            }
        }
    }
}

class ProjectionFinderComponent(parent: Module) : AbstractComponent(parent) {

    val projectionModuleIdProperty: Property<String?> = property(null)
    var projectionModuleId: String? by projectionModuleIdProperty

    val invalidatedProjectionId: InvalidatedProperty = invalidated(false, projectionModuleIdProperty)

    val projectionProperty: Property<Projection> = property(NoneProjection)
    var projection: Projection by projectionProperty

    @OnUpdate
    fun updateProjection() {
        val projectionModuleId = projectionModuleId

        invalidatedProjectionId.with {
            projection = if (projectionModuleId != null) {
                val module = parent.findById(projectionModuleId, Module::class)

                requireNotNull(module) { "Projection module not found" }

                val projection = module?.findComponentsByInterface<Projection>()?.firstOrNull()

                requireNotNull(projection) { "Projection module not found" }

                projection!!
            } else {
                NoneProjection
            }
        }
    }

    fun setupProjection(id: String): ObservableValue<Projection> {
        this.projectionModuleId = id
        return projectionProperty
    }

}

fun ModuleDefinition<out LayerModule>.projection(id: String) {
    withValues {
        projectionProperty.bindOneWay(projectionFinder.setupProjection(id))
    }
}

fun ModuleDefinition<out LayerModule>.projection(module: Module) {
    withValues {
        projection = module.components.filterIsInstance<Projection>().first()
    }
}
