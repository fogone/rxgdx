package ru.nobirds.rx.ui.debug

import com.badlogic.gdx.Gdx
import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.TwoWayConverters
import ru.nobirds.rx.property.bindTwoWay
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.LabelComponent

@DependentComponents(LabelComponent::class)
class FpsComponent(parent: Module) : AbstractComponent(parent) {

    private val fpsProperty: Property<Int> = property(0)
    private var fps:Int by fpsProperty

    private val label: LabelComponent by dependency()

    @OnSetup
    fun setup() {
        label.textProperty.bindTwoWay(fpsProperty, TwoWayConverters.intToString)
    }

    @OnUpdate
    fun update() {
        this.fps = Gdx.graphics.framesPerSecond
    }
}