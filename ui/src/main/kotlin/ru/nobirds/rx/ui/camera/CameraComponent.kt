package ru.nobirds.rx.ui.camera

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3
import ru.nobirds.rx.Projection
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.render.OnRender
import ru.nobirds.rx.ui.TransformComponent
import ru.nobirds.rx.ui.viewport.ViewportChangedEvent
import ru.nobirds.rx.ui.viewport.WorldSizeChangedEvent
import ru.nobirds.rx.utils.MutableBounds
import ru.nobirds.rx.utils.MutableVec2f
import ru.nobirds.rx.utils.obtainVec3
import ru.nobirds.rx.utils.poolable
import ru.nobirds.rx.utils.set

@DependentComponents(TransformComponent::class)
class CameraComponent(parent: Module) : AbstractComponent(parent), Projection {

    private val transform: TransformComponent by dependency()

    private var viewport: MutableBounds? = null

    private val camera = OrthographicCamera().apply {
        setToOrtho(false)
    }

    private val invalidatedMatrix by lazy { invalidated(true, transform) }
    // private val invalidatedFrustum = invalidated()

    override val invalidated: ReactiveStream<Signal> by lazy { changed(invalidatedMatrix) }

    override val matrix: Matrix4
        get() = camera.combined

    @OnSetup
    fun setup() {
        transform.ignoreTransform()
    }

    @OnEvent(WorldSizeChangedEvent::class)
    fun updateWorld(event: WorldSizeChangedEvent) {
        updateWorld(event.width, event.height)
    }

    @OnEvent(ViewportChangedEvent::class)
    fun updateViewport(event: ViewportChangedEvent) {
        this.viewport?.bounds(event.viewport)
    }

    @OnRender
    fun updateCamera() {
        invalidatedMatrix.with {
            camera.normalizeUp()
            camera.rotate(transform.rotation)
            camera.position.set(transform.x, transform.y, 1f)

            camera.update(true)
        }
    }

    private fun updateWorld(width: Float, height: Float) {
        invalidatedMatrix.invalidate()
        // invalidatedFrustum.invalidate()
        camera.setToOrtho(false, width, height)
    }

    override fun project(vector: MutableVec2f): MutableVec2f = proj(vector) { vector3 ->
        val v = viewport
        if (v != null) project(vector3, v.x, v.y, v.width, v.height)
        else project(vector3)
    }

    override fun unproject(vector: MutableVec2f): MutableVec2f = proj(vector) { vector3 ->
        val v = viewport
        if (v != null) unproject(vector3, v.x, v.y, v.width, v.height)
        else unproject(vector3)
    }

    private inline fun proj(vector: MutableVec2f, proj: Camera.(Vector3) -> Unit): MutableVec2f = poolable {
        val vector3 = obtainVec3(vector.x, vector.y, 1f)
        camera.proj(vector3)
        vector.set(vector3.x, vector3.y)
    }
}