package ru.nobirds.rx.ui.camera

import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.PositionComponent
import ru.nobirds.rx.ui.input.PanGestureEvent

@DependentComponents(PositionComponent::class)
class FreeTransformComponent(parent:Module) : AbstractComponent(parent) {

    private val position:PositionComponent by dependency()

    val invertProperty:Property<Boolean> = property(false)
    var invert:Boolean by invertProperty

    @OnEvent(PanGestureEvent::class)
    fun update(event: PanGestureEvent) {
        val factor = if (invert) -1 else 1
        position.translate(factor * -event.deltaX, factor * event.deltaY)
    }
}


fun ModuleDefinition<*>.freeTransform() {
    withComponent<FreeTransformComponent>()
}
