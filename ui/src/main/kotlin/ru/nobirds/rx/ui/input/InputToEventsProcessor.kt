package ru.nobirds.rx.ui.input

import com.badlogic.gdx.InputProcessor
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.utils.poolable

class InputToEventsProcessor(val target:()-> Eventable?) : InputProcessor {

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = poolable {
        fire<TouchUpEvent>(target()) {
            this.x = screenX.toFloat()
            this.y = screenY.toFloat()
            this.worldX = x
            this.worldY = y
            this.pointer = pointer
            this.button = button
        }
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = poolable {
        fire<TouchDownEvent>(target()) {
            this.x = screenX.toFloat()
            this.y = screenY.toFloat()
            this.worldX = x
            this.worldY = y
            this.pointer = pointer
            this.button = button
        }
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean = poolable {
        fire<MouseMovedEvent>(target()) {
            this.x = screenX.toFloat()
            this.y = screenY.toFloat()
            this.worldX = x
            this.worldY = y
        }
    }

    override fun scrolled(amount: Int): Boolean = poolable {
        fire<ScrolledEvent>(target()) {
            this.amount = amount
        }
    }

    override fun keyTyped(character: Char): Boolean = poolable {
        fire<KeyTypedEvent>(target()) {
            this.character = character
        }
    }

    override fun keyUp(keycode: Int): Boolean = poolable {
        fire<KeyUpEvent>(target()) {
            this.keycode = keycode
        }
    }

    override fun keyDown(keycode: Int): Boolean = poolable {
        fire<KeyDownEvent>(target()) {
            this.keycode = keycode
        }
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean = poolable {
        fire<TouchDraggedEvent>(target()) {
            this.x = screenX.toFloat()
            this.y = screenY.toFloat()
            this.worldX = x
            this.worldY = y
            this.pointer = pointer
        }
    }
}