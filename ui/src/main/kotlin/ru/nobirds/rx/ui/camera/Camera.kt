package ru.nobirds.rx.ui.camera

import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.module.AbstractModule
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.ui.alignment.AlignAtCenterOfScreenComponent
import ru.nobirds.rx.ui.alignment.AlignAtCenterOfWorldComponent

@DependentComponents(CameraComponent::class)
class Camera(id:String) : AbstractModule(id)

fun ModuleDefinition<*>.camera(id:String, initializer: ModuleDefinition<Camera>.()->Unit): ModuleDefinition<Camera> {
    return withModule(id) {
        withComponent<CameraComponent>()
        initializer()
    }
}

fun ModuleDefinition<Camera>.alignAtCenterOfWorld() {
    withComponent<AlignAtCenterOfWorldComponent>()
}

fun ModuleDefinition<Camera>.alignAtCenterOfScreen() {
    withComponent<AlignAtCenterOfScreenComponent>()
}
