package ru.nobirds.rx.ui.ninepatch

import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.ui.PositionComponent
import ru.nobirds.rx.ui.SpacingComponent
import ru.nobirds.rx.ui.layout.LayoutComponent
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.MutableSize
import ru.nobirds.rx.utils.concat
import ru.nobirds.rx.utils.set

@DependentComponents(NinePatchComponent::class, SpacingComponent::class)
class NinePatchLayout(parent: Module) : LayoutComponent(parent) {

    private val ninePatch: NinePatchComponent by dependency()

    override fun updatePreferSize() {
        val childSize = collectChildSize()

        ninePatch.contentSize = childSize

        val scaledNinePath = ninePatch.scaledNinePath

        if (scaledNinePath != null) {
            val fullSize = scaledNinePath.size

            updateSpacing(scaledNinePath)

            preferBounds.pref(fullSize.width, fullSize.height)
            preferBounds.max(fullSize.width, fullSize.height)
        }
    }

    private fun updateSpacing(patch: ScaledNinePatch) {
        val size = patch.size
        val content = patch.content

        val left = content.x + 1
        val right = size.width - left - content.width
        val top = content.y + 1
        val bottom = size.height - top - content.height

        spacing.padding.set(top, right, bottom, left)
    }

    private fun collectChildSize(): ImmutableSize = childrenBounds.fold(MutableSize.zero(), MutableSize::concat)

    override fun updateLayout() {
        val padding = spacing.padding
        childrenWithBounds.forEach {
            it.findComponent<PositionComponent>().position(padding.left, padding.bottom)
        }
    }
}
