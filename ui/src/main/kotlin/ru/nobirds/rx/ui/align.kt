package ru.nobirds.rx.ui

enum class VerticalAlign {
    top, center, bottom, none
}

enum class HorizontalAlign {
    left, center, right, none
}

