package ru.nobirds.rx.ui.font

import com.badlogic.gdx.graphics.Color
import ru.nobirds.rx.gl.font.Font
import ru.nobirds.rx.gl.font.FontGlyph
import ru.nobirds.rx.utils.ImmutableSize

interface FontGlyphWithOffset {

    val glyph: FontGlyph

    val xOffset: Float
    val yOffset: Float
}

interface TextRun {

    val x: Float
    val y: Float

    val color: Color

    val glyphs: Iterable<FontGlyphWithOffset>

}

interface MutableTextRun : TextRun {

    override var x: Float
    override var y: Float

    override var color: Color


}

interface TextLayout {

    val size: ImmutableSize

    val runes: Iterable<TextRun>

}

object EmptyTextLayout : TextLayout {
    override val size: ImmutableSize = ImmutableSize.readonly(0f, 0f)
    override val runes: Iterable<TextRun> = emptyList()
}

interface TextLayoutLine {

    val width: Float
    val height: Float

    val runes: List<MutableTextRun>

}

interface TextLayoutBuilder {

    fun append(text: String, font: Font, color: Color): TextLayoutBuilder

    fun build(): TextLayout

}


