package ru.nobirds.rx.ui

import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.module.AbstractModule
import ru.nobirds.rx.module.dependency

@DependentComponents(TransformComponent::class, BoundsComponent::class)
open class UiModule(id: String) : AbstractModule(id) {

    val transform:TransformComponent by dependency()
    val bounds:BoundsComponent by dependency()

}

