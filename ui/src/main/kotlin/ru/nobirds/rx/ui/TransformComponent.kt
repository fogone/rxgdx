package ru.nobirds.rx.ui

import com.badlogic.gdx.math.Affine2
import com.badlogic.gdx.math.Matrix4
import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.ComponentDefinition
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.module.hasComponent
import ru.nobirds.rx.module.parents
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.reactive.plus
import ru.nobirds.rx.utils.MutableTransform

@DependentComponents(PositionComponent::class, PivotComponent::class)
class TransformComponent(parent:Module) : AbstractComponent(parent), Invalidatable, MutableTransform {

    val position: PositionComponent by dependency()
    val pivot: PivotComponent by dependency()

    override var x:Float
        get() = position.x
        set(value) {
            position.x = value
        }

    override var y:Float
        get() = position.y
        set(value) {
            position.y = value
        }

    override var pivotX: Float
        get() = pivot.x
        set(value) {
            pivot.x = value
        }

    override var pivotY: Float
        get() = pivot.y
        set(value) {
            pivot.y = value
        }

    val scaleXProperty: Property<Float> = property(1f)
    override var scaleX:Float by scaleXProperty

    val scaleYProperty: Property<Float> = property(1f)
    override var scaleY:Float by scaleYProperty

    val rotationProperty: Property<Float> = property(0f)
    override var rotation:Float by rotationProperty

    val ignoreLocalTransformProperty:Property<Boolean> = property(false)
    var ignoreLocalTransform:Boolean by ignoreLocalTransformProperty

    override val invalidated: ReactiveStream<Signal> by
            lazy { position.invalidated + pivot.invalidated +
                    changed(scaleXProperty, scaleYProperty,
                            rotationProperty, ignoreLocalTransformProperty) }

    private val invalidatedTransform: InvalidatedProperty = invalidated()

    private val transform: Affine2 = Affine2()
    private val transformMatrix: Matrix4 = Matrix4()

    override fun toMatrix(): Matrix4 = transformMatrix

    override fun toAffine(): Affine2 = transform

    fun ignoreTransform() {
        ignoreLocalTransform = true
    }

    @OnSetup
    fun setup() {
        invalidatedTransform.bindOneWaySignal(invalidated)

        onEvent<ParentSizeChangedEvent> {
            invalidatedTransform.invalidate()
        }
    }

    @OnUpdate
    fun updateIfNeeded() {
        invalidatedTransform.with {
            updateTransformationMatrix()
            applyParentTransformation()
            transformMatrix.set(transform)
        }
    }

    private fun updateTransformationMatrix() {
        if (ignoreLocalTransform) {
            transform.idt()
        } else {
            transform.setToTrnRotScl(x + pivot.x, y + pivot.y, this.rotation, this.scaleX, this.scaleY)
            transform.translate(-pivot.x, -pivot.y)
        }
    }

    private fun applyParentTransformation() {
        val parentTransformation = findParentTransform()?.transform

        if (parentTransformation != null)
            transform.preMul(parentTransformation)
    }

    private fun findParentTransform(): TransformComponent? = this.parent
            .parents()
            .firstOrNull { it.hasComponent<TransformComponent>() }
            ?.findComponent<TransformComponent>()

    override fun toString(): String = "transform [$x,$y] [$rotation] [$scaleX, $scaleY]"
}

fun ModuleDefinition<*>.setupTransform(block:TransformComponent.()->Unit):ComponentDefinition<TransformComponent> = withComponent(block)


/*
todo: remove, old code

override fun parentToLocal(vector: MutableVec2f): MutableVec2f {
        findParentTransform()?.parentToLocal(vector)

        if (ignoreLocalTransform) {
            return vector
        }

        val cos = MathUtils.cos(rotation * MathUtils.degreesToRadians)
        val sin = MathUtils.sin(rotation * MathUtils.degreesToRadians)

        val dx = vector.x - x - pivot.x
        val dy = vector.y - y - pivot.y

        vector.x = (dx * cos + dy * sin) / scaleX + pivot.x
        vector.y = (dx * -sin + dy * cos) / scaleY + pivot.y

        return vector
    }

    override fun localToParent(vector: MutableVec2f): MutableVec2f {
        if (ignoreLocalTransform) {
            findParentTransform()?.localToParent(vector)
            return vector
        }

        val cos = MathUtils.cosDeg(rotation)
        val sin = MathUtils.sinDeg(rotation)

        val dx = (vector.x - pivot.x) * scaleX
        val dy = (vector.y - pivot.y) * scaleY

        vector.x = (dx * cos + dy * sin) + pivot.x + x
        vector.y = (dx * -sin + dy * cos) + pivot.y + y

        findParentTransform()?.localToParent(vector)

        return vector
    }*/
