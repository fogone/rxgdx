package ru.nobirds.rx.ui.font

import com.badlogic.gdx.graphics.Color
import ru.nobirds.rx.gl.font.FontGlyph
import ru.nobirds.rx.utils.ImmutableSize

internal data class TextLayoutImpl(override val size: ImmutableSize, override val runes: Iterable<TextRun>) : TextLayout
internal data class TextRunImpl(override var x: Float,
                                override var y: Float,
                                override var color: Color,
                                override val glyphs: Iterable<FontGlyphWithOffset>,
                                val width: Float, val height: Float) : MutableTextRun

internal data class FontGlyphWithOffsetImpl(override val glyph: FontGlyph,
                                            override var xOffset: Float,
                                            override var yOffset: Float) : FontGlyphWithOffset