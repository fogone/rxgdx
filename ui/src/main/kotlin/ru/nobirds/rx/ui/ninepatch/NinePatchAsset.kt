package ru.nobirds.rx.ui.ninepatch

import ru.nobirds.rx.asset.AbstractAsset
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.AssetParameters
import ru.nobirds.rx.asset.BitmapAssetParameters
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.flatMap
import ru.nobirds.rx.reactive.map
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.observable.GdxRunner

data class NinePatchFactoryAssetParameters(val bitmap: BitmapAssetParameters) : AssetParameters<NinePatchFactory>


@Suppress("UNUSED_PARAMETER")
fun ninePatchFactoryAssetFactory(parameters: NinePatchFactoryAssetParameters, assetCoordinator: AssetCoordinator, resources: ResourceManager) =
        NinePatchFactoryAsset(parameters, assetCoordinator)

class NinePatchFactoryAsset(parameters: NinePatchFactoryAssetParameters, val assetCoordinator: AssetCoordinator) :
        AbstractAsset<NinePatchFactory, NinePatchFactoryAssetParameters>(parameters) {

    override fun load(reactiveStream: ReactiveStream<NinePatchFactoryAssetParameters>): ReactiveStream<NinePatchFactory> = reactiveStream
            .flatMap { assetCoordinator.require(it.bitmap) }
            .map(assetCoordinator.newThreadRunner, ::NinePatchFactory)

}

data class NinePatchAssetParameters(val factory: NinePatchFactoryAssetParameters, val bounds: NinePatchBounds? = null) : AssetParameters<NinePatch>

@Suppress("UNUSED_PARAMETER")
fun ninePatchAssetFactory(parameters: NinePatchAssetParameters, assetCoordinator: AssetCoordinator, resources: ResourceManager) =
        NinePatchAsset(parameters, assetCoordinator)

class NinePatchAsset(parameters: NinePatchAssetParameters, val assetCoordinator: AssetCoordinator) :
        AbstractAsset<NinePatch, NinePatchAssetParameters>(parameters) {

    override fun load(reactiveStream: ReactiveStream<NinePatchAssetParameters>): ReactiveStream<NinePatch> = reactiveStream
            .flatMap { assetCoordinator.require(it.factory) }
            .map(GdxRunner.runner) { if(parameters.bounds != null) it.createNinePatch(parameters.bounds) else it.createNinePatch() }

}

