package ru.nobirds.rx.ui

import com.badlogic.gdx.graphics.Color
import ru.nobirds.rx.LoadEvent
import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.AssetParameters
import ru.nobirds.rx.asset.BitmapFontAssetParameters
import ru.nobirds.rx.asset.FontAssetParameters
import ru.nobirds.rx.asset.FontGeneratorAssetParameters
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.gl.font.Font
import ru.nobirds.rx.gl.font.freetype.FreeTypeFontGenerationParameters
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.OnDispose
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.render.OnRender
import ru.nobirds.rx.render.RenderEvent
import ru.nobirds.rx.ui.font.EmptyTextLayout
import ru.nobirds.rx.ui.font.SimpleTextLayoutBuilder
import ru.nobirds.rx.ui.font.TextLayout
import ru.nobirds.rx.ui.font.render

@DependentComponents(LabelComponent::class, SpacingComponent::class)
class Label(id:String) : UiModule(id) {

    val label:LabelComponent by dependency()

    var text:String by lazyProperty { label.textProperty }
    var font:String? by lazyProperty { label.nameProperty }
    var size:Int by lazyProperty { label.sizeProperty }
    var color:Color by lazyProperty { label.colorProperty }
    var align:HorizontalAlign by lazyProperty { label.horizontalAlignProperty }

}

fun ModuleDefinition<*>.label(id:String, text:String = "", initializer: ModuleDefinition<Label>.()->Unit = {}):ModuleDefinition<Label> = withModule(id) {
    withValues {
        this.text = text
    }
    initializer()
}

@DependentComponents(TransformComponent::class, BoundsComponent::class)
class LabelComponent(parent:Module, val assets:AssetCoordinator) : AbstractComponent(parent) {

    private val transform:TransformComponent by dependency()
    private val bounds:BoundsComponent by dependency()

    val textProperty:Property<String> = property("")
    var text:String by textProperty

    val nameProperty:Property<String?> = property(null)
    var name:String? by nameProperty

    val sizeProperty:Property<Int> = property(12)
    var size:Int by sizeProperty

    val colorProperty:Property<Color> = property(Color.WHITE )
    var color:Color by colorProperty

    val horizontalAlignProperty:Property<HorizontalAlign> = property(HorizontalAlign.left)
    var horizontalAlign:HorizontalAlign by horizontalAlignProperty

    private var font: Font? = null
    private var fontParameters:AssetParameters<Font>? = null
    private var subscription: Subscription? = null
        set(value) {
            field?.unsubscribe()
            field = value
        }

    private fun updateLayout() {
        font?.let {
            layout = SimpleTextLayoutBuilder(horizontalAlign).append(text, it, color).build()
            bounds.size(layout.size)
        }
    }

    private var layout: TextLayout = EmptyTextLayout

    private val invalidatedFont: InvalidatedProperty = invalidated(true, nameProperty, sizeProperty, colorProperty)

    private val invalidatedText: InvalidatedProperty = invalidated(true, textProperty, horizontalAlignProperty)

    @OnEvent(LoadEvent::class)
    fun load(event: LoadEvent) {
        event.complete(font != null)
    }

    @OnDispose
    fun dispose() {
        subscription = null
        fontParameters?.let {
            assets.notRequired(it)
        }
    }

    @OnUpdate
    fun update() {
        invalidatedText.with {
            updateLayout()
        }
        invalidatedFont.with {
            changeFont()
            updateLayout()
        }
    }

    private fun changeFont() {
        name?.let {
            val assetParameters = if (isBitmapFont(it)) {
                createBitmapFontAssetParameters(it)
            } else {
                createFontAssetParameters(it)
            }

            this.fontParameters = assetParameters

            this.subscription = assets.require(assetParameters) {
                this.font = it
                invalidatedFont.invalidate()
            }
        }
    }

    private fun isBitmapFont(name: String): Boolean {
        return name.endsWith(".fnt")
    }

    private fun createBitmapFontAssetParameters(name: String): AssetParameters<Font> {
        return BitmapFontAssetParameters(name)
    }

    private fun createFontAssetParameters(name: String): AssetParameters<Font> {
        // todo
        val parameters = FreeTypeFontGenerationParameters(size = size, color = color)

        return FontAssetParameters(FontGeneratorAssetParameters(name), parameters)
    }

    @OnRender
    fun render(event: RenderEvent) {
        event.renderConveyor.render(layout, transform)
    }

}