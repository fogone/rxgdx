package ru.nobirds.rx.ui.input

import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.math.Vector2
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.utils.poolable
import ru.nobirds.rx.utils.set

class GesturesToEventProcessor(val target:()-> Eventable?) : GestureDetector.GestureListener {

    override fun zoom(initialDistance: Float, distance: Float): Boolean = poolable {
        fire<ZoomGestureEvent>(target()) {
            this.initialDistance = initialDistance
            this.distance = distance
        }
    }

    override fun tap(x: Float, y: Float, count: Int, button: Int): Boolean = poolable {
        fire<TapGestureEvent>(target()) {
            this.x = x
            this.y = y
            this.worldX = x
            this.worldY = y
            this.count = count
            this.button = button
        }
    }

    override fun touchDown(x: Float, y: Float, pointer: Int, button: Int): Boolean = poolable {
        fire<TouchDownGestureEvent>(target()) {
            this.x = x
            this.y = y
            this.worldX = x
            this.worldY = y
            this.pointer = pointer
            this.button = button
        }
    }

    override fun fling(velocityX: Float, velocityY: Float, button: Int): Boolean = poolable {
        fire<FlingGestureEvent>(target()) {
            this.velocityX = velocityX
            this.velocityY = velocityY
            this.button = button
        }
    }

    override fun pan(x: Float, y: Float, deltaX: Float, deltaY: Float): Boolean = poolable {
        fire<PanGestureEvent>(target()) {
            this.x = x
            this.y =  y
            this.worldX = x
            this.worldY = y
            this.deltaX = deltaX
            this.deltaY = deltaY
        }
    }

    override fun panStop(x: Float, y: Float, pointer: Int, button: Int): Boolean = poolable {
        fire<PanStopGestureEvent>(target()) {
            this.x = x
            this.y =  y
            this.worldX = x
            this.worldY = y
            this.pointer = pointer
            this.button = button
        }
    }

    override fun longPress(x: Float, y: Float): Boolean = poolable {
        fire<LongPressGestureEvent>(target()) {
            this.x = x
            this.y =  y
            this.worldX = x
            this.worldY = y
        }
    }

    override fun pinch(initialPointer1: Vector2, initialPointer2: Vector2,
                       pointer1: Vector2, pointer2: Vector2): Boolean = poolable {
        fire<PinchGestureEvent>(target()) {
            this.initialPointer1.set(initialPointer1)
            this.initialPointer2.set(initialPointer2)
            this.pointer1.set(pointer1)
            this.pointer2.set(pointer2)
        }
    }

    override fun pinchStop() {
        poolable {
            fire<PinchStopEvent>(target()) {}
        }
    }

}