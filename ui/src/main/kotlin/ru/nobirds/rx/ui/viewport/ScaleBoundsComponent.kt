package ru.nobirds.rx.ui.viewport

import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.reactive.subscribe
import ru.nobirds.rx.ui.BoundsComponent

@DependentComponents(BoundsComponent::class)
abstract class AbstractScaleBoundsComponent(parent: Module) : AbstractComponent(parent) {

    private val bounds:BoundsComponent by dependency()

    abstract fun scale(bounds:BoundsComponent)

    abstract val changed: ReactiveStream<Signal>

    @OnSetup
    fun setup() {
        changed.subscribe {
            scale(bounds)
        }
    }

}

