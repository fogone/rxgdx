package ru.nobirds.rx.ui

import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.ComponentDefinition
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.reactive.filter
import ru.nobirds.rx.reactive.filterIs
import ru.nobirds.rx.ui.debug.DebugBoundsRenderComponent
import ru.nobirds.rx.ui.input.AbstractScreenEvent
import ru.nobirds.rx.ui.viewport.WorldSizeChangedEvent
import ru.nobirds.rx.utils.MutableBounds
import ru.nobirds.rx.utils.MutablePosition
import ru.nobirds.rx.utils.obtainVec2
import ru.nobirds.rx.utils.poolable
import ru.nobirds.rx.utils.square
import ru.nobirds.rx.utils.x1
import ru.nobirds.rx.utils.x2
import ru.nobirds.rx.utils.x3
import ru.nobirds.rx.utils.x4
import ru.nobirds.rx.utils.y1
import ru.nobirds.rx.utils.y2
import ru.nobirds.rx.utils.y3
import ru.nobirds.rx.utils.y4

abstract class AbstractPositionComponent(parent: Module) : AbstractComponent(parent), MutablePosition, Invalidatable {

    val xProperty: Property<Float> = property(0f)
    override var x:Float by xProperty

    val yProperty: Property<Float> = property(0f)
    override var y:Float by yProperty

    override val invalidated: ReactiveStream<Signal> = changed(xProperty, yProperty)

}

class PositionComponent(parent: Module) : AbstractPositionComponent(parent) {

    override fun toString(): String = "position [$x,$y]"

}

class PivotComponent(parent: Module) : AbstractPositionComponent(parent) {

    override fun toString(): String = "pivot [$x,$y]"

}

fun ModuleDefinition<*>.withPivot(initializer:PivotComponent.()->Unit = {}): ComponentDefinition<PivotComponent> = withComponent(initializer)


object ParentSizeChangedEvent : AbstractEvent()
object ChildSizeChangedEvent : AbstractEvent()

@DependentComponents(TransformComponent::class, DebugBoundsRenderComponent::class)
class BoundsComponent(parent: Module) : AbstractComponent(parent), MutableBounds, Invalidatable {

    val position:PositionComponent by dependency()
    val transform:TransformComponent by dependency()

    override var x: Float by lazyProperty { position.xProperty }
    override var y: Float by lazyProperty { position.yProperty }

    val widthProperty: Property<Float> = property(0f)
    override var width:Float by widthProperty

    val heightProperty: Property<Float> = property(0f)
    override var height:Float by heightProperty

    private val invalidatedBounds: InvalidatedProperty by lazy { invalidated(true, widthProperty, heightProperty, position) }

    override val invalidated: ReactiveStream<Signal> by lazy { changed(invalidatedBounds) }

    val boundsEvents: ReactiveStream<AbstractScreenEvent> = events()
            .filterIs<AbstractScreenEvent>()
            .filter { filterInBounds(it) }

    private fun filterInBounds(e:AbstractScreenEvent):Boolean =
            contains(e.worldX, e.worldY)

    @OnUpdate
    fun updateBounds() {
        invalidatedBounds.with {
            parent.fire(ParentSizeChangedEvent, EventTraverseStrategies.full)
            parent.fire(ChildSizeChangedEvent, EventTraverseStrategies.bubble)
        }
    }

    override fun toString(): String = "${super.toString()} [$x x $y,$width x $height]"

    override fun contains(x:Float, y:Float): Boolean = poolable {
        val position = obtainVec2(x, y)

        val affine = transform.toAffine()

        val v1 = obtainVec2(affine.x1(), affine.y1())
        val v2 = obtainVec2(affine.x2(height), affine.y2(height))
        val v3 = obtainVec2(affine.x3(width, height), affine.y3(width, height))
        val v4 = obtainVec2(affine.x4(width), affine.y4(width))

        val trianglesSquare =
                square(v1, v2, position) +
                square(v2, v3, position) +
                square(v3, v4, position) +
                square(v4, v1, position)

        val rectangleSquare = square(v1, v2, v3, v4)

        val outside = trianglesSquare.toInt() > rectangleSquare.toInt() // todo!!

        !outside
    }

}

fun ModuleDefinition<*>.withBounds(width:Float, height:Float):ComponentDefinition<BoundsComponent> = withComponent {
    size(width, height)
}

@DependentComponents(BoundsComponent::class)
class ScreenBoundsScaleComponent(parent:Module) : AbstractComponent(parent) {

    private val bounds:BoundsComponent by dependency()

    @OnEvent(ResizeEvent::class)
    fun updateBounds(event:ResizeEvent) {
        bounds.bounds(event)
    }

}

fun ModuleDefinition<*>.withScreenBoundsScale():ComponentDefinition<ScreenBoundsScaleComponent> = withComponent()

@DependentComponents(BoundsComponent::class)
class WorldBoundsScaleComponent(parent:Module) : AbstractComponent(parent) {

    private val bounds:BoundsComponent by dependency()

    @OnEvent(WorldSizeChangedEvent::class)
    fun updateBounds(event:WorldSizeChangedEvent) {
        bounds.bounds(event)
    }

}

fun ModuleDefinition<*>.withWorldBoundsScale():ComponentDefinition<WorldBoundsScaleComponent> = withComponent()
