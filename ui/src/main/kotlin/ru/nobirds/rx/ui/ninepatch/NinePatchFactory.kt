package ru.nobirds.rx.ui.ninepatch

import com.badlogic.gdx.graphics.Pixmap
import ru.nobirds.rx.gl.texture.Texture
import ru.nobirds.rx.gl.texture.region

data class NinePatchBounds(val x:Int, val y:Int, val width:Int, val height:Int)

class NinePatchFactory(val bitmap: Pixmap) {

    private val texture: Texture by lazy { Texture.from(bitmap) }

    fun createNinePatch(bounds: NinePatchBounds = NinePatchBounds(0, 0, bitmap.width, bitmap.height)): NinePatch {
        val regions = findNinePatchRegions(bounds.x, bounds.y, bounds.width, bounds.height)

        return NinePatch(texture.region(bounds.x, bounds.y, bounds.width, bounds.height), regions)
    }

    private fun findNinePatchRegions(x: Int = 0, y: Int = 0,
                             width: Int = bitmap.width,
                             height: Int = bitmap.height): NinePatchSegments {

        val top = bitmap.rowPixels(x).segments()
        val bottom = bitmap.rowPixels(height - 1).segments()
        val left = bitmap.colPixels(y).segments()
        val right = bitmap.colPixels(width - 1).segments()

        assert(top.filled.count() >= 1 && left.filled.count() >= 1) { "Resize border must have at least one segment" }
        assert(right.filled.count() == 1 && bottom.filled.count() == 1) { "Content border must have only one segment" }

        return NinePatchSegments(top, left, bottom, right)
    }

    internal fun Pixmap.rowPixels(row: Int): Sequence<Int> = (0..width - 1).asSequence().map { getPixel(it, row) }
    internal fun Pixmap.colPixels(column: Int): Sequence<Int> = (0..height - 1).asSequence().map { getPixel(column, it) }

}

fun Sequence<Int>.segments(): Segments {
    val borders = borders()

    val borders1 = borders.subList(0, borders.size - 1)
    val borders2 = borders.subList(1, borders.size)

    var type = SegmentType.empty

    return borders1.zip(borders2).asSequence().mapIndexed { index, it ->
        val segment = Segment(type, index, it.first, it.second - it.first)
        type = type.flip()
        segment
    }.asSegments()
}

private fun SegmentType.flip() =
        if (this == SegmentType.empty) SegmentType.fill else SegmentType.empty

private fun Sequence<Int>.borders():List<Int> {
    val result = mutableListOf(0)

    var i = 0
    reduceIndexed { index, prev, current ->
        if(prev != current)
            result.add(index)
        i++
        current
    }

    result.add(i+1)

    return result
}
