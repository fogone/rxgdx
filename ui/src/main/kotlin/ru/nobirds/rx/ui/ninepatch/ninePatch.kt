package ru.nobirds.rx.ui.ninepatch

import ru.nobirds.rx.gl.texture.TextureRegion
import ru.nobirds.rx.utils.ImmutableBounds
import ru.nobirds.rx.utils.ImmutableSize

private fun Segments.computeScaledSegmentSize(total:Float):Float {
    val staticSize = empty.sumBy { it.size }
    val scaledCount = filled.count()
    return (total - staticSize) / scaledCount.toFloat()
}

class NinePatch(val region: TextureRegion,
                val segments: NinePatchSegments) {

    private val regions = NinePatchRegionMatrix(region, segments)

    fun scaleTo(size: ImmutableSize): ScaledNinePatch {
        val scaledRegions = computeScaledRegions(size)
        val content = computeScaledContent(size)
        return ScaledNinePatch(size, scaledRegions, content)
    }

    fun scaleForContent(contentSize: ImmutableSize): ScaledNinePatch {
        val size = computeSizeByContent(contentSize)
        return scaleTo(size)
    }

    private fun computeScaledRegions(size: ImmutableSize): List<ScaledNinePatchRegion> {
        val scaledWidth = segments.columnSegments.computeScaledSegmentSize(size.width)
        val scaledHeight = segments.rowSegments.computeScaledSegmentSize(size.height)

        var dx = 0f

        return segments.columnSegments.segments.flatMap { colSegment ->

            var dy = size.height

            val width = if (colSegment.filled) scaledWidth else colSegment.size.toFloat()

            val result = segments.rowSegments.segments.map { rowSegment ->
                val region = regions[colSegment.index, rowSegment.index]

                val height = if (rowSegment.filled) scaledHeight else rowSegment.size.toFloat()

                val scaledRegion = ScaledNinePatchRegion(region, ImmutableBounds.Companion.readonly(dx, dy - height, width, height))

                dy -= height

                scaledRegion
            }

            dx += width

            result
        }
    }

    private fun computeScaledContent(size: ImmutableSize): ImmutableBounds {
        val columnContentSegment = segments.contentColumnSegments.filled.first()
        val contentX = columnContentSegment.from
        val contentLeft = contentX + 1
        val contentRight = region.width - (contentLeft + columnContentSegment.size)
        val contentWidth = size.width - contentRight - contentLeft

        val rowContentSegment = segments.contentRowSegments.filled.first()
        val contentY = rowContentSegment.from
        val contentTop = contentY + 1
        val contentBottom = region.height - (contentTop + rowContentSegment.size)
        val contentHeight = size.height - contentBottom - contentTop

        return ImmutableBounds.readonly(contentX.toFloat(), contentY.toFloat(), contentWidth, contentHeight)
    }

    private fun computeSizeByContent(content: ImmutableSize): ImmutableSize {
        val columnContentSegment = segments.contentColumnSegments.filled.first()

        val contentLeft = columnContentSegment.from
        val contentRight = region.width - (contentLeft + columnContentSegment.size)

        val width = content.width + contentLeft + contentRight

        val rowContentSegment = segments.contentRowSegments.filled.first()

        val contentTop = rowContentSegment.from
        val contentBottom = region.height - (contentTop + rowContentSegment.size)

        val height = content.height + contentTop + contentBottom

        return ImmutableSize.readonly(width, height)
    }

}

data class ScaledNinePatchRegion(val region: TextureRegion, val bounds: ImmutableBounds)

class ScaledNinePatch(val size: ImmutableSize, val regions:List<ScaledNinePatchRegion>, val content: ImmutableBounds)
