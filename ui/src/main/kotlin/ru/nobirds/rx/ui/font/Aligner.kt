package ru.nobirds.rx.ui.font

import ru.nobirds.rx.ui.HorizontalAlign
import ru.nobirds.rx.utils.ImmutableSize

object Aligner {

    private val center = { size: ImmutableSize, line: TextLayoutLine -> (size.width - line.width) / 2f }
    private val right = { size: ImmutableSize, line: TextLayoutLine -> size.width - line.width }

    private fun byHorizontalAlign(align: HorizontalAlign) = when (align) {
        HorizontalAlign.center -> center
        HorizontalAlign.right -> right
        else -> null
    }

    fun align(size: ImmutableSize, lines: List<TextLayoutLine>, alignment: HorizontalAlign) {
        byHorizontalAlign(alignment)?.let {
            for (line in lines) {
                val offset = it(size, line)
                for (rune in line.runes) {
                    rune.x += offset
                }
            }
        }
    }
}