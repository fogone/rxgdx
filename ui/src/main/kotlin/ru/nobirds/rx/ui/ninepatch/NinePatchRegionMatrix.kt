package ru.nobirds.rx.ui.ninepatch

import ru.nobirds.rx.gl.texture.TextureRegion
import ru.nobirds.rx.gl.texture.region

internal class NinePatchRegionMatrix(val texture: TextureRegion, val segments: NinePatchSegments) {

    val width:Int = segments.columnSegments.segments.size
    val height:Int = segments.rowSegments.segments.size

    private val container = createContainer()

    private fun createContainer(): Array<Array<TextureRegion>> = Array(width) { dx ->
        val (_, _, x, sizeX) = segments.columnSegments.segments[dx]

        Array(height) { dy ->
            val (_, _, y, sizeY) = segments.rowSegments.segments[dy]

            texture.region(x, y, sizeX, sizeY)
        }
    }

    operator fun get(x: Int, y: Int): TextureRegion {
        return container[x][y]
    }

}