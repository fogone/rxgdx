package ru.nobirds.rx.ui.action

import ru.nobirds.rx.action.Action
import ru.nobirds.rx.event.Event
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.reactive.filter
import ru.nobirds.rx.reactive.subscribe

class EventAction(val module: Module, val condition:(Event)->Boolean) : Action {

    private var done = false

    init {
        module.events().filter(condition).subscribe {
            done = true
        }
    }

    override fun process(delta: Float): Boolean {
        return done
    }
}