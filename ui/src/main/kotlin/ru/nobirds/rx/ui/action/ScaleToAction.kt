package ru.nobirds.rx.ui.action

import com.badlogic.gdx.math.Interpolation
import ru.nobirds.rx.action.AbstractTemporalAction
import ru.nobirds.rx.action.ActionBuilder
import ru.nobirds.rx.action.actions
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.ui.TransformComponent
import ru.nobirds.rx.utils.ImmutableVec2f
import ru.nobirds.rx.utils.scaleTo

class ScaleToAction(val target: Module, val end: ImmutableVec2f, duration:Float, interpolation: Interpolation = Interpolation.linear) :
        AbstractTemporalAction(duration, interpolation) {

    private val transform: TransformComponent = target.findComponent()

    private val start by lazy { ImmutableVec2f.readonly(transform.scaleX, transform.scaleY) }

    override fun update(processed: Float) {
        transform.scaleTo(compute(processed) { x }, compute(processed) { y })
    }

    private inline fun compute(processed: Float, component: ImmutableVec2f.()->Float):Float
            = start.component() + (end.component() - start.component()) * processed
}

fun Module.scaleTo(end: ImmutableVec2f, duration:Float, interpolation: Interpolation = Interpolation.linear) {
    actions().run(ScaleToAction(this, end, duration, interpolation))
}

fun ActionBuilder.scaleTo(end: ImmutableVec2f, duration:Float, interpolation: Interpolation = Interpolation.linear) {
    action(ScaleToAction(component.parent, end, duration, interpolation))
}