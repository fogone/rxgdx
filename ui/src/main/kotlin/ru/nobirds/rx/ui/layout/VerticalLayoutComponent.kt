package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.ModuleDefinition
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.module.findOptionalComponent
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.HorizontalAlign
import ru.nobirds.rx.ui.PreferBoundsComponent
import ru.nobirds.rx.ui.UiModule
import ru.nobirds.rx.ui.lazyProperty

@DependentComponents(VerticalLayoutComponent::class)
class VBox(id:String) : UiModule(id) {

    val verticalLayout:VerticalLayoutComponent by dependency()
    var horizontalAlign:HorizontalAlign by lazyProperty { verticalLayout.horizontalAlignProperty }

}

fun ModuleDefinition<*>.vbox(id:String, initializer:ModuleDefinition<VBox>.()->Unit):ModuleDefinition<VBox>
        = withModule(id, initializer)

class VerticalLayoutComponent(parent: Module) : LayoutComponent(parent) {

    val horizontalAlignProperty: Property<HorizontalAlign> = property(HorizontalAlign.none)
    var horizontalAlign:HorizontalAlign by horizontalAlignProperty

    override fun updatePreferSize() {
        var width = 0f
        var height = spacing.padding.top + spacing.padding.bottom + spacing.margin

        childrenWithBounds.forEach {
            val bounds = it.findComponent<BoundsComponent>()
            val prefBounds = it.findOptionalComponent<PreferBoundsComponent>()

            width = Math.max(width, prefBounds?.prefWidth ?: bounds.width) // max width
            height += prefBounds?.prefHeight ?: bounds.height // sum of heights
        }

        width += spacing.padding.left + spacing.padding.right

        preferBounds.pref(width, height)
    }

    override fun updateLayout() {
        val innerWidth = bounds.width - spacing.padding.left - spacing.padding.right

        var y = bounds.height - spacing.padding.top + spacing.margin

        childrenWithBounds.forEach {
            val childBounds = it.findComponent<BoundsComponent>()
            val childPreferBounds = it.findOptionalComponent<PreferBoundsComponent>()

            var width = childBounds.width
            var height = childBounds.height

            if (childPreferBounds != null) {
                width = Math.max(Math.min(childPreferBounds.prefWidth, innerWidth), childPreferBounds.minWidth)

                if(childPreferBounds.maxWidth > 0f && width > childPreferBounds.maxWidth)
                    width = childPreferBounds.maxWidth

                height = childPreferBounds.prefHeight
            }

            val x = spacing.padding.left + when(horizontalAlign) {
                HorizontalAlign.right -> innerWidth - width
                HorizontalAlign.center -> (innerWidth - width) / 2f
                else -> 0f
            }

            y -= height + spacing.margin

            childBounds.bounds(x, y, width, height)
        }

    }

}

