package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.OnUpdate
import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.annotation.DependentComponents
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.Event
import ru.nobirds.rx.event.annotation.OnEvent
import ru.nobirds.rx.module.AfterModuleAttachedEvent
import ru.nobirds.rx.module.ChildModuleAttached
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.OnSetup
import ru.nobirds.rx.module.filterByComponent
import ru.nobirds.rx.module.findOptionalComponent
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.reactive.subscribe
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.ChildSizeChangedEvent
import ru.nobirds.rx.ui.PreferBoundsComponent
import ru.nobirds.rx.ui.SpacingComponent

@DependentComponents(
        BoundsComponent::class,
        PreferBoundsComponent::class,
        SpacingComponent::class
)
abstract class LayoutComponent(parent: Module) : AbstractComponent(parent), Invalidatable {

    val bounds: BoundsComponent by dependency()
    val preferBounds: PreferBoundsComponent by dependency()
    val spacing: SpacingComponent by dependency()

    private val invalidatedLayout: InvalidatedProperty = invalidated()
    private val invalidatedSize: InvalidatedProperty = invalidated()

    override val invalidated: ReactiveStream<Signal> = changed(invalidatedLayout, invalidatedSize)

    val childrenWithBounds: Sequence<Module>
        get() = parent.children.filterByComponent<BoundsComponent>()

    val childrenBounds: Sequence<BoundsComponent>
        get() = parent.children.map { it.findOptionalComponent<BoundsComponent>() }.filterNotNull()

    @OnSetup
    fun setup() {
        bounds.invalidated.subscribe {
            invalidate()
        }
    }

    @OnUpdate
    fun updateLayoutIfInvalidated() {
        invalidatedSize.with {
            updatePreferSize()
            updateSize()
        }

        invalidatedLayout.with {
            updateLayout()
        }
    }

    private fun updateSize() {
        bounds.size(preferBounds.prefWidth, preferBounds.prefHeight)
    }

    @OnEvent(
            ResizeEvent::class,
            ChildSizeChangedEvent::class,
            ChildModuleAttached::class,
            AfterModuleAttachedEvent::class
    )
    fun invalidateEvent() {
        invalidate()
    }

    fun invalidate() {
        invalidatedLayout.invalidate()
        invalidatedSize.invalidate()
    }

    protected abstract fun updatePreferSize()

    protected abstract fun updateLayout()

}

