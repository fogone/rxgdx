package ru.nobirds.rx.ui.action

import com.badlogic.gdx.math.Interpolation
import ru.nobirds.rx.action.AbstractRelativeTemporalAction
import ru.nobirds.rx.action.ActionBuilder
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.findComponent
import ru.nobirds.rx.ui.TransformComponent
import ru.nobirds.rx.utils.rotate

class RotateAction(target: Module, val rotation:Float, duration:Float, interpolation: Interpolation = Interpolation.linear) :
        AbstractRelativeTemporalAction(duration, interpolation) {

    private val transform: TransformComponent = target.findComponent()

    override fun delta(percentDelta: Float) {
        transform.rotate(percentDelta * rotation)
    }

}

fun ActionBuilder.rotate(rotation:Float, duration:Float, interpolation: Interpolation = Interpolation.linear) {
    action(RotateAction(component.parent, rotation, duration, interpolation))
}