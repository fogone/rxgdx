package ru.nobirds.rx.di

import kotlin.reflect.KClass

interface Context {

    fun <T : Any> findAll(type: KClass<T>): Sequence<T>

    fun <T : Any> find(type: KClass<T>): T

    fun <T : Any> find(type: KClass<T>, name: String): T

    fun <T : Any> findWithPreferName(type: KClass<T>, name: String): T

}

inline fun <reified T : Any> Context.find(name: String): T = find(T::class, name)
inline fun <reified T : Any> Context.find(): T = find(T::class)
inline fun <reified T : Any> Context.findWitPreferName(name: String): T = findWithPreferName(T::class, name)
inline fun <reified T : Any> Context.findAll(): Sequence<T> = findAll(T::class)

