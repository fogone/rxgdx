package ru.nobirds.rx.di.annotations

import ru.nobirds.rx.di.configuration.TypeInformation
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.lang.reflect.WildcardType
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.jvm.javaType

interface TypeInformationProvider {

    fun findTypeInformation(type: Type): TypeInformation

    fun findTypeInformation(type: KType): TypeInformation = findTypeInformation(type.javaType)

    fun findTypeInformation(parameter: KParameter): TypeInformation = findTypeInformation(parameter.type)

}

class DefaultTypeInformationProvider() : TypeInformationProvider {

    override fun findTypeInformation(type: Type): TypeInformation {
        when (type) {
            is Class<*> -> return TypeInformation(type.kotlin)
            is ParameterizedType -> {
                val klass = (type.rawType as Class<*>).kotlin
                val typeArguments = type.actualTypeArguments
                return TypeInformation(klass, typeArguments.map { findTypeInformation(it) })
            }
            is WildcardType -> {
                val types = if (type.lowerBounds.isNotEmpty()) type.lowerBounds
                else if (type.upperBounds.isNotEmpty()) type.upperBounds
                else emptyArray()

                val kclass = if(types.isNotEmpty()) (types.first() as Class<*>).kotlin else Any::class

                return TypeInformation(kclass)
            }
        }

        throw IllegalArgumentException("Could not find info for type $type")
    }

}