package ru.nobirds.rx.di.configuration

import kotlin.reflect.KClass

interface ContextConfiguration {

    val definitions: List<ObjectDefinition<*>>

    fun <T : Any> findDefinitionsByType(type: KClass<T>): List<ObjectDefinition<T>>

    fun <T : Any> findOptionalDefinitionsByType(type: KClass<T>): List<ObjectDefinition<T>>

    fun <T : Any> findDefinition(type: KClass<T>, name: String): ObjectDefinition<T>

    fun <T : Any> findOptionalDefinition(type: KClass<T>, name: String): ObjectDefinition<T>?

    fun <T : Any> findDefinitionWithPreferName(type: KClass<T>, name: String): ObjectDefinition<T>

    fun <T : Any> findOptionalDefinitionWithPreferName(type: KClass<T>, name: String): ObjectDefinition<T>?

}

inline fun <reified T:Any> ContextConfiguration.findDefinition(name: String): ObjectDefinition<T>
        = findDefinition(T::class, name)