package ru.nobirds.rx.di.annotations

import ru.nobirds.rx.di.configuration.ObjectScope
import kotlin.reflect.KClass

annotation class Configuration
annotation class Definition(val override: Boolean = false)
annotation class Setup
annotation class Scope(val value:KClass<out ObjectScope>)

