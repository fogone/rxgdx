package ru.nobirds.rx.di.annotations

import ru.nobirds.rx.di.configuration.ContextConfiguration
import ru.nobirds.rx.di.configuration.DefaultContextConfiguration
import ru.nobirds.rx.di.configuration.ObjectDefinition
import ru.nobirds.rx.di.findAnnotatedFunctions
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.full.createInstance

class AnnotationContextConfigurationFactory(val configurationInstances: List<Any>) {

    private val methodObjectDefinitionFactory = MethodObjectDefinitionFactory()

    fun create(): ContextConfiguration {
        val definitions = createDefinitions(configurationInstances)

        return DefaultContextConfiguration(definitions)
    }

    private fun createDefinitions(configurationInstances: List<Any>): List<ObjectDefinition<*>> {
        return configurationInstances
                .flatMap { createConfiguration(it) }
                .flatMap { configurationInstance -> createDefinitionsForInstance(configurationInstance) }
    }

    private fun createDefinitionsForInstance(configurationInstance: Any): List<ObjectDefinition<*>> {
        val configurationClass = configurationInstance.javaClass.kotlin

        require(configurationClass.annotations.any { it.annotationClass == Configuration::class })
            { "Class $configurationClass does not contains @Configuration annotation." }

        val dependentConfigurations = findConfigurations(configurationClass, configurationInstance)

        val dependentDefinitions = createDefinitions(dependentConfigurations)

        val definitions = findDefinitions(configurationClass, configurationInstance)

        val overrides = definitions.filter { it.override }.map { it.name }.toSet()

        val dependentNames = dependentDefinitions.map { it.name }.toSet()

        if (!dependentNames.containsAll(overrides)) {
            val notFound = overrides - dependentNames
            throw IllegalStateException("Definitions $notFound override nothing")
        }

        return definitions + dependentDefinitions.filter { it.name !in overrides }
    }

    @Suppress("UNCHECKED_CAST")
    private fun findDefinitions(configurationClass: KClass<Any>, configurationInstance: Any): List<ObjectDefinition<*>> {
        return configurationClass
                .findAnnotatedFunctions<Definition>()
                .map { createDefinitionFromMethod(configurationInstance, it as KFunction<Any>) }
    }

    private fun findConfigurations(configurationClass: KClass<Any>, configurationInstance: Any): List<Any> {
        return configurationClass
                .findAnnotatedFunctions<Configuration>()
                .map { it.call(configurationInstance) }
                .filterNotNull()
    }

    @Suppress("UNCHECKED_CAST")
    private fun createConfiguration(it: Any):Iterable<Any> {
        return when (it) {
            is KClass<*> -> listOf(it.createInstance())
            is Iterable<*> -> it as Iterable<Any>
            else -> listOf(it)
        }
    }

    private fun <R : Any> createDefinitionFromMethod(instance: Any, function: KFunction<R>): ObjectDefinition<*> {
        return methodObjectDefinitionFactory.create(instance, function)
    }

}