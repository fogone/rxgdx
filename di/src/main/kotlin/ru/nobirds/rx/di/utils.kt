package ru.nobirds.rx.di

import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.full.allSuperclasses
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.functions

fun <T : Any> KClass<T>.hierarchy():Sequence<KClass<*>> = fetchInstanceTypes() // todo?

fun <T : Any> KClass<T>.fetchInstanceTypes():Sequence<KClass<*>> = (this.allSuperclasses + this).asSequence()

inline fun <reified A:Annotation> KClass<*>.findAnnotatedFunctions(): List<KFunction<*>> =
        this.functions.filter { it.findAnnotation<A>() != null }

