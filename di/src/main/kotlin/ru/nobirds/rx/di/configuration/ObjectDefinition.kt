package ru.nobirds.rx.di.configuration

import kotlin.reflect.KClass
import kotlin.reflect.full.isSuperclassOf

interface ObjectDefinition<T : Any> {

    val scope: ObjectScope

    val name: String

    val type: KClass<T>

    val types: List<KClass<*>>

    val dependencies: List<ObjectDefinitionReference>

    val override: Boolean

    fun create(parameters: List<Any>): T

}

@Suppress("UNCHECKED_CAST")
fun <F : Any> ObjectDefinition<*>.cast(type: KClass<F>): ObjectDefinition<F>? {
    return if (this.type.isSuperclassOf(type)) {
        this as ObjectDefinition<F>
    } else {
        null
    }
}

