package ru.nobirds.rx.di.configuration

import kotlin.reflect.KClass

class DefaultContextConfiguration(override val definitions: List<ObjectDefinition<*>>) : ContextConfiguration {

    private val byTypeMappings = createTypeMappings(definitions)
    private val byNameMappings = createNameMappings(definitions)

    private fun createNameMappings(definitions: List<ObjectDefinition<*>>): Map<String, ObjectDefinition<*>> {
        return definitions.associate { it.name to it }
    }

    private fun createTypeMappings(definitions: List<ObjectDefinition<*>>): Map<KClass<*>, List<ObjectDefinition<*>>> {
        return definitions.flatMap { d -> d.types.map { it to d } }.groupBy({ it.first }, { it.second })
    }

    override fun <T : Any> findDefinitionsByType(type: KClass<T>): List<ObjectDefinition<T>> {
        val definitions = findOptionalDefinitionsByType(type)

        if (definitions.isEmpty()) {
            throw IllegalArgumentException("Definitions with type $type not found.")
        }

        return definitions
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> findOptionalDefinitionsByType(type: KClass<T>): List<ObjectDefinition<T>> {
        return byTypeMappings.getOrElse(type) { emptyList() } as List<ObjectDefinition<T>>
    }

    override fun <T : Any> findDefinition(type: KClass<T>, name: String): ObjectDefinition<T> {
        val objectDefinition = findOptionalDefinition(type, name) ?:
                throw IllegalArgumentException("Object definition with name $name not found.")

        return objectDefinition.cast(type) ?:
                throw IllegalArgumentException("Object definition with name $name has type ${objectDefinition.type} but requested $type.")
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> findOptionalDefinition(type: KClass<T>, name: String): ObjectDefinition<T>? {
        return byNameMappings[name] as ObjectDefinition<T>?
    }

    override fun <T : Any> findDefinitionWithPreferName(type: KClass<T>, name: String): ObjectDefinition<T> {
        val definitions = findDefinitionsByType(type)

        if (definitions.size == 1) {
            return definitions.first()
        }

        return definitions.find { it.name == name } ?:
                throw IllegalArgumentException("Found many object definitions $definitions, but there is no with name $name")
    }

    override fun <T : Any> findOptionalDefinitionWithPreferName(type: KClass<T>, name: String): ObjectDefinition<T>? {
        val definitions = findOptionalDefinitionsByType(type)

        if (definitions.size == 1) {
            return definitions.first()
        }

        return definitions.find { it.name == name }
    }
}