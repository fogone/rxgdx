package ru.nobirds.rx.di.annotations

import ru.nobirds.rx.di.configuration.ObjectDefinitionReference
import ru.nobirds.rx.di.configuration.ObjectScope
import ru.nobirds.rx.di.configuration.SingletonScope
import ru.nobirds.rx.di.fetchInstanceTypes
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.createInstance

class MethodObjectDefinitionFactory {

    private val typeInformationProvider = DefaultTypeInformationProvider()

    fun <T : Any> create(configurationInstance: Any, function: KFunction<T>): MethodObjectDefinition<T> {
        val scope: ObjectScope = findScopeOrDefault(function)

        val name: String = function.name

        val override: Boolean = isOverride(function)

        @Suppress("UNCHECKED_CAST")
        val type: KClass<T> = function.returnType.classifier as? KClass<T> ?:
                throw IllegalArgumentException("") // todo

        val types: List<KClass<*>> = type.fetchInstanceTypes().toList()

        val dependencies: List<ObjectDefinitionReference> = function.parameters
                .filter { !it.isOptional && it.kind == KParameter.Kind.VALUE }
                .map { ObjectDefinitionReference(it.name!!, typeInformationProvider.findTypeInformation(it)) }

        return MethodObjectDefinition(
                configurationInstance, function, scope, name, type, types, dependencies, override
        )
    }

    private fun <T> isOverride(function: KFunction<T>): Boolean {
        return (function.annotations.firstOrNull { it.annotationClass == Definition::class } as? Definition)?.override
                ?: false
    }

    private fun findScopeOrDefault(function: KFunction<*>): ObjectScope {
        val scopeType = function.annotations
                .firstOrNull { it.annotationClass == Scope::class }
                ?.let { (it as Scope).value } ?: SingletonScope::class

        return scopeType.createInstance()
    }

}