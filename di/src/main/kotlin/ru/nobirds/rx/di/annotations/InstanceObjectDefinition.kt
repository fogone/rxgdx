package ru.nobirds.rx.di.annotations

import ru.nobirds.rx.di.configuration.ObjectDefinition
import ru.nobirds.rx.di.configuration.ObjectDefinitionReference
import ru.nobirds.rx.di.configuration.ObjectScope
import ru.nobirds.rx.di.configuration.SingletonScope
import ru.nobirds.rx.di.fetchInstanceTypes
import kotlin.reflect.KClass

data class InstanceObjectDefinition<T : Any>(
        val instance: T,
        override val name: String,
        override val override: Boolean = false
) : ObjectDefinition<T> {

    override val scope: ObjectScope = SingletonScope()

    override val type: KClass<T> = instance.javaClass.kotlin

    override val dependencies: List<ObjectDefinitionReference> = emptyList()

    override val types: List<KClass<*>> = type.fetchInstanceTypes().toList()

    override fun create(parameters: List<Any>): T = instance

}