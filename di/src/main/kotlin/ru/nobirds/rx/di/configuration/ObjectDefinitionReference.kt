package ru.nobirds.rx.di.configuration

import kotlin.reflect.KClass

data class TypeInformation(val type: KClass<*>, val parameterTypes: List<TypeInformation> = emptyList())

data class ObjectDefinitionReference(val name:String, val type: TypeInformation)

