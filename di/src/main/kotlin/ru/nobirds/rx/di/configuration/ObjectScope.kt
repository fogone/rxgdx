package ru.nobirds.rx.di.configuration

interface ObjectScope

class SingletonScope : ObjectScope {

    override fun toString(): String = "SingletonScope"

    override fun equals(other: Any?): Boolean {
        return other is SingletonScope
    }

    override fun hashCode(): Int {
        return toString().hashCode()
    }

}