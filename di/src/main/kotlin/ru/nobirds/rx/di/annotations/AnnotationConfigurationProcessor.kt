package ru.nobirds.rx.di.annotations

import ru.nobirds.rx.di.Context
import ru.nobirds.rx.di.DefaultContext

class AnnotationConfigurationProcessor(val configurations: List<Any>, val parent:Context? = null) {

    fun createContext(): Context {
        val contextConfiguration = AnnotationContextConfigurationFactory(configurations).create()

        return DefaultContext(contextConfiguration, parent)
    }

}

inline fun <reified T : Any> annotationContext(parent:Context? = null): Context {
    return if(parent != null) annotationContext(parent, T::class) else annotationContext(T::class)
}

fun annotationContext(vararg configurations: Any): Context {
    return AnnotationConfigurationProcessor(configurations.toList()).createContext()
}

fun annotationContext(parent:Context, vararg configurations: Any): Context {
    return AnnotationConfigurationProcessor(configurations.toList(), parent).createContext()
}
