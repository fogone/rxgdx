package ru.nobirds.rx.di.annotations

import ru.nobirds.rx.di.configuration.ObjectDefinition
import ru.nobirds.rx.di.configuration.ObjectDefinitionReference
import ru.nobirds.rx.di.configuration.ObjectScope
import kotlin.reflect.KClass
import kotlin.reflect.KFunction

data class MethodObjectDefinition<T : Any>(
        val configuration: Any,
        val function: KFunction<T>,
        override val scope: ObjectScope,
        override val name: String,
        override val type: KClass<T>,
        override val types: List<KClass<*>>,
        override val dependencies: List<ObjectDefinitionReference>,
        override val override: Boolean) : ObjectDefinition<T> {

    override fun create(parameters: List<Any>): T = function.call(configuration, *parameters.toTypedArray())

}

