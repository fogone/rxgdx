package ru.nobirds.rx.di

import ru.nobirds.rx.di.annotations.InstanceObjectDefinition
import ru.nobirds.rx.di.configuration.ContextConfiguration
import ru.nobirds.rx.di.configuration.DefaultContextConfiguration
import ru.nobirds.rx.di.configuration.ObjectDefinition
import ru.nobirds.rx.di.configuration.ObjectDefinitionReference
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

class DefaultContext(configuration: ContextConfiguration, val parent: Context? = null) : Context {

    private val configuration: ContextConfiguration = createExtendedConfiguration(configuration)

    private val contextByType: MutableMap<KClass<*>, MutableList<Any>> = hashMapOf()
    private val context: MutableMap<String, Any> = hashMapOf()
    private val initialized: MutableMap<ObjectDefinition<*>, Any> = hashMapOf()

    init {
        // createInstances()
    }

    private fun createExtendedConfiguration(configuration: ContextConfiguration): ContextConfiguration {
        val contextDefinition = InstanceObjectDefinition(this, "context")
        val allDefinitions = listOf(contextDefinition) + configuration.definitions
        return DefaultContextConfiguration(allDefinitions)
    }

    private fun createInstances() {
        configuration.definitions.map { findByDefinition(it) }
    }


    private fun <T : Any> findByDefinition(definition: ObjectDefinition<T>):T {
        val instance = initialized[definition]

        if (instance != null) {
            @Suppress("UNCHECKED_CAST")
            return instance as T
        }

        return registerInstance(definition, createObject(definition))
    }

    private fun <T : Any> registerInstance(definition: ObjectDefinition<T>, instance: T): T {
        initialized[definition] = instance

        val name = definition.name

        context[name] = instance

        instance.javaClass.kotlin.fetchInstanceTypes().forEach {
            contextByType.getOrPut(it) { arrayListOf() }.add(instance)
        }

        return instance
    }

    private fun <T : Any> createObject(definition: ObjectDefinition<T>): T {
        val dependencies = definition.dependencies
                .map { findInstance(it) }

        return definition.create(dependencies)
    }

    private fun ObjectDefinitionReference.isContainer():Boolean {
        return type.type.isSubclassOf(Iterable::class)
    }

    private fun findInstance(reference: ObjectDefinitionReference):Any {
        return if (reference.isContainer()) {
            val containerType = reference.type.type
            val byType = configuration.findDefinitionsByType(reference.type.parameterTypes.first().type)
            createContainer(containerType, byType.map { findByDefinition(it) })
        } else {
            findByDefinition(configuration.findDefinitionWithPreferName(reference.type.type, reference.name))
        }
    }

    private fun createContainer(containerType: KClass<*>, list: List<Any>): Any {
        return when(containerType) {
            Iterable::class -> list
            List::class -> list
            Set::class -> list.toSet()
            else -> throw IllegalArgumentException("Container $containerType not supported.")
        }
    }

    override fun <T : Any> findAll(type: KClass<T>): Sequence<T> {

        @Suppress("UNCHECKED_CAST")
        val instances = contextByType.getOrElse(type) { emptyList<T>() }.map { it as T }.asSequence()

        if (parent != null) {
            return instances + parent.findAll(type)
        }

        return instances
    }

    override fun <T : Any> find(type: KClass<T>): T {
        val instances = contextByType.getOrElse(type) { emptyList<T>() }

        if (instances.isEmpty()) {
            val definitions = configuration.findDefinitionsByType(type)

            if (definitions.isNotEmpty()) {
                definitions.forEach { findByDefinition(it) }
                return find(type)
            }

            if (parent != null) {
                return parent.find(type)
            }

            throw IllegalArgumentException("Object with type $type not found in context")
        }

        if(instances.size > 1)
            throw IllegalArgumentException("Object with type $type has more than one instances in context, use name to select one")

        @Suppress("UNCHECKED_CAST")
        return instances.first() as T
    }

    override fun <T : Any> find(type: KClass<T>, name: String): T {
        val instance = context[name]

        if (instance == null) {
            val definition = if(parent != null) configuration.findOptionalDefinition(type, name)
                else configuration.findDefinition(type, name)

            if (definition != null) {
                return findByDefinition(definition)
            }

            if (parent != null) {
                return parent.find(type, name)
            }
            throw IllegalArgumentException("Object with type $type and name $name not found in context")
        }

        @Suppress("UNCHECKED_CAST")
        return instance as T
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> findWithPreferName(type: KClass<T>, name: String): T {
        val instance = context[name]

        if (instance == null) {
            val instances = contextByType.getOrElse(type) { emptyList<T>() }

            if (instances.size == 1) {
                return instances.first() as T
            }

            if (instances.isEmpty()) {
                val definition = if(parent != null) configuration.findOptionalDefinitionWithPreferName(type, name)
                    else configuration.findDefinitionWithPreferName(type, name)

                if (definition != null) {
                    return findByDefinition(definition)
                }

                if (parent != null) {
                    return parent.findWithPreferName(type, name)
                }
                throw IllegalArgumentException("Object with type $type not found in context")
            }

            throw IllegalArgumentException("Object with type $type not one and name $name not found in context")
        }

        return instance as T

    }

}