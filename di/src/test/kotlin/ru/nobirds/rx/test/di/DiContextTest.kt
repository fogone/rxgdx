package ru.nobirds.rx.test.di

import org.junit.Assert.assertEquals
import org.junit.Test
import ru.nobirds.rx.di.Context
import ru.nobirds.rx.di.annotations.Configuration
import ru.nobirds.rx.di.annotations.Definition
import ru.nobirds.rx.di.annotations.annotationContext
import ru.nobirds.rx.di.find

data class ContextHolder(val context: Context)
@Configuration
class ContextInjectionConfiguration {

    @Definition
    fun contextHolder(context: Context): ContextHolder {
        return ContextHolder(context)
    }

}

class DiContextTest {

    @Test
    fun listInjectTest() {
        val context = annotationContext<ContextInjectionConfiguration>()

        val holder = context.find<ContextHolder>()

        assertEquals(context, holder.context)
    }

}