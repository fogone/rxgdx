package ru.nobirds.rx.test.di

import org.junit.Assert.assertEquals
import org.junit.Test
import ru.nobirds.rx.di.annotations.Configuration
import ru.nobirds.rx.di.annotations.Definition
import ru.nobirds.rx.di.annotations.annotationContext
import ru.nobirds.rx.di.find

data class DateList(val dates:List<Date>)
@Configuration
class ListInjectionConfiguration {

    @Definition
    fun item1() = Date()

    @Definition
    fun item2() = Date()

    @Definition
    fun itemsList(items:List<Date>): DateList {
        return DateList(items)
    }

}

class DiListTest {

    @Test
    fun listInjectTest() {
        val context = annotationContext<ListInjectionConfiguration>()

        val list = context.find<DateList>()

        assertEquals(2, list.dates.size)
    }

}