package ru.nobirds.rx.test.di

import org.junit.Assert
import org.junit.Test
import ru.nobirds.rx.di.annotations.AnnotationContextConfigurationFactory
import ru.nobirds.rx.di.annotations.Configuration
import ru.nobirds.rx.di.annotations.Definition
import ru.nobirds.rx.di.annotations.annotationContext
import ru.nobirds.rx.di.find

data class Date(val time: Long = 0L)

@Configuration
class DependentConfiguration {

    @Definition
    fun someDateObject(): Date {
        return Date(10)
    }

    @Definition
    fun anotherDate() = Date(0)

}

@Configuration
class SimpleConfiguration(val temp: Long) {

    @Configuration
    fun dependentConfiguration() = DependentConfiguration()

    @Definition(override = true)
    fun someDateObject(): Date {
        return Date(temp * 1000000)
    }

    @Definition
    fun someOtherDate(someDateObject: Date): Date {
        return Date((someDateObject.time - 1000000) / temp)
    }

}

class AnnotationsConfigTest {

    private val simpleConfiguration = SimpleConfiguration(10L)

    @Test
    fun configurationTest() {
        val contextConfiguration = AnnotationContextConfigurationFactory(listOf(simpleConfiguration))
                .create()

        val definitions = contextConfiguration.findDefinitionsByType(Date::class)

        Assert.assertEquals(3, definitions.size)
        Assert.assertEquals(setOf("someDateObject", "someOtherDate", "anotherDate"), definitions.map { it.name }.toSet())
    }

    @Test
    fun contextTest1() {
        val context = annotationContext(simpleConfiguration)

        val date: Date = context.find("someOtherDate")

        Assert.assertEquals((10L * 1000000L - 1000000L)/10L, date.time)

        val overridedDate: Date = context.find("someDateObject")

        Assert.assertEquals(10L * 1000000, overridedDate.time)
    }

}

