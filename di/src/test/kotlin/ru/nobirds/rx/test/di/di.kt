package ru.nobirds.rx.test.di

import org.junit.Assert.assertEquals
import org.junit.Test
import ru.nobirds.rx.di.annotations.Configuration
import ru.nobirds.rx.di.annotations.Definition
import ru.nobirds.rx.di.annotations.annotationContext

interface I
interface W

open class A : I
class B : A(), W

@Configuration
class TestContextConfiguration(val originalA: A, val originalB: B) {

    @Definition
    fun originalA() = originalA

    @Definition
    fun originalB() = originalB

}

class DiTest {

    @Test
    fun test1() {

        val originalA = A()
        val originalB = B()

        val originalSet = setOf(originalA, originalB)

        val context = annotationContext(TestContextConfiguration(originalA, originalB))

        val b = context.find(B::class)

        assertEquals(originalB, b)

        val a = context.find(A::class, "originalA")

        assertEquals(originalA, a)

        val foundA = context.findAll(A::class).toSet()

        assertEquals(originalSet, foundA)

        val foundI = context.findAll(I::class).toSet()

        assertEquals(originalSet, foundI)

        val foundW = context.findAll(W::class).toSet()

        assertEquals(setOf(originalB), foundW)
    }

}


