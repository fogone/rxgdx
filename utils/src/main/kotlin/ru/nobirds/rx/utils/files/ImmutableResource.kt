package ru.nobirds.rx.utils.files

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.utils.BufferUtils
import java.io.InputStream
import java.io.Reader
import java.nio.ByteBuffer
import java.nio.charset.Charset

interface Resource {

    val name: String

}

val Resource.extension: String
    get() = name.substringAfterLast(".")

val Resource.fileName: String
    get() = name.substringAfterLast("/").substringBeforeLast(".")

val Resource.path: String
    get() = name.substringBeforeLast("/")

interface ImmutableResource : Resource {

    fun toInputStream(): InputStream

    fun toFileHandle(): FileHandle

}

fun ImmutableResource.toReader(charset: Charset = Charsets.UTF_8): Reader = toInputStream().reader(charset)

fun ImmutableResource.readText(): String = toInputStream().reader().use { it.readText() }
fun ImmutableResource.readBytes(): ByteArray = toInputStream().use { it.readBytes() }

fun ImmutableResource.readByteBuffer(): ByteBuffer {
    val bytes = readBytes()
    val buffer = BufferUtils.newUnsafeByteBuffer(bytes.size)
    BufferUtils.copy(bytes, 0, buffer, bytes.size)
    return buffer
}
