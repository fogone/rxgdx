package ru.nobirds.rx.utils

import com.badlogic.gdx.math.Interpolation

object Interpolations {

    val interpolationsByName:Map<String, Interpolation> = mapOf(
            "linear" to Interpolation.linear,
            "fade" to Interpolation.fade,
            "pow2" to Interpolation.pow2,
            "pow2In" to Interpolation.pow2In,
            "pow2Out" to Interpolation.pow2Out,
            "pow3" to Interpolation.pow3,
            "pow3In" to Interpolation.pow3In,
            "pow3Out" to Interpolation.pow3Out,
            "pow4" to Interpolation.pow4,
            "pow4In" to Interpolation.pow4In,
            "pow4Out" to Interpolation.pow4Out,
            "pow5" to Interpolation.pow5,
            "pow5In" to Interpolation.pow5In,
            "pow5Out" to Interpolation.pow5Out,
            "sine" to Interpolation.sine,
            "sineIn" to Interpolation.sineIn,
            "sineOut" to Interpolation.sineOut,
            "exp10" to Interpolation.exp10,
            "exp10In" to Interpolation.exp10In,
            "exp10Out" to Interpolation.exp10Out,
            "exp5" to Interpolation.exp5,
            "exp5In" to Interpolation.exp5In,
            "exp5Out" to Interpolation.exp5Out,
            "circle" to Interpolation.circle,
            "circleIn" to Interpolation.circleIn,
            "circleOut" to Interpolation.circleOut,
            "elastic" to Interpolation.elastic,
            "elasticIn" to Interpolation.elasticIn,
            "elasticOut" to Interpolation.elasticOut,
            "swing" to Interpolation.swing,
            "swingIn" to Interpolation.swingIn,
            "swingOut" to Interpolation.swingOut,
            "bounce" to Interpolation.bounce,
            "bounceIn" to Interpolation.bounceIn,
            "bounceOut" to Interpolation.bounceOut)

    val interpolations:List<Interpolation> = interpolationsByName.entries.sortedBy { it.key }.map { it.value }.toList()

    fun random():Interpolation = interpolations[interpolations.size.random()]

}
