package ru.nobirds.rx.utils

interface ImmutablePosition : ImmutableVec2f {

    companion object {
        fun readonly(x: Float, y: Float): ImmutablePosition = ReadonlyPosition(x, y)
    }

}

interface MutablePosition : ImmutablePosition, MutableVec2f {

    override var x: Float
    override var y: Float

    fun position(x: Float, y: Float): MutablePosition = apply {
        this.x = x
        this.y = y
    }

    fun position(position: ImmutableVec2f): MutablePosition = position(position.x, position.y)

    fun copyPosition(): MutablePosition = zero().position(this)

    fun translate(dx: Float, dy: Float): MutablePosition = position(x + dx, y + dy)

    companion object : PoolFactory<MutablePosition> {

        override fun create(): MutablePosition = zero()

        fun position(x: Float = 0f, y: Float = 0f): MutablePosition = Position(x, y)
        fun zero(): MutablePosition = position()
    }

}

internal data class ReadonlyPosition(override val x: Float, override val y: Float) : ImmutablePosition
internal data class Position(override var x: Float, override var y: Float) : MutablePosition