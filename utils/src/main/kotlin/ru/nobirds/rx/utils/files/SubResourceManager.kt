package ru.nobirds.rx.utils.files

internal class SubResourceManager(val parent: ResourceManager, val subName:String) : ResourceManager {

    override fun forRead(name: String): ImmutableResource = parent.forRead(getSubDirectory(name))

    override fun forWrite(name: String): MutableResource = parent.forWrite(getSubDirectory(name))

    override fun exists(name: String): Boolean = parent.exists(getSubDirectory(name))

    private fun getSubDirectory(name: String) = "$subName/$name" // todo: checks and slashes


}