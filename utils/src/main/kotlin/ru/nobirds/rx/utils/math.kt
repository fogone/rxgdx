package ru.nobirds.rx.utils

import com.badlogic.gdx.math.MathUtils
import java.util.Random

fun Float.round():Float = Math.round(this).toFloat()

fun Float.random():Float = this * Random().nextFloat()
fun Int.random():Int = Random().nextInt(this)

fun Float.abs(): Float = Math.abs(this)

fun Float.sqr():Float = this * this
fun Float.sqrt():Float = Math.sqrt(this.toDouble()).toFloat()

@Suppress("UNUSED_PARAMETER")
fun square(v1: ImmutableVec2f, v2: ImmutableVec2f, v3: ImmutableVec2f, v4: ImmutableVec2f):Float {
    val a = v1 distanceTo v2
    val b = v2 distanceTo v3

    return a * b
}

fun square(v1: ImmutableVec2f, v2: ImmutableVec2f, v3: ImmutableVec2f):Float {
    val a = v1 distanceTo v2
    val b = v2 distanceTo v3
    val c = v3 distanceTo v1

    val p = (a + b + c) / 2

    val v = p * (p - a) * (p - b) * (p - c)

    return v.sqrt()
}

fun Float.radiansToDegrees():Float = this * MathUtils.radiansToDegrees
fun Float.degreesToRadians():Float = this * MathUtils.degreesToRadians

fun Float.equalsWithEpsilon(other:Float, epsilon:Float = MathUtils.FLOAT_ROUNDING_ERROR):Boolean = MathUtils.isEqual(this, other, epsilon)

fun Int.toFloatBits():Float = java.lang.Float.intBitsToFloat(this)