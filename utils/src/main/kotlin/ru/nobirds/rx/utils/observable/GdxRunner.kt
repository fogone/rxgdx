package ru.nobirds.rx.utils.observable

import com.badlogic.gdx.Gdx
import ru.nobirds.rx.reactive.Runner
import ru.nobirds.rx.reactive.Runners

object GdxRunner {

    val runner: Runner = Runners.runner { t -> Gdx.app.postRunnable(t) }

}