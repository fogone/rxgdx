package ru.nobirds.rx.utils.files

import com.badlogic.gdx.Files
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import java.io.InputStream
import java.io.OutputStream

class GdxResourceManager() : ResourceManager {

    companion object {
        private val readAccessors = listOf(
                Files::classpath, Files::internal, Files::external
                //, Files::absolute, Files::local // todo: does it need?
        )

        private val writeAccessors = listOf(
                Files::internal, Files::external
        )

    }

    override fun forRead(name: String): ImmutableResource {
        val handle = findReadHandle(name) ?: throw ResourceNotFoundException(name)
        return FileHandleResource(handle)
    }

    override fun forWrite(name: String): MutableResource {
        val handle = findWriteHandle(name) ?: throw ResourceNotFoundException(name)
        return FileHandleResource(handle)
    }

    override fun exists(name: String): Boolean = findReadHandle(name) != null

    private fun findReadHandle(name: String): FileHandle? = readAccessors.asSequence()
            .map { accessor -> accessor(Gdx.files, name) }
            .firstOrNull { it != null && it.exists() }

    private fun findWriteHandle(name: String): FileHandle? = writeAccessors.asSequence()
            .map { accessor -> accessor(Gdx.files, name) }
            .firstOrNull { it != null }
}

internal class FileHandleResource(val handle: FileHandle) : ImmutableResource, MutableResource {

    override val name: String
        get() = handle.path()

    override fun toInputStream(): InputStream = handle.read()

    override fun toOutputStream(append:Boolean): OutputStream = handle.write(append)

    override fun toFileHandle(): FileHandle = handle

}