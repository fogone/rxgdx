package ru.nobirds.rx.utils

import java.util.ArrayDeque
import java.util.ArrayList
import java.util.Deque
import kotlin.reflect.KClass
import kotlin.reflect.full.companionObjectInstance
import kotlin.reflect.full.createInstance

interface PoolFactory<out T : Any> {

    fun create(): T

}

interface Poolable {

    fun reset()

}

interface Pool<T : Any> {

    fun obtain(): T

    fun free(value: T)

}

inline fun <T : Any> Pool<T>.obtain(initializer: T.() -> Unit): T = obtain().apply(initializer)

open class FactoryPool<T : Any>(val capacity: Int = 100, val factory: () -> T) : Pool<T> {

    private val values: Deque<T> = ArrayDeque()

    override fun obtain(): T = if (values.isEmpty()) factory() else values.poll()

    override fun free(value: T) {
        if (values.size < capacity) {
            if (value is Poolable)
                value.reset()
            values.add(value)
        }
    }

}

fun <T : Any> pool(type: KClass<T>, capacity: Int = 100): Pool<T> = FactoryPool(capacity, createPoolFactory(type))

@Suppress("UNCHECKED_CAST")
private fun <T : Any> createPoolFactory(type: KClass<T>): () -> T {
    val companion = type.companionObjectInstance

    return when {
        companion is PoolFactory<*> -> { -> (companion as PoolFactory<T>).create() }
        type.hasNoArgsConstructor() -> { -> type.createInstance() }
        else -> throw IllegalArgumentException("No no-args constructor and no PoolFactory companion object found.")
    }

    /* //todo:  used workaround, see https://youtrack.jetbrains.com/issue/KT-9743
    */

//    return when {
//        companion is PoolFactory<*> -> {
//            val poolFactory = companion as PoolFactory<T>
//            val f = { -> poolFactory.create() }
//            f
//        }
//        type.hasNoArgsConstructor() -> {
//            val f = { -> type.java.newInstance() }
//            f
//        }
//        else -> throw IllegalArgumentException("No no-args constructor and no PoolFactory companion object found.")
//    }
}

private fun <T : Any> KClass<T>.hasNoArgsConstructor(): Boolean = constructors.any { it.parameters.all { it.isOptional } }

internal class ContextPool(capacity: Int = 100) : FactoryPool<PoolContext>(capacity, ::PoolContext)

object Pools {

    private val contexts = ContextPool()
    private val pools = mutableMapOf<KClass<*>, Pool<*>>()

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> get(type: KClass<T>): Pool<T>? = pools[type] as Pool<T>?

    fun <T : Any> register(type: KClass<T>, pool: Pool<T>) {
        require(type !in pools) { "Pool for type $type already registered." }

        pools.put(type, pool)
    }

    fun <T : Any> obtain(type: KClass<T>, initializer: T.() -> Unit = {}): T {
        val pool = get(type) ?: create(type)

        val instance = pool.obtain(initializer)

        return instance
    }

    private fun <T : Any> create(type: KClass<T>): Pool<T> = pool(type).apply {
        register(type, this)
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> free(value: T) {
        (get(value::class) as Pool<T>?)?.free(value)
    }

    fun context(): PoolContext = contexts.obtain()
    fun freeContext(context: PoolContext) {
        contexts.free(context)
    }

    inline fun <R> withContext(block: PoolContext.() -> R): R {
        val context = context()
        try {
            return context.block()
        } finally {
            freeContext(context)
        }
    }

}

inline fun <R> poolable(block: PoolContext.() -> R): R = Pools.withContext(block)

class PoolContext internal constructor() : Poolable {

    private val instances = ArrayList<Any>()

    inline fun <reified T : Any> obtain(): T = obtain(T::class)

    inline fun <reified T : Any> obtain(crossinline initializer: T.() -> Unit): T = obtain(T::class).apply(initializer)

    fun <T : Any> obtain(type: KClass<T>): T = Pools.obtain(type).apply {
        register(this)
    }

    fun register(instance: Any) {
        instances.add(instance)
    }

    override fun reset() {
        instances.forEach { Pools.free(it) }
        instances.clear()
    }

}

