package ru.nobirds.rx.utils

import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.ReactiveStreamProducer
import ru.nobirds.rx.reactive.reactiveStreamProducer
import ru.nobirds.rx.reactive.subscribe

fun <S:Any> stateManager(builder: StateManager<S>.()->Unit): StateManager<S> = StateManager<S>().apply(builder)

class StateManager<S:Any> {

    private class Rules<S> {

        val fromAny: ReactiveStreamProducer<S> by lazy { reactiveStreamProducer<S>() }
        val fromState:MutableMap<S, ReactiveStreamProducer<S>> by lazy { hashMapOf<S, ReactiveStreamProducer<S>>() }

    }

    private val toStates = hashMapOf<S, Rules<S>>()

    val state:S?
        get() = currentState

    private var currentState:S? = null

    fun toState(state:S): ReactiveStream<S> = toStates.getOrPut(state) { Rules() }.fromAny

    fun toState(from:S, to:S): ReactiveStream<S> = toStates.getOrPut(to) { Rules() }.fromState.getOrPut(from) { reactiveStreamProducer() }

    fun switchTo(state:S) {
        if(currentState == state)
            return

        val consumer = findStateEdge(currentState, state) ?:
                throw IllegalStateException("No way to mutate from $currentState to $state.")

        currentState = state

        consumer.onNext(state)
    }

    private fun findStateEdge(from: S?, to: S): ReactiveStreamProducer<S>? {
        val rules = toStates[to] ?: return null

        return if(from != null)
            rules.fromState[from] ?: rules.fromAny
        else
            rules.fromAny
    }

}

inline fun <S:Any> StateManager<S>.onChangeToState(state:S, crossinline handler:()->Unit): ReactiveStream<S> {
    val edge = toState(state)

    edge.subscribe { handler() }

    return edge
}

inline fun <S:Any> StateManager<S>.onChangeToState(from:S, to:S, crossinline handler:()->Unit): ReactiveStream<S> {
    val edge = toState(from, to)

    edge.subscribe { handler() }

    return edge
}
