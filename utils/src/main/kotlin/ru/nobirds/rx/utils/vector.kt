package ru.nobirds.rx.utils

import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3

fun vec2(x:Float = 0f, y:Float = 0f):MutableVec2f = MutableVec2f.vector(x, y)
fun immutableVec2(x:Float, y:Float):ImmutableVec2f = ImmutableVec2f.readonly(x, y)
fun vec3(x:Float = 0f, y:Float = 0f, z:Float = 0f): Vector3 = Vector3(x, y, z)

interface ImmutableVec2f {

    val x:Float
    val y:Float

    companion object {
        fun readonly(x:Float, y: Float):ImmutableVec2f = ReadonlyVec2f(x, y)

        fun fromLengthAndAngle(length: Float, degrees: Float):ImmutableVec2f {
            val radians = degrees.degreesToRadians()

            val x = length * MathUtils.cos(radians)
            val y = length * MathUtils.sin(radians)

            return readonly(x, y)
        }

        val zero:ImmutableVec2f = readonly(0f, 0f)
    }

}

interface MutableVec2f : ImmutableVec2f {

    override var x:Float
    override var y:Float

    companion object : PoolFactory<MutableVec2f> {

        override fun create(): MutableVec2f = zero()

        fun vector(x:Float, y: Float):MutableVec2f = Vec2f(x, y)
        fun zero():MutableVec2f = vector(0f, 0f)

        fun wrap(vector: Vector2):MutableVec2f = object: MutableVec2f {
            override var x: Float
                get() = vector.x
                set(value) {
                    vector.x = value
                }
            override var y: Float
                get() = vector.y
                set(value) {
                    vector.y = value
                }
        }
    }

}

fun Vector2.asVec2f():MutableVec2f = MutableVec2f.wrap(this)
fun ImmutableVec2f.asVector2():Vector2 = Vector2(x, y)

internal data class ReadonlyVec2f(override val x:Float, override val y: Float) : ImmutableVec2f
internal data class Vec2f(override var x:Float, override var y: Float) : MutableVec2f

fun MutableVec2f.copy():MutableVec2f = MutableVec2f.vector(x, y)

fun MutableVec2f.set(x: Float, y:Float):MutableVec2f = apply {
    this.x = x
    this.y = y
}

fun MutableVec2f.set(vec2: ImmutableVec2f):MutableVec2f = apply {
    this.x = vec2.x
    this.y = vec2.y
}

fun MutableVec2f.set(vector: Vector2):MutableVec2f = apply {
    this.x = vector.x
    this.y = vector.y
}

inline fun MutableVec2f.operate(x:Float, y:Float, operation:(Float, Float)->Float): MutableVec2f
        = set(operation(this.x,x), operation(this.y,y))

fun MutableVec2f.plus(x:Float, y:Float): MutableVec2f = operate(x, y, Float::plus)
operator fun MutableVec2f.plus(value:Float): MutableVec2f = plus(value, value)
operator fun MutableVec2f.plus(vec2: ImmutableVec2f): MutableVec2f = plus(vec2.x, vec2.y)

fun MutableVec2f.minus(x:Float, y:Float): MutableVec2f = operate(x, y, Float::minus)
operator fun MutableVec2f.minus(value:Float): MutableVec2f = minus(value, value)
operator fun MutableVec2f.minus(vec2: ImmutableVec2f): MutableVec2f = minus(vec2.x, vec2.y)

fun MutableVec2f.times(x:Float, y:Float): MutableVec2f = operate(x, y, Float::times)
operator fun MutableVec2f.times(value:Float): MutableVec2f = times(value, value)
operator fun MutableVec2f.times(vec2: ImmutableVec2f): MutableVec2f = times(vec2.x, vec2.y)

fun MutableVec2f.div(x:Float, y:Float): MutableVec2f = operate(x, y, Float::div)
operator fun MutableVec2f.div(vec2: ImmutableVec2f): MutableVec2f = div(vec2.x, vec2.y)


inline fun ImmutableVec2f.operate(x:Float, y:Float, operation:(Float, Float)->Float): ImmutableVec2f
        = ImmutableVec2f.readonly(operation(this.x,x), operation(this.y,y))

fun ImmutableVec2f.plus(x:Float,y: Float): ImmutableVec2f = operate(x, y, Float::plus)

operator fun ImmutableVec2f.plus(vec2: ImmutableVec2f): ImmutableVec2f = plus(vec2.x, vec2.y)

operator fun ImmutableVec2f.plus(value: Float): ImmutableVec2f = plus(value, value)

fun ImmutableVec2f.minus(x: Float, y: Float): ImmutableVec2f = operate(x, y, Float::minus)

operator fun ImmutableVec2f.minus(vec2: ImmutableVec2f): ImmutableVec2f = minus(vec2.x, vec2.y)
operator fun ImmutableVec2f.minus(value: Float): ImmutableVec2f = minus(value, value)

operator fun ImmutableVec2f.unaryMinus(): ImmutableVec2f = times(-1f)

fun ImmutableVec2f.times(x: Float, y: Float): ImmutableVec2f = operate(x, y, Float::times)
operator fun ImmutableVec2f.times(value: Float): ImmutableVec2f = times(value, value)
operator fun ImmutableVec2f.times(vec2: ImmutableVec2f): ImmutableVec2f = times(vec2.x, vec2.y)

fun ImmutableVec2f.div(x: Float, y: Float): ImmutableVec2f = operate(x, y, Float::div)
operator fun ImmutableVec2f.div(value: Float): ImmutableVec2f = div(value, value)
operator fun ImmutableVec2f.div(vec2: ImmutableVec2f): ImmutableVec2f = div(vec2.x, vec2.y)

fun ImmutableVec2f.dot(x: Float, y: Float): Float = x * this.x + y * this.y
infix fun ImmutableVec2f.dot(vec2: ImmutableVec2f): Float = dot(vec2.x, vec2.y)

fun ImmutableVec2f.cross(x: Float, y: Float): Float = this.x * y - this.y * x
infix fun ImmutableVec2f.x(vec2: ImmutableVec2f): Float = cross(vec2.x, vec2.y)

fun ImmutableVec2f.distanceTo(x:Float, y: Float): Float = distance2To(x, y).sqrt()

fun ImmutableVec2f.distance2To(x:Float, y: Float): Float {
    val dx = x - this.x
    val dy = y - this.y

    return dx.sqr() + dy.sqr()
}

infix fun ImmutableVec2f.distanceTo(vec2: ImmutableVec2f): Float = distanceTo(vec2.x, vec2.y)
infix fun ImmutableVec2f.distance2To(vec2: ImmutableVec2f): Float = distance2To(vec2.x, vec2.y)

internal inline fun ImmutableVec2f.limit(length: Float, condition:(Float, Float)->Boolean): ImmutableVec2f {
    val length2 = length2
    val limitLength2 = length.sqr()

    return if (condition(length2, limitLength2)) this * (length2 / limitLength2).sqrt() else this
}

infix fun ImmutableVec2f.max(maxLength: Float): ImmutableVec2f = limit(maxLength) { v1, v2 -> v1 > v2 }
infix fun ImmutableVec2f.min(minLength: Float): ImmutableVec2f = limit(minLength) { v1, v2 -> v1 < v2 }

fun ImmutableVec2f.clamp(minLength: Float, maxLength: Float): ImmutableVec2f {
    val length2 = length2
    val minLength2 = minLength.sqr()
    val maxLength2 = maxLength.sqr()

    return when {
        length2 > maxLength2 -> this * (length2 / maxLength2).sqrt()
        length2 < minLength2 -> this * (length2 / minLength2).sqrt()
        else -> this
    }
}

val ImmutableVec2f.length:Float
    get() = length2.sqrt()

val ImmutableVec2f.length2:Float
        get() = x.sqr() + y.sqr()

fun ImmutableVec2f.withLength(length: Float):ImmutableVec2f = withLength2(length.sqr())
fun ImmutableVec2f.withLength2(length2: Float):ImmutableVec2f = this * (length2/this.length2).sqrt()

val ImmutableVec2f.angle:Float
    get() {
        val angle = MathUtils.atan2(y, x).radiansToDegrees()
        return if (angle >= 0) angle else angle + 360f
    }

fun ImmutableVec2f.angle(other:ImmutableVec2f):Float =
        MathUtils.atan2(this x other, this dot other).radiansToDegrees()

fun ImmutableVec2f.withAngle(degrees:Float):ImmutableVec2f =
        ImmutableVec2f.fromLengthAndAngle(length, degrees)

fun ImmutableVec2f.withRotation(degrees:Float):ImmutableVec2f {
    val radians = degrees.degreesToRadians()

    val cos = MathUtils.cos(radians)
    val sin = MathUtils.sin(radians)

    return ImmutableVec2f.readonly(x * cos - y * sin, x * sin + y * cos)
}

fun ImmutableVec2f.lerp(target: ImmutableVec2f, progress: Float): ImmutableVec2f {
    val invertedProgress = 1.0f - progress

    val x = x * invertedProgress + target.x * progress
    val y = y * invertedProgress + target.y * progress

    return ImmutableVec2f.readonly(x, y)
}

operator fun ImmutableVec2f.compareTo(vec2: ImmutableVec2f):Int = length2.compareTo(vec2.length2)

fun ImmutableVec2f.equals(vec2: ImmutableVec2f):Boolean = x.equalsWithEpsilon(vec2.x) && y.equalsWithEpsilon(vec2.y)

fun PoolContext.obtainVec2(x:Float, y:Float):MutableVec2f = obtain { set(x, y) }
fun PoolContext.obtainVec3(x:Float, y:Float, z:Float): Vector3 = obtain { set(x, y, z) }

