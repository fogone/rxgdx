package ru.nobirds.rx.utils

import com.badlogic.gdx.utils.BufferUtils
import java.nio.ByteBuffer

object Sizes {

    val short: Int = 2
    val float:Int = 4
    val int:Int = 4

}

inline fun buffer(count:Int, builder: ByteBuffer.()->Unit): ByteBuffer {
    val buffer = BufferUtils.newByteBuffer(count)
    buffer.builder()
    buffer.flip()
    return buffer
}

fun buffer(vararg numbers:Float): ByteBuffer = buffer(numbers.size * Sizes.float) {
    put(*numbers)
}

fun emptyBuffer():ByteBuffer = ByteBuffer.allocateDirect(0)

fun FloatArray.toBuffer(): ByteBuffer = buffer(*this)

fun buffer(vararg numbers:Short): ByteBuffer = buffer(numbers.size * Sizes.short) {
    put(*numbers)
}

fun ShortArray.toBuffer(): ByteBuffer = buffer(*this)

fun ByteBuffer.put(vararg numbers:Float):ByteBuffer = apply {
    for (number in numbers) {
        putFloat(number)
    }
}

fun ByteBuffer.put(vararg numbers:Short):ByteBuffer = apply {
    for (number in numbers) {
        putShort(number)
    }
}
