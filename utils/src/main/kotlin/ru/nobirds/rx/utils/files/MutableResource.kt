package ru.nobirds.rx.utils.files

import java.io.OutputStream

interface MutableResource : Resource {

    fun toOutputStream(append:Boolean = false): OutputStream

}