package ru.nobirds.rx.utils.observable

import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal

class ObservableSpacing(top:Float = 0f, right:Float = 0f, bottom:Float = 0f, left:Float = 0f) : ru.nobirds.rx.utils.MutableSpacing {

    val topProperty: Property<Float> = property(top)
    override var top:Float by topProperty

    val bottomProperty: Property<Float> = property(bottom)
    override var bottom:Float by bottomProperty

    val rightProperty: Property<Float> = property(right)
    override var right:Float by rightProperty

    val leftProperty: Property<Float> = property(left)
    override var left:Float by leftProperty

    val changed: ReactiveStream<Signal> = ru.nobirds.rx.property.changed(topProperty, bottomProperty, rightProperty, leftProperty)

}