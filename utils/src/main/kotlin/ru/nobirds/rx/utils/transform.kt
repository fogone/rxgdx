package ru.nobirds.rx.utils

import com.badlogic.gdx.math.Affine2
import com.badlogic.gdx.math.Matrix4


interface ImmutableTransform : ImmutablePosition {

    val scaleX:Float
    val scaleY:Float

    val rotation:Float

    val pivotX:Float
    val pivotY:Float

    fun toMatrix(): Matrix4

    fun toAffine(): Affine2

}

interface MutableTransform : ImmutableTransform, MutablePosition {

    override var scaleX:Float
    override var scaleY:Float

    override var rotation:Float

    override var pivotX:Float
    override var pivotY:Float

}


fun MutableTransform.rotate(rotation: Float):MutableTransform = apply {
    this.rotation += rotation
}

fun MutableTransform.scale(value: Float):MutableTransform = scale(value, value)

fun MutableTransform.scale(x: Float, y:Float):MutableTransform = apply {
    this.scaleX += x
    this.scaleY += y
}

fun MutableTransform.scaleTo(x: Float, y:Float) = apply {
    this.scaleX = x
    this.scaleY = y
}

fun MutableTransform.scaleTo(value:Float) = scaleTo(value, value)

