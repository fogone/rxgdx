package ru.nobirds.rx.utils

import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

infix fun <T:Any, C:Any> T.instanceOf(type: KClass<C>):Boolean = this::class.isSubclassOf(type)
