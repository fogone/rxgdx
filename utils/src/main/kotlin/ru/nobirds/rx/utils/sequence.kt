package ru.nobirds.rx.utils

inline fun <T> MutableCollection<T>.removeAll(condition:(T)->Boolean):List<T> {
    val result = arrayListOf<T>()

    val iterator = iterator()
    while (iterator.hasNext()) {
        val next = iterator.next()
        if (condition(next)) {
            iterator.remove()
            result.add(next)
        }
    }

    return result
}

fun <T> sequence(iterator:()->Iterator<T>):Sequence<T> = object: Sequence<T> {
    override fun iterator(): Iterator<T> = iterator()
}

