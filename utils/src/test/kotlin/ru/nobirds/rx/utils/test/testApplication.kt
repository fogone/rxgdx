package ru.nobirds.rx.utils.test

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.backends.lwjgl.LwjglApplication

class TestApplicationAdapter(val block:()->Unit) : ApplicationAdapter() {

    val storedExceptions:MutableList<Throwable> = arrayListOf()

    override fun create() {
        try {
            block()
        } catch(e: Throwable) {
            storedExceptions.add(e)
            throw e
        }
    }

}

class TestLwjglApplication(block:()->Unit) : LwjglApplication(TestApplicationAdapter(block)) {

    fun join() {
        mainLoopThread.join()
        val exceptions = (listener as TestApplicationAdapter).storedExceptions
        if (exceptions.isNotEmpty()) {
            throw exceptions.first()
        }
        running = false
    }

}

fun testApplication(block: () -> Unit) {
    TestLwjglApplication(block).join()
}