package ru.nobirds.rx.property

fun ObservableValue<String>.asInt():ObservableValue<Int> = map { it.toInt() }
fun ObservableValue<String>.asLong():ObservableValue<Long> = map { it.toLong() }

fun ObservableValue<*>.asString():ObservableValue<String> = map { it.toString() }

