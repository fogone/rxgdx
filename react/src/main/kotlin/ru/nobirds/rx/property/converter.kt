package ru.nobirds.rx.property


interface Converter<in T, out R> {

    fun convert(value:T):R

}

interface TwoWayConverter<T, R> : Converter<T, R> {

    fun back():Converter<R, T>

}

fun <T, R> TwoWayConverter<T, R>.invert():TwoWayConverter<R, T> = object: TwoWayConverter<R, T> {
    override fun back(): Converter<T, R> = this@invert
    override fun convert(value: R): T = this@invert.back().convert(value)
}


object Converters {

    private val identity: Converter<*, *> = converter<Any?, Any?> { it }

    @Suppress("UNCHECKED_CAST")
    fun <T> identity():Converter<T, T> = identity as Converter<T, T>

    val signal: Converter<Any, Boolean> = converter { true }

    val stringToInt: Converter<String, Int> = converter { it.toInt() }
    val stringToFloat: Converter<String, Float> = converter { it.toFloat() }
    val anyToString: Converter<Any, String> = converter { it.toString() }

}

object TwoWayConverters {

    private val identity: Converter<*, *> = converter<Any?, Any?>(Converters.identity(), Converters.identity())

    @Suppress("UNCHECKED_CAST")
    fun <T> identity():TwoWayConverter<T, T> = identity as TwoWayConverter<T, T>

    val stringToInt: TwoWayConverter<String, Int> = converter(Converters.stringToInt, Converters.anyToString)
    val intToString: TwoWayConverter<Int, String> = stringToInt.invert()

    val stringToFloat: TwoWayConverter<String, Float> = converter(Converters.stringToFloat, Converters.anyToString)
    val floatToString: TwoWayConverter<Float, String> = stringToFloat.invert()

}

fun <T, R> converter(converter:(T)->R):Converter<T, R> = object: Converter<T, R> {
    override fun convert(value: T): R = converter(value)
}

fun <T, R> converter(convert:Converter<T, R>, back:Converter<R, T>):TwoWayConverter<T, R> = object : TwoWayConverter<T, R> {
    override fun back(): Converter<R, T> = back
    override fun convert(value: T): R = convert.convert(value)
}