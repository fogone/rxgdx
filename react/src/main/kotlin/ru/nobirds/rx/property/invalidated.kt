package ru.nobirds.rx.property

import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal

class InvalidatedProperty(initial:Boolean = true) : Property<Boolean>(initial) {

    fun invalidate() {
        if(!value)
            this.value = true
    }

    fun makeValid() {
        if(value)
            this.value = false
    }

    inline fun with(block:()->Unit) {
        if (this.value) {
            block()
            makeValid()
        }
    }

}

fun invalidated(initial:Boolean = true): InvalidatedProperty = InvalidatedProperty(initial)

inline fun invalidated(initial:Boolean = true, initializer: InvalidatedProperty.()->Unit): InvalidatedProperty =
        InvalidatedProperty(initial).apply(initializer)

fun invalidated(initial:Boolean = true, vararg dependencies: Invalidatable): InvalidatedProperty =
        invalidated(initial, changed(*dependencies))

fun invalidated(initial:Boolean = true, dependencies: ReactiveStream<Signal>): InvalidatedProperty = invalidated(initial) {
    bindOneWaySignal(dependencies)
}
