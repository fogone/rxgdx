package ru.nobirds.rx.property

import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.reactive.asSignal
import ru.nobirds.rx.reactive.combinedReactiveStream
import ru.nobirds.rx.reactive.subscribe
import ru.nobirds.rx.reactive.subscription
import kotlin.reflect.KProperty

interface Invalidatable {

    val invalidated: ReactiveStream<Signal>

}

class ValueChangedEvent<T>(val property: ObservableValue<T>, val old:T?, val new:T)

interface ObservableValue<T> : Invalidatable {

    val value:T

    val changed: ReactiveStream<ValueChangedEvent<T>>

    override val invalidated: ReactiveStream<Signal>
        get() = changed.asSignal()

}

operator fun <T> ObservableValue<T>.getValue(thisRef: Any, property: KProperty<*>): T = value

fun <T> ObservableValue<T>.onChange(handler:(ObservableValue<T>, T?, T)->Unit): Subscription
        = changed.subscribe { handler(it.property, it.old, it.new) }

interface MutableObservableValue<T> : ObservableValue<T> {

    override var value:T

}

fun <T> MutableObservableValue<T>.bindOneWay(observableValue:ObservableValue<T>): Subscription
        = bindOneWay(observableValue, TwoWayConverters.identity())

fun <T, R> MutableObservableValue<T>.bindOneWay(observableValue:ObservableValue<R>, converter: Converter<R, T>): Subscription {
    value = converter.convert(observableValue.value)
    return observableValue.onChange { _, _, new -> value = converter.convert(new) }
}

fun <T> MutableObservableValue<T>.bindTwoWay(mutableObservableValue: MutableObservableValue<T>): Subscription
        = subscription(bindOneWay(mutableObservableValue), mutableObservableValue.bindOneWay(this))

fun <T, R> MutableObservableValue<T>.bindTwoWay(mutableObservableValue: MutableObservableValue<R>, converter: TwoWayConverter<R, T>): Subscription
        = subscription(bindOneWay(mutableObservableValue, converter), mutableObservableValue.bindOneWay(this, converter.back()))

fun changed(vararg observableValues: Invalidatable): ReactiveStream<Signal> = combinedReactiveStream(observableValues.map { it.invalidated })
