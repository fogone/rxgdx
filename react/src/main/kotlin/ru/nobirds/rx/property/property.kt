package ru.nobirds.rx.property

import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.reactiveStreamProducer
import kotlin.properties.Delegates
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

open class Property<T>(value:T) :
        MutableObservableValue<T>,
        ReadWriteProperty<Any, T> {

    private val changedStream = reactiveStreamProducer<ValueChangedEvent<T>>()

    override val changed: ReactiveStream<ValueChangedEvent<T>>
        get() = changedStream

    override var value:T by Delegates.observable(value) { _, o, n ->
        if(o != n) {
            changedStream.onNext(ValueChangedEvent(this, o, n))
        }
    }

    override fun toString(): String = value?.toString() ?: "null"

    override fun getValue(thisRef: Any, property: KProperty<*>): T = this.value
    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) { this.value = value }

}

fun <T>  property(value:T): Property<T> = Property(value)
fun <T>  property(value:T, initializer: Property<T>.()->Unit): Property<T> = Property(value).apply(initializer)
