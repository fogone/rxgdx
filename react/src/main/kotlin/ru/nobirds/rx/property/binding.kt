package ru.nobirds.rx.property

import ru.nobirds.rx.reactive.ReactiveStream
import ru.nobirds.rx.reactive.Signal
import ru.nobirds.rx.reactive.Subscription
import ru.nobirds.rx.reactive.combinedReactiveStream
import ru.nobirds.rx.reactive.reactiveStreamProducer
import ru.nobirds.rx.reactive.subscribe

fun <T> binding(dependencies: ReactiveStream<Signal>, computer:()->T):Binding<T> = Binding(dependencies, computer)
fun <T> binding(vararg dependencies: ObservableValue<*>, computer:()->T):Binding<T> = binding(combinedReactiveStream(dependencies.map { it.invalidated }), computer)

fun <T, V, R> ObservableValue<T>.operate(other:ObservableValue<V>, mapper:(T, V)->R):ObservableValue<R> = binding(this, other) { mapper(this.value, other.value) }

fun <T, R> ObservableValue<T>.map(mapper:(T)->R):ObservableValue<R> = binding(this) { mapper(this.value) }

class Binding<T>(dependencies: ReactiveStream<Signal>, val computer:()->T) : ObservableValue<T> {

    private val changedStream = reactiveStreamProducer<ValueChangedEvent<T>>()

    override var value: T = computer()
        private set

    private fun computeValue() {
        val oldValue = value
        val newValue = computer()

        if (newValue != oldValue) {
            this.value = newValue
            changedStream.onNext(ValueChangedEvent(this, oldValue, newValue))
        }
    }

    init {
        dependencies.subscribe {
            computeValue()
        }
    }

    override val changed: ReactiveStream<ValueChangedEvent<T>>
        get() = changedStream

}

fun MutableObservableValue<Boolean>.bindOneWaySignal(value: ObservableValue<out Any>):Subscription = bindOneWay(value, Converters.signal)

fun MutableObservableValue<Boolean>.bindOneWaySignal(trigger: ReactiveStream<Signal>, value:Boolean = true):Subscription {
    return trigger.subscribe {
        this@bindOneWaySignal.value = value
    }
}

