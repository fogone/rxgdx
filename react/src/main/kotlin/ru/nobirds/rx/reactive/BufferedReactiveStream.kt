package ru.nobirds.rx.reactive

import java.util.concurrent.CopyOnWriteArrayList

fun <T> ReactiveStream<T>.buffered(): ReactiveStream<T> = BufferedReactiveStream(this)

class BufferedReactiveStream<T>(val reactiveStream: ReactiveStream<T>) : SubscriptionBasedReactiveStream<T>() {

    private val buffer = CopyOnWriteArrayList<T>()

    private val subscription = subscribe()

    private fun subscribe(): Subscription {
        return reactiveStream.subscribeFor {
            next {
                buffer.add(it)
                for (subscriber in subscribers) {
                    subscriber.onNext(it)
                }
            }
            error { e ->
                subscribers.forEach { it.onError(e) }
            }
            complete {
                subscribers.forEach { it.onComplete() }
            }
        }
    }

    override fun onSubscribe(subscriber: Subscriber<T>) {
        for (item in buffer) {
            subscriber.onNext(item)
        }
    }
}