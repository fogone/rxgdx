package ru.nobirds.rx.reactive

fun <T> reactiveStream(values:Iterable<T>): ReactiveStream<T> = PredefinedSequenceReactiveStream(values)
fun <T> reactiveStream(vararg values:T): ReactiveStream<T> = reactiveStream(values.asIterable())

class PredefinedSequenceReactiveStream<out T>(val values:Iterable<T>) : ReactiveStream<T> {
    override fun subscribe(subscriber: Subscriber<T>): Subscription {
        for (value in values) {
            subscriber.onNext(value)
        }

        subscriber.onComplete()

        return Subscription.nothing
    }
}