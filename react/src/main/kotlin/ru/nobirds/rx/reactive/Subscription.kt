package ru.nobirds.rx.reactive

interface Subscription {

    companion object {
        val nothing = object : Subscription {
            override fun unsubscribe() {
            }
        }
    }

    fun unsubscribe()

}

fun subscription(vararg subscriptions: Subscription): Subscription = subscription(subscriptions.asIterable())

fun subscription(subscriptions: Iterable<Subscription>): Subscription = object : Subscription {
    override fun unsubscribe() {
        subscriptions.forEach(Subscription::unsubscribe)
    }
}

fun subscription(action:()->Unit): Subscription = object : Subscription {
    override fun unsubscribe() = action()
}
