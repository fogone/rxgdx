package ru.nobirds.rx.reactive

import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

fun <T> ReactiveStream<T>.filter(predicate:(T)->Boolean): ReactiveStream<T> = FilteredReactiveStream(this, predicate)

@Suppress("UNCHECKED_CAST")
fun <R:Any> ReactiveStream<Any>.filterIs(type: KClass<R>): ReactiveStream<R> = filter { it::class.isSubclassOf(type) }.map { it as R }

inline fun <reified R:Any> ReactiveStream<Any>.filterIs(): ReactiveStream<R> = filter { it is R }.map { it as R }

fun <T> ReactiveStream<T>.filter(runner: Runner, predicate:(T)->Boolean): ReactiveStream<T> = runAt(runner).filter(predicate)

class FilteredReactiveStream<T>(val reactiveStream: ReactiveStream<T>, val predicate: (T) -> Boolean) : ReactiveStream<T> {

    override fun subscribe(subscriber: Subscriber<T>): Subscription = reactiveStream.subscribe(subscriber) { value ->
        try {
            if(predicate(value))
                subscriber.onNext(value)

        } catch(e:Throwable) {
            subscriber.onError(e)
        }
    }

}