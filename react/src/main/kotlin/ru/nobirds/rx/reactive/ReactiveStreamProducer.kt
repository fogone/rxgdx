package ru.nobirds.rx.reactive

interface ReactiveStreamProducer<T> : ReactiveStream<T>, Subscriber<T>

internal class ReactiveStreamProducerImpl<T>() : SubscriptionBasedReactiveStream<T>(), ReactiveStreamProducer<T> {

    override fun onNext(value: T) {
        for (subscriber in subscribers) {
            try {
                subscriber.onNext(value)
            } catch(e: Exception) {
                subscriber.onError(e)
            }
        }
    }

    override fun onComplete() {
        for (subscriber in subscribers) {
            subscriber.onComplete()
        }
    }

    override fun onError(exception: Throwable) {
        for (subscriber in subscribers) {
            subscriber.onError(exception)
        }
    }

}

fun <T> reactiveStreamProducer(): ReactiveStreamProducer<T> = ReactiveStreamProducerImpl()

