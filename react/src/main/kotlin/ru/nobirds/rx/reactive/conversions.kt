package ru.nobirds.rx.reactive

fun ReactiveStream<String>.mapToInt():ReactiveStream<Int> = map { it.toInt() }
fun ReactiveStream<String>.mapToLong():ReactiveStream<Long> = map { it.toLong() }
fun ReactiveStream<Any>.mapToString():ReactiveStream<String> = map { it.toString() }
