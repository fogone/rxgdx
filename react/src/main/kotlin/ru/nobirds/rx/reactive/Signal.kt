package ru.nobirds.rx.reactive

object Signal {}

fun ReactiveStream<*>.asSignal(): ReactiveStream<Signal> = map { Signal }