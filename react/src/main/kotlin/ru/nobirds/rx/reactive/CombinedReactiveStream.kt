package ru.nobirds.rx.reactive

operator fun <T> ReactiveStream<T>.plus(other: ReactiveStream<T>): ReactiveStream<T> = combinedReactiveStream(this, other)

fun <T> combinedReactiveStream(streams:Iterable<ReactiveStream<T>>): ReactiveStream<T> = CombinedReactiveStream(streams)
fun <T> combinedReactiveStream(vararg streams: ReactiveStream<T>): ReactiveStream<T> = combinedReactiveStream(streams.asIterable())

class CombinedReactiveStream<out T>(val streams:Iterable<ReactiveStream<T>>) : ReactiveStream<T> {
    override fun subscribe(subscriber: Subscriber<T>): Subscription {
        return subscription(streams.map { it.subscribe(subscriber) })
    }
}

fun <T, R> ReactiveStream<T>.flatMap(accessor:(T)-> ReactiveStream<R>): ReactiveStream<R> = FlattenReactiveStream(this, accessor)

class FlattenReactiveStream<T, out R>(val reactiveStream: ReactiveStream<T>, val accessor: (T) -> ReactiveStream<R>) : ReactiveStream<R> {

    override fun subscribe(subscriber: Subscriber<R>): Subscription {
        val subscriptions = arrayListOf<Subscription>()

        val subscription = reactiveStream.subscribe(subscriber) {
            subscriptions.add(accessor(it).subscribe(subscriber))
        }

        return subscription {
            subscription.unsubscribe()
            subscriptions.forEach(Subscription::unsubscribe)
        }
    }

}
