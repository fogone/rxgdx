package ru.nobirds.rx.reactive

fun <T> buildSubscriber(builder: SubscriberBuilder<T>.()->Unit): Subscriber<T> {
    val subscriberBuilder = SubscriberBuilder<T>()
    subscriberBuilder.builder()
    return subscriberBuilder.build()
}

fun <T, R> Subscriber<T>.withNext(onNext:(R)->Unit): Subscriber<R> = OnNextSubscriber(this, onNext)
internal class OnNextSubscriber<T, R>(val subscriber: Subscriber<T>, val handler: (R) -> Unit) : Subscriber<R> {
    override fun onComplete() {
        subscriber.onComplete()
    }

    override fun onError(exception: Throwable) {
        subscriber.onError(exception)
    }

    override fun onNext(value: R) = handler(value)
}

fun <T> Subscriber<T>.withSuccess(onSuccess: () -> Unit): Subscriber<T> = OnCompleteSubscriber(this, onSuccess)
internal class OnCompleteSubscriber<T>(subscriber: Subscriber<T>, val handler: () -> Unit) : Subscriber<T> by subscriber {
    override fun onComplete() = handler()
}

fun <T> Subscriber<T>.withError(onError: (Throwable) -> Unit): Subscriber<T> = OnErrorSubscriber(this, onError)
internal class OnErrorSubscriber<T>(subscriber: Subscriber<T>, val handler: (Throwable) -> Unit) : Subscriber<T> by subscriber {
    override fun onError(exception: Throwable) = handler(exception)
}

class SubscriberBuilder<T> {

    private var onNext:(T)->Unit = {}
    private var onComplete:()->Unit = {}
    private var onError:(Throwable)->Unit = { throw it }

    fun next(onNext:(T)->Unit): SubscriberBuilder<T> = apply {
        this.onNext = onNext
    }

    fun complete(onSuccess:()->Unit): SubscriberBuilder<T> = apply {
        this.onComplete = onSuccess
    }

    fun error(onError:(Throwable)->Unit): SubscriberBuilder<T> = apply {
        this.onError = onError
    }

    fun build(): Subscriber<T> = object : Subscriber<T> {
        override fun onNext(value: T) = this@SubscriberBuilder.onNext(value)
        override fun onComplete() = this@SubscriberBuilder.onComplete()
        override fun onError(exception: Throwable) = this@SubscriberBuilder.onError(exception)
    }

}