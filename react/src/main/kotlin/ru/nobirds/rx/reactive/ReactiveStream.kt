package ru.nobirds.rx.reactive

import java.util.concurrent.CopyOnWriteArrayList

interface ReactiveStream<out T> {

    fun subscribe(subscriber: Subscriber<T>): Subscription

}

fun <T> ReactiveStream<T>.subscribe(onNext:(T)->Unit): Subscription = subscribe(subscriber(onNext))
fun <T> ReactiveStream<T>.subscribeFor(builder: SubscriberBuilder<T>.() -> Unit): Subscription = subscribe(buildSubscriber(builder))
fun <T, R> ReactiveStream<T>.subscribe(subscriber: Subscriber<R>, onNext:(T)->Unit): Subscription = subscribe(subscriber.withNext(onNext))

fun <T> reactiveStream(onSubscribe:(Subscriber<T>)->Unit): ReactiveStream<T> = object: SubscriptionBasedReactiveStream<T>() {
    override fun onSubscribe(subscriber: Subscriber<T>) {
        onSubscribe(subscriber)
    }
}

@Suppress("UNCHECKED_CAST")
fun <T> emptyReactiveStream(): ReactiveStream<T> = ReactiveStreams.EMPTY as ReactiveStream<T>

object ReactiveStreams {
    val EMPTY: ReactiveStream<Unit> = create {
        it.onComplete()
        Subscription.nothing
    }

    fun <T> create(subscribe:(Subscriber<T>)->Subscription): ReactiveStream<T> = object: ReactiveStream<T> {
        override fun subscribe(subscriber: Subscriber<T>): Subscription = subscribe(subscriber)
    }

}

abstract class SubscriptionBasedReactiveStream<T>() : ReactiveStream<T> {

    private val subscribersImpl = CopyOnWriteArrayList<Subscriber<T>>()

    val subscribers:Iterable<Subscriber<T>>
        get() = subscribersImpl

    override final fun subscribe(subscriber: Subscriber<T>): Subscription {
        subscribersImpl.add(subscriber)

        onSubscribe(subscriber)

        return subscription {
            subscribersImpl.remove(subscriber)
        }
    }

    protected open fun onSubscribe(subscriber: Subscriber<T>) {
    }

}
