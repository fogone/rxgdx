package ru.nobirds.rx.reactive

fun <T, R> ReactiveStream<T>.map(transformer:(T)->R): ReactiveStream<R> = TransformableReactiveStream(this, transformer)
fun <T, R> ReactiveStream<T>.map(runner: Runner, transformer:(T)->R): ReactiveStream<R> = runAt(runner).map(transformer)

class TransformableReactiveStream<T, R>(val reactiveStream: ReactiveStream<T>, val transformer: (T) -> R) : ReactiveStream<R> {

    override fun subscribe(subscriber: Subscriber<R>): Subscription = reactiveStream.subscribe(subscriber) { value ->
        try {
            subscriber.onNext(transformer(value))
        } catch(e: Throwable) {
            subscriber.onError(e)
        }
    }

}
