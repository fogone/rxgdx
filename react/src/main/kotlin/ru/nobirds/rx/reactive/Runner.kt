package ru.nobirds.rx.reactive

import java.util.concurrent.Executor
import java.util.concurrent.Executors

interface Runner {

    operator fun invoke(block:()->Unit)

}

object Runners {

    val immediate:Runner = runner { it() }

    fun executor(executor: Executor):Runner = runner {
        executor.execute(it)
    }

    fun newThread():Runner = executor(Executors.newSingleThreadExecutor())

    fun runner(execution: (()->Unit) -> Unit): Runner = object: Runner {
        override fun invoke(block: () -> Unit) = execution(block)
    }

}

fun <T> ReactiveStream<T>.runAt(runner: Runner): ReactiveStream<T> = RunnableReactiveStream(this, runner)

fun <T> ReactiveStream<T>.inExecutorThread(executor: Executor): ReactiveStream<T> = runAt(Runners.executor(executor))

fun <T> ReactiveStream<T>.inNewThread(): ReactiveStream<T> = runAt(Runners.newThread())

class RunnableReactiveStream<out T>(val reactiveStream: ReactiveStream<T>, val runner: Runner) : ReactiveStream<T> {

    override fun subscribe(subscriber: Subscriber<T>): Subscription {
        return reactiveStream.subscribe(subscriber) { value ->
            runner { subscriber.onNext(value) }
        }
    }

}
