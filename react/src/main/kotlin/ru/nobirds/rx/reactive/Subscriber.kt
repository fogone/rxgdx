package ru.nobirds.rx.reactive

interface Subscriber<in T> {

    fun onNext(value:T)

    fun onComplete() {
    }

    fun onError(exception:Throwable) {
        throw exception
    }

}

fun <T> Subscriber<T>.onNext(vararg values:T) {
    for (value in values) {
        onNext(value)
    }
}

fun <T> Subscriber<T>.onNext(values:Iterable<T>) {
    for (value in values) {
        onNext(value)
    }
}

fun <T> subscriber(onNext: (T) -> Unit): Subscriber<T> = object : Subscriber<T> {
    override fun onNext(value: T) = onNext(value)
}