package ru.nobirds.rx

import org.junit.Assert
import org.junit.Test
import ru.nobirds.rx.reactive.reactiveStreamProducer
import ru.nobirds.rx.reactive.subscribe

class RxTest {

    @Test
    fun subscriberTest() {
        val observableObserver = reactiveStreamProducer<String>()

        val result = arrayListOf<String>()

        val subscription = observableObserver.subscribe { result.add(it) }

        observableObserver.onNext("test")

        Assert.assertEquals(1, result.size)
        Assert.assertEquals("test", result.first())

        subscription.unsubscribe()

        observableObserver.onNext("test2")

        Assert.assertTrue(result.size == 1)
    }

}