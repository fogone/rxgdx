package ru.nobirds.rx

import org.junit.Assert
import org.junit.Test
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.reactive.subscribe

class InvalidatedTest {

    @Test
    fun test1() {
        val property1 = property("test1")
        val property2 = property("test2")

        val changed = changed(property1, property2)

        var changedFlag = false

        changed.subscribe {
            changedFlag = true
        }

        val invalidated = invalidated()

        invalidated.bindOneWaySignal(changed)

        Assert.assertTrue(invalidated.value)

        invalidated.with {}

        Assert.assertFalse(invalidated.value)

        property1.value = "changed"

        Assert.assertTrue(changedFlag)
        Assert.assertTrue(invalidated.value)

        invalidated.with {}

        Assert.assertFalse(invalidated.value)

        property2.value = "changed"

        Assert.assertTrue(invalidated.value)
    }

}