package ru.nobirds.rx

import org.junit.Assert
import org.junit.Test
import ru.nobirds.rx.property.Converters
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.TwoWayConverters
import ru.nobirds.rx.property.bindOneWay
import ru.nobirds.rx.property.bindTwoWay

class PropertyTest {

    @Test
    fun bindToTest() {
        val property1 = Property("test1")
        val property2 = Property("test2")

        val subscription = property2.bindOneWay(property1)

        property1.value = "processed"

        Assert.assertEquals("processed", property1.value)
        Assert.assertEquals("processed", property2.value)

        property2.value = "unchanged"

        Assert.assertEquals("processed", property1.value)
        Assert.assertEquals("unchanged", property2.value)

        subscription.unsubscribe()

        property1.value = "updated"

        Assert.assertEquals("updated", property1.value)
        Assert.assertEquals("unchanged", property2.value)
    }

    @Test
    fun bindTest() {
        val property1 = Property("test1")
        val property2 = Property("test2")

        val subscription = property2.bindTwoWay(property1)

        property1.value = "processed"

        Assert.assertEquals("processed", property1.value)
        Assert.assertEquals("processed", property2.value)

        property2.value = "unchanged"

        Assert.assertEquals("unchanged", property1.value)
        Assert.assertEquals("unchanged", property2.value)

        subscription.unsubscribe()

        property1.value = "updated"

        Assert.assertEquals("updated", property1.value)
        Assert.assertEquals("unchanged", property2.value)

        property2.value = "updated2"

        Assert.assertEquals("updated", property1.value)
        Assert.assertEquals("updated2", property2.value)
    }

    @Test
    fun bindWithConvertTest() {
        val property1 = Property("100")
        val property2 = Property(100)

        val subscription = property1.bindOneWay(property2, Converters.anyToString)

        property2.value = 300

        Assert.assertEquals("300", property1.value)
        Assert.assertEquals(300, property2.value)

        property1.value = "500"

        Assert.assertEquals("500", property1.value)
        Assert.assertEquals(300, property2.value)

        subscription.unsubscribe()

        property2.value = 600

        Assert.assertEquals("500", property1.value)
        Assert.assertEquals(600, property2.value)
    }

    @Test
    fun bindTwoWayWithConvertTest() {
        val property1 = Property("100")
        val property2 = Property(100)

        val subscription = property1.bindTwoWay(property2, TwoWayConverters.intToString)

        property2.value = 300

        Assert.assertEquals("300", property1.value)
        Assert.assertEquals(300, property2.value)

        property1.value = "500"

        Assert.assertEquals("500", property1.value)
        Assert.assertEquals(500, property2.value)

        subscription.unsubscribe()

        property2.value = 600

        Assert.assertEquals("500", property1.value)
        Assert.assertEquals(600, property2.value)

        property1.value = "700"

        Assert.assertEquals("700", property1.value)
        Assert.assertEquals(600, property2.value)
    }
}