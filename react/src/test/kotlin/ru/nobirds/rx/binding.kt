package ru.nobirds.rx

import org.junit.Assert
import org.junit.Test
import ru.nobirds.rx.property.asInt
import ru.nobirds.rx.property.asString
import ru.nobirds.rx.property.bindOneWay
import ru.nobirds.rx.property.plus
import ru.nobirds.rx.property.property

class BindingTest {

    @Test
    fun test1() {
        val stringProperty = property("300")

        val intProperty1 = property(10)
        val intProperty2 = property(20)

        val binding = intProperty1 + intProperty2

        stringProperty.bindOneWay(binding.asString())

        Assert.assertEquals("30", stringProperty.value)

        intProperty1.value = 100

        Assert.assertEquals("120", stringProperty.value)
    }


    @Test
    fun test2() {
        val stringProperty = property("300")

        val intProperty1 = property(10)

        val subscription = intProperty1.bindOneWay(stringProperty.asInt())

        Assert.assertEquals(300, intProperty1.value)

        stringProperty.value = "1000"

        Assert.assertEquals(1000, intProperty1.value)

        subscription.unsubscribe()

        stringProperty.value = "10"

        Assert.assertEquals(1000, intProperty1.value)
    }

    @Test
    fun test3() {
        val stringProperty = property("300")

        val intProperty1 = property(10)

        intProperty1.bindOneWay(stringProperty.asInt())

        stringProperty.bindOneWay(intProperty1.asString())

        intProperty1.value = 1000


    }
}