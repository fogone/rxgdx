package ru.nobirds.rx.reactive

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert
import org.junit.Test

class SubscriptionTest {

    @Test
    fun simpleSubscriptionTest() {
        val observable = reactiveStream<Int> {
            it.onNext(1)
        }

        var done = false

        observable.subscribe {
            done = true
        }

        Assert.assertThat(done, equalTo(true))
    }

    @Test
    fun unSubscribeTest() {
        val observableSubscriber = reactiveStreamProducer<Int>()

        var done = false

        val subscription = observableSubscriber.subscribe {
            done = true
        }

        observableSubscriber.onNext(1)

        Assert.assertThat(done, equalTo(true))

        done = false

        subscription.unsubscribe()

        observableSubscriber.onNext(2)

        Assert.assertThat(done, equalTo(false))
    }

}