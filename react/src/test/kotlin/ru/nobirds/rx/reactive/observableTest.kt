package ru.nobirds.rx.reactive

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Assert.fail
import org.junit.Test

class PredefinedReactiveStreamTest {

    @Test
    fun simpleObservableTest() {

        val result = arrayListOf<Int>()

        reactiveStream(1, 2, 3, 4).map { it * 2 }.filter { it % 4 == 0 }.subscribe {
            result.add(it)
        }

        assertThat(listOf(4, 8), equalTo(result.toList()))
    }

    @Test
    fun onErrorTest() {

        var complete = false
        var done = false

        reactiveStream(10).map { assert(false) }.subscribeFor {
            next {
                fail()
            }
            error {
                done = true
            }
            complete {
                complete = true
            }
        }

        assertThat(done, equalTo(true))
        assertThat(complete, equalTo(true))
    }

}