package ru.nobirds.rx.reactive

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test

class BufferedReactiveStreamTest {

    @Test
    fun simpleTest() {
        val observableSubscriber = reactiveStreamProducer<Int>()

        val buffered = observableSubscriber.buffered()

        observableSubscriber.onNext(1)

        val result = arrayListOf<Int>()

        buffered.subscribe {
            result.add(it)
        }

        assertThat(result.size, equalTo(1))

        observableSubscriber.onNext(2)

        val result2 = arrayListOf<Int>()

        buffered.subscribe {
            result2.add(it)
        }

        assertThat(result.size, equalTo(2))
        assertThat(result2.size, equalTo(2))
    }

}